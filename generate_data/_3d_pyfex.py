
# Creates data in 3D from finite elements solution with PyfeX

# Imports
import numpy as np
import scipy.io

#pip install pytictoc
from pytictoc import TicToc
import time
import json

import sys,os
sys.path.append("{0}{1}".format( os.path.dirname(os.path.realpath(__file__)) , "/../pyfex/pyfex/utils/numerics"))
#sys.path.append("{0}{1}".format( os.path.dirname(os.path.realpath(__file__)) , "./pyfex/utils/numerics"))
from RB_library import *
from EPt import *


from arrayBind import *
from Electrophysiology import *

param_file    = '../lifex_electrophysiology_slab.prm'

e = pyfex_init(param_file,False);

"""
file_name = '../pinn-electrophysiology/params.json'

dir = '../data/pyfex/'

num_initial_points = 1000
num_collocation_points = 15000
num_training_samples = 15000
num_test_points = 5000
num_boundary_points = 3000
num_activation_points = 2000

train_on_border = False
T0 = 6e-3
"""

# Specify simulation time and nonlinear solver

class TIMESOLVER:
    "Time parameters"
    t0 = 0
    tf = 0.1
    dt = 1e-4


class NONLINEARSOLVER:
    "Nonlinear solver parameters"
    max_iterations    = 10
    absolute_res_tol  = 1e-8
    relative_incr_tol = 1e-10



class create_data(object):
    def __init__(self):


        """
        class TIMESOLVER:
            "Time parameters"
            t0 = time[0]
            tf = time[1]
            dt = time[2]


        class NONLINEARSOLVER:
            "Nonlinear solver parameters"
            max_iterations    = nonlinear[0]
            absolute_res_tol  = nonlinear[1]
            relative_incr_tol = nonlinear[2]



        self.TIMESOLVER.t0 = time.t0
        self.TIMESOLVER.tf = time.tf
        self.TIMESOLVER.dt = time.dt

        self.NONLINEARSOLVER.max_iterations = nonlinear.max_iterations
        self.NONLINEARSOLVER.absolute_res_tol = nonlinear.absolute_res_tol
        self.NONLINEARSOLVER.relative_incr_tol = nonlinear.relative_incr_tol
        """

        self.xyz_array = np.array([])
        self.u_array = np.array([])
        self.act = np.array([])
        print('Pass classes ok')

    def run(self):


        d = PublicElectrophysiology('Electrophysiology','Electrophysiology / Ionic model','Fiber generation','Electrophysiology / Applied current','Electrophysiology / Ischemic region',True);

        d.readParamFile()
        d.runModel()
        x = d.nodes_x()
        y = d.nodes_y()
        z = d.nodes_z()


        ### Offline phase ###

        d_h = EPt(d, 1, TIMESOLVER, NONLINEARSOLVER)


        self.u_array = np.matrix(d_h).T

        self.act = np.matrix(d.activation_time).T

        #print(np.amax(S_h))
        #print(np.amin(S_h))


        self.xyz_array = np.stack((x,y,z), axis=1)

        # Saved for debugging

        #np.save(dir+'Snapshots', u_array)
        #np.save(dir+'nodes', xyz_array)
        #np.save(dir+'activation_3d', a)



    def __call__(self, file_name, dir, num_initial_points, num_collocation_points, num_training_samples, num_test_points, num_boundary_points, num_activation_points, uniform=True):

        assert isinstance(file_name, str), 'File name must be \'str\''

        # Read the domain parameters

        prm = json.load(open(file_name))
        w = np.array(prm['3D']['w'])
        T = prm['3D']['T']
        space_dim = 3
        t_array = np.arange(TIMESOLVER.t0, TIMESOLVER.tf + TIMESOLVER.dt, TIMESOLVER.dt)

        #print(t_array[0], t_array[-1])
        #print(T[0], T[1])

        # Check if file and TIMESOLVER are compatible
        if t_array[0] > T[0] or t_array[-1] < T[1]:
            raise Exception('The time interval in the parameter file must be compatible with the TIMESOLVER chosen.')


        # Indices of values inside [0,T]
        t_admissible = np.squeeze(np.where(np.logical_and(t_array >= T[0], t_array <= T[1])))

        # Geta data from pyfex, only at first call
        print('Generating electrophysiology data with PyfeX')
        if self.xyz_array.size == 0:
            self.run()
            print('\nSimulation over')
        # Generate a usable dataset


        # Training points------------------------------------------------------------------------
        # I sample them from the results gotten with LifeX

        # Compare ranges in space from the mesh and the ones specified in the file
        print('Mesh ranges:')
        print([np.min(self.xyz_array[:,0]), np.max(self.xyz_array[:,0])])
        print([np.min(self.xyz_array[:,1]), np.max(self.xyz_array[:,1])])
        print([np.min(self.xyz_array[:,2]), np.max(self.xyz_array[:,2])])

        print('File ranges:')
        print(w)

        print('\nStarting random sampling')

        # Initial points-------------------------------------------------------------------------------------------------------------------
        print('Initial datum')
        train_ind = np.random.choice(self.xyz_array.shape[0], size=(num_initial_points))
        t_ind = int((T[0]-TIMESOLVER.t0)/TIMESOLVER.dt) # Positive

        x_init = self.xyz_array[train_ind, :]
        u_init = np.reshape(self.u_array[train_ind, t_ind], (num_initial_points,1))

        t_init = np.full((num_initial_points, 1), T[0])

        np.save(dir+'init', np.concatenate((x_init, t_init, u_init), axis=1))


        # Collocation points---------------------------------------------------------------------------------------------------------------
        # Homogeneous data
        # TODO: check the .prm file for the impulse?
        print('Collocation points')
        x_grid = np.random.uniform(size=[num_collocation_points, space_dim], low=w[:,0], high=w[:,1])
        t_grid = np.random.uniform(size=[num_collocation_points,1], low=T[0], high=T[1])

        np.save(dir+'PDE', np.concatenate((x_grid, t_grid), axis=1))




        # Training points ----------------------------------------------------------------------------------------------------
        print('Training samples')

        # In this case train data is taken only on one face of the slab
        # The syntax is the same even if in one case ind_y is an array and in the other an int
        if not uniform:
            ind_y = np.arange(self.xyz_array.shape[0])[self.xyz_array[:,1]==w[1,0]]
        else:
            ind_y = self.xyz_array.shape[0]

        train_ind = np.random.choice(ind_y, size=(num_training_samples))
        t_ind     = np.random.choice(t_admissible, size=(num_training_samples))

        x_train = self.xyz_array[train_ind, :]
        t_train = np.reshape(t_array[t_ind], (num_training_samples, 1))
        u_train = np.reshape(self.u_array[train_ind, t_ind], (num_training_samples, 1))


        np.save(dir+'train', np.concatenate((x_train, t_train, u_train), axis=1))

        del x_train, t_train, u_train


        # Test points ------------------------------------------------------------------------------------------------------------------------7
        print('Test points')

        test_ind  = np.random.choice(self.xyz_array.shape[0], size=(num_test_points))
        t_ind     = np.random.choice(t_admissible, size=(num_test_points))

        x_test = self.xyz_array[test_ind, :]
        t_test = np.reshape(t_array[t_ind], (num_test_points, 1))
        u_test = np.reshape(self.u_array[test_ind, t_ind], (num_test_points, 1))

        np.save(dir+'MSE', np.concatenate((x_test, t_test, u_test), axis=1))

        del x_test, t_test, u_test


        #Boundary points----------------------------------------------------------------------------------------------------------------------
        print('Boundary conditions')

        # Indeces of nodes at the borders
        ind_x = np.logical_or(self.xyz_array[:,0]==w[0,0], self.xyz_array[:,0]==w[0,1])
        ind_y = np.logical_or(self.xyz_array[:,1]==w[1,0], self.xyz_array[:,1]==w[1,1])
        ind_z = np.logical_or(self.xyz_array[:,2]==w[2,0], self.xyz_array[:,2]==w[2,1])

        # Samples from nodes at the borders
        xyz_bc = self.xyz_array[np.logical_or(ind_x, np.logical_or(ind_y, ind_z)), :]
        xyz_bc = xyz_bc[np.random.choice(xyz_bc.shape[0], (num_boundary_points)),:]
        g_bc = np.zeros((num_boundary_points, 1))
        t_bc = np.random.uniform(size=(num_boundary_points,1), low=T[0], high=T[1])


        N = np.zeros((num_boundary_points, 3))

        ind_x = np.logical_or(xyz_bc[:,0]==w[0,0], xyz_bc[:,0]==w[0,1])
        ind_y = np.logical_or(xyz_bc[:,1]==w[1,0], xyz_bc[:,1]==w[1,1])
        ind_z = np.logical_or(xyz_bc[:,2]==w[2,0], xyz_bc[:,2]==w[2,1])

        N[ind_x,0] = 1
        N[ind_y,1] = 1
        N[ind_z,2] = 1

        # If the point in on a corner the versor is null
        for i in range(num_boundary_points):
            if np.sum(N[i,:]) > 1:
                N[i,:] = np.zeros((3))
        #print(N)

        np.save(dir+'BD_versors', N)
        np.save(dir+'boundary', np.concatenate((xyz_bc,t_bc, g_bc), axis=1))


        # Activation times ------------------------------------------------------------------------------------------------------------------------
        print('Activation times')

        # Loaded directly from PyfeX
        train_ind = np.random.choice(self.xyz_array.shape[0], size=(num_activation_points))
        xyz_act = self.xyz_array[train_ind, :]

        t_act = self.act[train_ind,:]

        np.save(dir+'activation_data', np.concatenate((xyz_act, t_act), axis=1))

        print('All data saved at : {}\n'.format((dir)))
        print(20*'='+'\n')
