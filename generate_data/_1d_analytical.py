
# Creates data in 1D from analytical solution

import numpy as np
import json

# Parameters to use
"""
# File with the parameters
file_name = '../pinn-electrophysiology/params.json'

# Directory were the data is saved
dir = '../data/'


# Read parameters from file

num_initial_points = 1000
num_collocation_points = 5000
num_training_samples = 2000
num_test_points = 5000
num_boundary_points = 1000
num_activation_points = 2000
"""

class create_data(object):
    def __init__(self):
        pass

    def __call__(self, file_name, dir, num_initial_points, num_collocation_points, num_training_samples, num_test_points, num_boundary_points, num_activation_points, uniform=True):

        assert isinstance(file_name, str), 'File name must be \'str\''

        prm = json.load(open(file_name))
        w = np.array(prm['1D']['w'])
        T = prm['1D']['T']
        space_dim = 1
        phi = prm['1D']['Initial datum']['center']
        K = prm['Physics']['Aliev-Panfilov']['K_exact']
        alpha = prm['Physics']['Aliev-Panfilov']['alpha']
        D = prm['1D']['sigma_exact'][0]


        # Uses analytical solution
        c = np.sqrt(2*D) * np.sqrt(K) * (1 - 2*alpha) / 2
        beta = np.sqrt(K)/(2 * np.sqrt(2*D))

        sol    = lambda x, t: 1/2 + np.tanh(-beta*(x - c*t - phi))/2

        # Initial data
        print('Initial datum')
        x_init = np.random.uniform(size=[num_initial_points, space_dim], low = w[:,0], high=w[:,1])
        t_init = np.full([num_initial_points,1], T[0])

        #print('Init extrema: ', np.amin(x_init),np.amax(x_init))

        u_init = np.reshape(sol(x_init, t_init), newshape=(num_initial_points,1))

        np.save(dir+'init', np.concatenate((x_init, t_init, u_init), axis = 1))

        del x_init, t_init, u_init

        # Collocation points grid
        print('Collocation points')
        x_grid = np.random.uniform(size=[num_collocation_points, space_dim], low = w[:,0], high=w[:,1])
        t_grid = np.random.uniform(size=[num_collocation_points, 1], low = T[0], high=T[1])

        #print('Grid extrema: ', np.amin(x_grid),np.amax(x_grid))

        np.save(dir+'PDE', np.concatenate((x_grid, t_grid), axis = 1))
        del x_grid, t_grid

        # Boundary data - Dirichlet
        print('Boundary data')
        x_bdry = np.random.choice(w[0,:], size=(num_boundary_points,1))
        t_bdry = np.random.uniform(size=(num_boundary_points, 1), low = T[0], high=T[1])
        u_bdry = sol(x_bdry, t_bdry)


        np.save(dir+'boundary', np.concatenate((x_bdry, t_bdry, u_bdry), axis = 1))
        n = np.zeros((num_boundary_points,1))

        # Versors
        for i in range(num_boundary_points):
            if x_bdry[i] == w[0,0]:
                n[i] = -1.0
            else:
                n[i] = 1.0

        np.save(dir+'BD_versors', n)


        del t_bdry, u_bdry, x_bdry, n

        # Training points

        # Training samples
        # Can choose to use only a section of the domain
        print('Training samples')
        if uniform:
            x_train = np.random.uniform(size=(num_training_samples, space_dim), low = w[:,0], high=w[:,1])
            t_train = np.random.uniform(size=(num_training_samples, 1), low = T[0], high=T[1])
        else:
            x_train = np.random.uniform(size=(num_training_samples, space_dim), low = w[:,0], high=(w[:,1]-w[:,1])/2)
            t_train = np.random.uniform(size=(num_training_samples, 1), low = T[0], high=T[1])



        u_train = sol(x_train, t_train)

        np.save(dir+'train', np.concatenate((x_train, t_train, u_train), axis = 1))


        # MSE points
        print('Test points')
        x_test = np.random.uniform(size=[num_test_points, space_dim], low = w[:,0], high=w[:,1])
        t_test = np.random.uniform(size=[num_test_points, 1], low = T[0], high=T[1])
        u_test = sol(x_test, t_test)

        np.save(dir+'MSE', np.concatenate((x_test, t_test, u_test), axis = 1))



        # Activation points
        print('Activation times')
        x_act = np.random.uniform(size=(num_activation_points, space_dim), low = phi, high=w[:,1])
        t_act = (x_act - phi)/c

        np.save(dir+'activation_data', np.concatenate((x_act, t_act), axis=1))


# Run

#create_data(file_name, dir, num_initial_points, num_collocation_points, num_training_samples, num_test_points, num_boundary_points, num_activation_points)
