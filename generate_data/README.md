This directory contains definitions of data generating functors as described in Section 5.3 of the report.

These functors can be included with the syntax

```python
from  generate_data._module_name import create_data
```
a particular attention must be given to the 3D case with PyfeX, where the `.prm`, time step and nonlinear solver are specified.
