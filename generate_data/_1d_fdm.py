
# Creates data in 1D from finite difference solution


import numpy as np
import json
import matplotlib.pyplot as plt

# Parameters to use
"""
# File with the parameters
file_name = '../pinn-electrophysiology/params.json'

# Directory were the data is saved
dir = '../data/1d/'


num_initial_points = 1000
num_collocation_points = 10000
num_training_samples = 5000
num_test_points = 5000
num_boundary_points = 1000
num_activation_points = 2000
"""
class create_data(object):

    def __init__(self):
        self.u_fdm = np.array([])

    def __call__(self, file_name, dir, num_initial_points, num_collocation_points, num_training_samples, num_test_points, num_boundary_points, num_activation_points, uniform=True):

        assert isinstance(file_name, str), 'File name must be \'str\''
        # Read the domain parameters

        prm = json.load(open(file_name))
        w = np.array(prm['1D']['w'])
        T = prm['1D']['T']
        space_dim = 1
        l2 = prm['1D']['Initial datum']['width']**2
        c = prm['1D']['Initial datum']['center']


        # Initial data
        print('Initial datum')
        x_init = np.random.uniform(size=[num_initial_points, space_dim], low = w[:,0], high=w[:,1])
        t_init = np.full([num_initial_points,1], T[0])

        #print('Init extrema: ', np.amin(x_init),np.amax(x_init))

        u_init = np.zeros([num_initial_points, 1])


        # Initial datum, indicator function of (c-l, c+l)
        for i in range(num_initial_points):
            if (x_init[i,0] - c)**2  < l2:
                u_init[i] = 1.0

        np.save(dir+'init', np.concatenate((x_init, t_init, u_init), axis = 1))

        del x_init, t_init, u_init

        # Collocation points grid
        print('Collocation points')
        x_grid = np.random.uniform(size=[num_collocation_points, space_dim], low = w[:,0], high=w[:,1])
        t_grid = np.random.uniform(size=[num_collocation_points, 1], low = T[0], high=T[1])

        #print('Grid extrema: ', np.amin(x_grid),np.amax(x_grid))

        np.save(dir+'PDE', np.concatenate((x_grid, t_grid), axis = 1))
        del x_grid, t_grid

        # Boundary data
        print('Boundary data')

        # In this case the boundary condition is homogeneous
        x_bdry = np.random.choice(w[0,:], size=(num_boundary_points,1))
        t_bdry = np.random.uniform(size=(num_boundary_points, 1), low = T[0], high=T[1])
        u_bdry = np.zeros((num_boundary_points,1))

        np.save(dir+'boundary', np.concatenate((x_bdry, t_bdry, u_bdry), axis = 1))
        n = np.zeros((num_boundary_points,1))

        for i in range(num_boundary_points):
            if x_bdry[i] == w[0,0]:
                n[i] = -1.0
            else:
                n[i] = 1.0

        np.save(dir+'BD_versors', n)

        del t_bdry, u_bdry, x_bdry, n

        # FDM solution--------------------------------------------------------------------------------------------------

        dx = 0.25
        dt = 0.001

        # Computes FDM only at first call
        if self.u_fdm.size == 0:
            dx2 = dx*dx
            nx, nt = int((w[0,1]-w[0,0])/dx), int((T[1]-T[0])/dt)

            K = prm['Physics']['Aliev-Panfilov']['K_exact']
            alpha = prm['Physics']['Aliev-Panfilov']['alpha']


            u_fdm   = np.zeros((nx + 1, nt + 1))

            # Translate the center to work with indeces
            c_i = c - w[0,0]

            #Initial condition
            indeces_0 = []
            for i in range(nx + 1):
                    p2 = (i*dx-c_i)**2
                    if p2 < l2:
                        u_fdm[i,0] = 1.0
                        indeces_0.append(i)

            def do_timestep(u0, s):
                # Propagate with forward-difference in time, central-difference in space
                u = np.zeros((nx + 1))
                u[1:-1] = u0[1:-1] + dt * (
                      s*(u0[2:] - 2.0*u0[1:-1] + u0[:-2])/dx2) + dt * K * np.multiply(np.multiply(u0[1:-1],(u0[1:-1]-alpha)),(1.0 - u0[1:-1]))

                return u


            def lin_interpolate(f, index, increment, point):
                #Linearly interpolate the 3D function goven the values in a grid around the point
                #print(f.shape)
                coord   = (point - np.multiply(index, increment))/increment
                i_coord = np.ones(2) - coord
                #print(coord.shape)
                #print(i_coord.shape)

                return f[1,1]*coord[0]*coord[1] \
                    + f[0,1]*i_coord[0]*coord[1] \
                    + f[1,0]*coord[0]*i_coord[1] \
                    + f[0,0]*i_coord[0]*i_coord[1]

            print('Computing real solution with FDM')
            for k in range(nt):
                u_fdm[:,k+1] = do_timestep(u_fdm[:,k], prm['2D']['sigma_exact'][0])

        # Training samples
        # Can choose to use only a section of the domain
        print('Training samples')
        if uniform:
            x_train = np.random.uniform(size=(num_training_samples, space_dim), low = w[:,0], high=w[:,1])
            t_train = np.random.uniform(size=(num_training_samples, 1), low = T[0], high=T[1])
        else:
            w_half = w.copy()
            w_half[0,1] = (w[0,1]+w[0,0])/2
            x_train = np.random.uniform(size=(num_training_samples, space_dim), low = w_half[:,0], high=w_half[:,1])
            t_train = np.random.uniform(size=(num_training_samples, 1), low = T[0], high=T[1])
            del w_half

        u_train = np.zeros([num_training_samples,1])

        #print('Grid extrema: ', np.amin(x_train),np.amax(x_train))


        for m in range(num_training_samples):
            i, k = int((x_train[m,0]-w[0,0])/dx), int((t_train[m]-T[0])/dt)
            u_train[m] = lin_interpolate(u_fdm[i:(i+2), k:(k+2)], (i,k), (dx,dt), np.array([x_train[m,0]-w[0,0], t_train[m]-T[0]]))

        #print('Value extrema: ', np.amin(u_train),np.amax(u_train))


        np.save(dir+'train', np.concatenate((x_train, t_train, u_train), axis = 1))
        #del x_train, t_train, u_train

        # Testing points
        print('Test points')
        x_test = np.random.uniform(size=[num_test_points, space_dim], low = w[:,0], high=w[:,1])
        t_test = np.random.uniform(size=[num_test_points, 1], low = T[0], high=T[1]-dt)
        u_test = np.zeros([num_test_points,1])

        for m in range(num_test_points):
            i, k = int((x_test[m,0]-w[0,0])/dx), int((t_test[m]-T[0])/dt)
            u_test[m] = lin_interpolate(u_fdm[i:(i+2), k:(k+2)], (i,k), (dx,dt), np.array([x_test[m,0]-w[0,0], t_test[m]-T[0]]))

        np.save(dir+'MSE', np.concatenate((x_test, t_test, u_test), axis = 1))

        print('All data saved at : {}\n'.format((dir)))


    """
    # Activation points
    # A bit imprecise, just takes the index of the first value in u_fdm > 0.5

    x_i = np.random.choice(indeces_0, size=(num_activation_points))
    x_act = x_i*dx + w[0,0]

    ind = np.where(u_fdm[x_i,:] >= 0.5)
    print(ind)
    t_act = np.full((num_activation_points), T[1])
    next = 0
    for j in x_i:
        i_ = np.where(ind[0]==j)
        if i_[0].shape[0] != 0:
            t_act[next] = ind[1][i_][0]*dt + T[0]
        next += 1

    print(t_act)

    np.save(dir+'act', np.concatenate((x_act, t_act), axis=1))


    fig = plt.figure()
    ax = fig.add_subplot(121)
    ax.scatter(x_train[:,0], t_train, c=u_train)
    ax = fig.add_subplot(122)
    ax.plot(range(nx+1),u_fdm[:,nt//2])
    plt.savefig('../figure/tests/data_1d_fdm.png')

    """
# Run

#create_data(file_name, dir, num_initial_points, num_collocation_points, num_training_samples, num_test_points, num_boundary_points, num_activation_points)
