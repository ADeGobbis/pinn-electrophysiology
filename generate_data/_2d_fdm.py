
# Creates data in 2D from finite difference solution


import numpy as np
import json
import matplotlib.pyplot as plt


# Parameters to use
"""
# File with the parameters
file_name = '../pinn-electrophysiology/params.json'

# Directory were the data is saved
dir = '../data/'



num_initial_points = 1000
num_collocation_points = 10000
num_training_samples = 5000
num_test_points = 5000
num_boundary_points = 1000
"""

class create_data(object):
    def __init__(self):
        self.u_fdm = np.array([])

    def __call__(self, file_name, dir, num_initial_points, num_collocation_points, num_training_samples, num_test_points, num_boundary_points, num_activation_points, uniform=True):

        assert isinstance(file_name, str), 'File name must be \'str\''

        # Read the domain parameters

        prm = json.load(open(file_name))
        w = np.array(prm['2D']['w'])
        T = prm['2D']['T']
        space_dim = 2
        r2 = prm['2D']['Initial datum']['radius']**2
        c = prm['2D']['Initial datum']['center']


        # Initial data
        print('Initial datum')
        x_init = np.random.uniform(size=[num_initial_points, space_dim], low = w[:,0], high=w[:,1])
        t_init = np.full([num_initial_points,1], T[0])

        #print('Init extrema: ', np.amin(x_init),np.amax(x_init))

        u_init = np.zeros([num_initial_points, 1])


        for i in range(num_initial_points):
            if (x_init[i,0] - c[0])**2 + (x_init[i,1] - c[1])**2 < r2:
                u_init[i] = 1.0

        np.save(dir+'init', np.concatenate((x_init, t_init, u_init), axis = 1))

        del x_init, t_init, u_init



        # Collocation points grid
        print('Collocation points')
        x_grid = np.random.uniform(size=[num_collocation_points, space_dim], low = w[:,0], high=w[:,1])
        t_grid = np.random.uniform(size=[num_collocation_points, 1], low = T[0], high=T[1])

        #print('Grid extrema: ', np.amin(x_grid),np.amax(x_grid))

        np.save(dir+'PDE', np.concatenate((x_grid, t_grid), axis = 1))
        del x_grid, t_grid



        # Boundary data
        print('Boundary data')

        # Maps points uniformly to the boundary
        def to_bd(x):
            if x < 1:
                return [x, w[0,1], 0.0, -1.0]
            elif x < 2:
                return [w[0,0], x - 1, -1.0, 0]
            elif x < 3:
                return [x-2, w[1,1], 0.0, 1.0]
            else:
                return [w[1,0], x-3, 1.0, 0.0]

        t_bdry = np.random.uniform(size=[num_boundary_points, 1], low = T[0], high=T[1])
        u_bdry = np.zeros((num_boundary_points, 1))

        unif = np.random.uniform(size=[num_boundary_points], low = 0.0, high= 4.0)
        res = np.array([to_bd(x) for x in unif])

        np.save(dir+'boundary', np.concatenate((res[:,:2], t_bdry, u_bdry), axis = 1))
        np.save(dir+'BD_versors', res[:,-2:])

        del t_bdry, u_bdry, res, unif, to_bd


        # FDM solution


        dx = 0.5
        dy = 0.5
        dt = 0.001

        # Computes FDM only at first call
        if self.u_fdm.size == 0:
            # At first call the FDM solution is computed, on subsequent calls the saved value is used

            dx2, dy2, dxdy = dx*dx, dy*dy, dx*dy
            nx, ny, nt = int((w[0,1]-w[0,0])/dx), int((w[1,1]-w[1,0])/dy), int((T[1]-T[0])/dt)

            K = prm['Physics']['Aliev-Panfilov']['K_exact']
            alpha = prm['Physics']['Aliev-Panfilov']['alpha']


            self.u_fdm   = np.zeros((nx + 1, ny + 1, nt + 1))
            # Translate the center to work with indeces
            c_i = c - w[:,0]
            #Initial condition
            for i in range(nx + 1):
                for j in range(ny + 1):
                    p2 = (i*dx-c[0])**2 + (j*dy-c[1])**2
                    if p2 < r2:
                        self.u_fdm[i,j,0] = 1.0

            def do_timestep(u0, s):
                # Propagate with forward-difference in time, central-difference in space
                u = np.zeros((nx + 1, ny + 1))
                u[1:-1, 1:-1] = u0[1:-1, 1:-1] + dt * (
                      s[0]*(u0[2:, 1:-1] - 2.0*u0[1:-1, 1:-1] + u0[:-2, 1:-1])/dx2
                      + s[1]*(u0[1:-1, 2:] - 2.0*u0[1:-1, 1:-1] + u0[1:-1, :-2])/dy2 ) + dt * K * np.multiply(np.multiply(u0[1:-1, 1:-1],(u0[1:-1, 1:-1]-alpha)),(1.0 - u0[1:-1, 1:-1]))

                return u


            print('Computing real solution with FDM')
            for k in range(nt):
                self.u_fdm[:,:,k+1] = do_timestep(self.u_fdm[:,:,k], prm['2D']['sigma_exact'])

        # Linear interpolation function for 2 + 1 dimensions
        # Used for points outside the nodes
        def lin_interpolate(f, index, increment, point):
            #Linearly interpolate the 3D function goven the values in a grid around the point
            #print(f.shape)
            coord   = (point - np.multiply(index, increment))/increment
            i_coord = np.ones(3) - coord

            return f[1,1,1]*coord[0]*coord[1]*coord[2] \
                + f[0,1,1]*i_coord[0]*coord[1]*coord[2] \
                + f[1,0,1]*coord[0]*i_coord[1]*coord[2] \
                + f[1,1,0]*coord[0]*coord[1]*i_coord[2] \
                + f[0,0,1]*i_coord[0]*i_coord[1]*coord[2] \
                + f[0,1,0]*i_coord[0]*coord[1]*i_coord[2] \
                + f[1,0,0]*coord[0]*i_coord[1]*i_coord[2] \
                + f[0,0,0]*i_coord[0]*i_coord[1]*i_coord[2]


        # Training samples
        # Can choose to use only a section of the domain
        print('Training samples')
        if uniform:
            x_train = np.random.uniform(size=(num_training_samples, space_dim), low = w[:,0], high=w[:,1])
            t_train = np.random.uniform(size=(num_training_samples, 1), low = T[0], high=T[1])
        else:
            w_half = w.copy()
            w_half[0,1] = (w[0,1]+w[0,0])/2
            x_train = np.random.uniform(size=(num_training_samples, space_dim), low = w_half[:,0], high=w_half[:,1])
            t_train = np.random.uniform(size=(num_training_samples, 1), low = T[0], high=T[1])
            del w_half


        u_train = np.zeros([num_training_samples,1])

        #print('Grid extrema: ', np.amin(x_train),np.amax(x_train))


        for m in range(num_training_samples):
            i, j, k = int((x_train[m,0]-w[0,0])/dx), int((x_train[m,1]-w[1,0])/dy), int((t_train[m]-T[0])/dt)
            u_train[m] = lin_interpolate(self.u_fdm[i:(i+2), j:(j+2), k:(k+2)], (i,j,k), (dx,dy,dt), np.array([x_train[m,0]-w[0,0], x_train[m,1]-w[1,0], t_train[m]-T[0]]))

        np.save(dir+'train', np.concatenate((x_train, t_train, u_train), axis = 1))
        #del x_train, t_train, u_train

        # Testing points
        print('Test points')
        x_test = np.random.uniform(size=[num_test_points, space_dim], low = w[:,0], high=w[:,1])
        t_test = np.random.uniform(size=[num_test_points, 1], low = T[0], high=T[1]-dt)
        u_test = np.zeros([num_test_points,1])

        for m in range(num_test_points):
            i, j, k = int((x_test[m,0]-w[0,0])/dx), int((x_test[m,1]-w[1,0])/dy), int((t_test[m]-T[0])/dt)
            u_test[m] = lin_interpolate(self.u_fdm[i:(i+2), j:(j+2), k:(k+2)], (i,j,k), (dx,dy,dt), np.array([x_test[m,0]-w[0,0], x_test[m,1]-w[1,0], t_test[m]-T[0]]))

        np.save(dir+'MSE', np.concatenate((x_test, t_test, u_test), axis = 1))

        print('All data saved at : {}\n'.format((dir)))


    # Run
    #create_data(file_name, dir, num_initial_points, num_collocation_points, num_training_samples, num_test_points, num_boundary_points, num_activation_points)
