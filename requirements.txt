numpy>=1.16
scipy>=1.4
matplotlib>=3.1
tensorflow>=2.3
mpi4py>=3.0
