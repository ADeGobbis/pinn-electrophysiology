#!/usr/bin/env python3
import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import nisaba as ns
from params import *


T = [0.0, 2.0]
w = [-30.0, 30.0]

w2 = (w[1]-w[0])**2

print(rho)
# Define the weight parameter
if len(sys.argv) >= 2:
    K = float(sys.argv[1])
else:
    K = 100

D = tf.constant(D_exact, dtype='float64')
K = tf.constant(K, dtype='float64')
alpha = 0.1

c = np.sqrt(2*D) * np.sqrt(K) * (1 - 2*alpha) / 2
beta = np.sqrt(K)/(2 * np.sqrt(2*D))

tf_sol    = lambda x, t: 1/2 + tf.tanh(-beta*(x - c*t))/2

def scale(x, limits):
    return (2*x - limits[1] - limits[0])/(limits[1] - limits[0])

rho = np.array(rho)*4

error = np.zeros((len(rho), num_points))

#Create the points

#Training points
x_train  = tf.random.uniform(shape=[num_training_samples], minval = w[0], maxval = w[1]).numpy()
t_train  = tf.random.uniform(shape=[num_training_samples], minval = T[0], maxval = T[1]).numpy()
xt_train = tf.stack((scale(x_train,w), scale(t_train,T)), axis=1)
u_train = tf_sol(x_train, t_train).numpy().astype('float64')


#Collocation points
x_grid  = scale(tf.random.uniform(shape=[num_collocation_points], minval = w[0], maxval = w[1]).numpy(), w).astype('float64')
t_grid  = scale(tf.random.uniform(shape=[num_collocation_points], minval = T[0], maxval = T[1]).numpy(), T).astype('float64')

#Training points
x_test  = tf.random.uniform(shape=[num_testing_samples], minval = 2*w[0], maxval = 2*w[1]).numpy()
t_test  = tf.random.uniform(shape=[num_testing_samples], minval=T[0], maxval = T[1]).numpy()

xt_test = tf.stack((scale(x_test, w), scale(t_test, T)), axis=1)

u_exact = tf_sol(x_test, t_test).numpy().astype('float64')

for r in range(len(rho)):
    print('Weight PDE Loss: ', rho[r])
    for k in range(num_points):
        # model
        model = tf.keras.Sequential([
         tf.keras.layers.Dense(5, input_shape=(2,), activation=tf.nn.tanh),
         tf.keras.layers.Dense(5, activation=tf.nn.tanh),
         tf.keras.layers.Dense(5, activation=tf.nn.tanh),
         tf.keras.layers.Dense(1, dtype='float64')
        ])


        trainable_variables = model.variables

        @tf.function
        def PDE(x, t):
            with tf.GradientTape(persistent = True) as tape:
                tape.watch(x)
                tape.watch(t)
                u = model(tf.stack((x,t),axis=1))
                u_x = tape.gradient(u, x)
                u_t = tape.gradient(u, t)
                u_xx = tape.gradient(u_x, x)

                Iion = tf.squeeze(u * (u - 1.0) * (u - alpha))

            return u_t/(T[1]-T[0]) - 2*D*u_xx/w2 + K*Iion/2

        losses = [ns.LossMeanSquares('fit', lambda: model(xt_train) - u_train[:,None]), \
                      ns.LossMeanSquares('PDE', lambda: PDE(tf.constant(x_grid), tf.constant(t_grid)), weight=rho[r])]




        #%%%%%%%%%%%%%%%%%% Training
        # initialize optimizer
        # initialize optimizer
        pb = ns.OptimizationProblem(trainable_variables, losses, frequency_print=10000)

        ns.minimize(pb, 'scipy', 'BFGS', num_epochs=num_epochs)
        #ns.minimize(pb, 'keras', tf.keras.optimizers.Adam(learning_rate=1e-3), num_epochs=num_epochs)

        # Measure the MSE


        u_NN = model(xt_test)

        error[r,k] = tf.sqrt(tf.reduce_mean(tf.square(u_exact[:,None] - u_NN)))

        print('Point: ', k + 1)


# Plot the results in a box plot


plt.boxplot(np.transpose(error))
plt.ylim([-0.1, 1.1])
plt.xlabel('Weight of the PDE loss function')
plt.ylabel('MSE')
plt.xticks(range(1,len(rho)+1), rho/4)
textstr = '\n'.join(('BFGS', r'K =%.2f' % K, r'D =%.2f' % D))
plt.title(textstr)

#plt.show()
plt.savefig('../../figure/old/weight_PDE/bfgs_'+str(D.numpy())+'_'+str(K.numpy())+'.png')

# Last Output

print('K: ', K.numpy(), ' D: ', D.numpy())
#print('Best rho for average error: ', rho[np.argmin(mean_H_1)])
#print('Best rho for min error: ', rho[np.argmin(min_H_1)])
