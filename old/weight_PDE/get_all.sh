#!/bin/bash

array=(1.0 5.0 10.0 50.0 100.0 500.0)

for i in "${array[@]}"
do
	python nisaba_adam_1d.py $i
	python nisaba_bfgs_1d.py $i
done
