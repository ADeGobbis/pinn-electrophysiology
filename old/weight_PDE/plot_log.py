import matplotlib.pyplot as plt


K = [1, 5, 10, 50, 100, 500]
adam = [1.0, 0.1, 0.01, 0.01, 0.0001, 1e-7]
bfgs = [1.0, 1.0, 1.0, 0.01, 0.001, 1e-5]

K2 = [x**(-2) for x in K]

fig = plt.figure()
ax = fig.add_subplot(111)
ax.loglog(K, K2, 'k')
ax.loglog(K, adam, 'rx-')
ax.loglog(K, bfgs, 'bx-')
ax.legend(['K^(-2)', 'Adam', 'BFGS'])
ax.set_xlabel('K')
ax.set_ylabel('Optimal weight')

plt.savefig('figure/compare_plot.png')
