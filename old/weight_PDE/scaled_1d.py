#!/usr/bin/env python3
import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from params import *


T = [0.0, 2.0]
w = [-30.0, 30.0]

w2 = (w[1]-w[0])**2

# Define the weight parameter
if len(sys.argv) >= 2:
    K = float(sys.argv[1])
else:
    K = 100.0

D = 10.0
alpha = 0.1

c = np.sqrt(2*D) * np.sqrt(K) * (1 - 2*alpha) / 2
beta = np.sqrt(K)/(2 * np.sqrt(2*D))

tf_sol    = lambda x, t: 1/2 + tf.tanh(-beta*(x - c*t))/2

def scale(x, limits):
    return (2*x - limits[1] - limits[0])/(limits[1] - limits[0])

rho = np.array(rho)*4

error = np.zeros((len(rho), num_points))

#Create the points

#Training points
x_train  = tf.random.uniform(shape=[num_training_samples], minval = w[0], maxval = w[1]).numpy()
t_train  = tf.random.uniform(shape=[num_training_samples], minval = T[0], maxval = T[1]).numpy()
xt_train = tf.stack((scale(x_train,w), scale(t_train,T)), axis=1)
u_train = tf.constant(tf_sol(x_train, t_train))


#Collocation points
x_grid  = scale(tf.random.uniform(shape=[num_collocation_points], minval = w[0], maxval = w[1]).numpy(), w)
t_grid  = scale(tf.random.uniform(shape=[num_collocation_points], minval = T[0], maxval = T[1]).numpy(), T)

#Training points
x_test  = tf.random.uniform(shape=[num_testing_samples], minval = 2*w[0], maxval = 2*w[1]).numpy()
t_test  = tf.random.uniform(shape=[num_testing_samples], minval=T[0], maxval = T[1]).numpy()

xt_test = tf.stack((scale(x_test, w), scale(t_test, T)), axis=1)

u_exact = tf_sol(x_test, t_test)

for r in range(len(rho)):
    print('Weight PDE Loss: ', rho[r])
    for k in range(num_points):
        # model
        model = tf.keras.Sequential([
         tf.keras.layers.Dense(5, input_shape=(2,), activation=tf.nn.tanh),
         tf.keras.layers.Dense(5, activation=tf.nn.tanh),
         tf.keras.layers.Dense(5, activation=tf.nn.tanh),
         tf.keras.layers.Dense(1)
        ])


        trainable_variables = [model.variables]

        @tf.function
        def PDE(x, t):
            with tf.GradientTape(persistent = True) as tape:
                tape.watch(x)
                tape.watch(t)
                u = model(tf.stack((x,t),axis=1))
                u_x = tape.gradient(u, x)
                u_t = tape.gradient(u, t)
                u_xx = tape.gradient(u_x, x)

                Iion = tf.squeeze(u * (u - 1.0) * (u - alpha))

            return u_t/(T[1]-T[0]) - 2*D*u_xx /w2+ K*Iion/2



        last_loss_fit = tf.constant([0.0])
        def loss_fit():
             global last_loss_fit
             last_loss_fit = tf.reduce_mean(tf.square(model(xt_train) - u_train[:,None]))
             return last_loss_fit

        last_loss_PDE = tf.constant([0.0])
        def loss_PDE():
             global last_loss_PDE
             last_loss_PDE = tf.reduce_mean(tf.square(PDE(tf.constant(x_grid), tf.constant(t_grid))))
             return last_loss_PDE


        #%%%%%%%%%%%%%%%%%% Training
        # initialize optimizer
        opt = tf.keras.optimizers.Adam(learning_rate = 10e-3)

        # optimization loop
        for i in range(num_epochs):
            opt.minimize(lambda: loss_fit() + rho[r]*loss_PDE(), trainable_variables)


        # Measure the MSE


        u_NN = model(xt_test)

        error[r,k] = tf.sqrt(tf.reduce_mean(tf.square(u_exact[:,None] - u_NN)))

        print('Point: ', k + 1)


# Plot the results in a box plot


plt.boxplot(np.transpose(error))
plt.ylim([-0.1, 1.1])
plt.xlabel('Weight of the PDE loss function')
plt.xticks(range(1,len(rho)+1), rho/4)
textstr = '\n'.join(('Standard', r'K =%.2f' % K, r'D =%.2f' % D))
plt.title(textstr)

#plt.show()
plt.savefig('figure/standard_'+str(D)+'_'+str(K)+'.png')

# Last Output

print('K: ', K, ' D: ', D)
#print('Best rho for average error: ', rho[np.argmin(mean_H_1)])
#print('Best rho for min error: ', rho[np.argmin(min_H_1)])
