num_epochs = 5000

num_training_samples = 10
num_testing_samples  = 5000
num_collocation_points = 3000
num_points = 10

D_exact = 5.0

exponentials = range(6)

rho = [10**(-x) for x in exponentials]
rho.append(0.0)
