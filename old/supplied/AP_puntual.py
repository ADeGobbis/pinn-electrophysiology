#!/usr/bin/env python3

#####################################################################################


import numpy as np
import matplotlib.pyplot as plt

# diffusivity,
epsilon_0 = 0.002
c_1 = 0.2
c_2 = 0.3
beta = 0.15

Tcool = 0.0
Thot = 1.0
K = 8.0    # 12.9 -> AP model

#dt = dx2 * dy2 / (2 * D * (dx2 + dy2))  to have the stability?
dt = 0.005 # 0.1 ms real_t = 12.9 * t
tf = 30.0
nt = int(tf/dt)

u_fdm = np.zeros((nt+1,2))

# Initial conditions - ring of inner radius r, width dr centred at (cx,cy) (mm)
# Actually a circle centered there not a ring

u_fdm[0,0] = 0.5

def do_timestep(u0):
    # Propagate with forward-difference in time, central-difference in space
    u = np.zeros(2)
    u[0] = u0[0] -  dt * K * (u0[0]*(u0[0]-0.1)*(u0[0]-1)) -  u0[1]*u0[0]
    u[1] = u0[1] + dt* ((epsilon_0 + (c_1*u0[1])/(c_2 + u0[0]))*( - u0[1] - K*u0[0]*(u0[0] - beta -1)))
    return u

# Number of timesteps
for k in range(nt):
    u_fdm[k+1,:] = do_timestep(u_fdm[k,:])

# Output 4 figures at these timesteps
fig = plt.figure()
ax = fig.add_subplot(211)
ax.plot(dt*np.arange(nt+1), u_fdm[:,0])
ax.set_ylabel('Potential')
ax = fig.add_subplot(212)
ax.plot(dt*np.arange(nt+1), u_fdm[:,1])
ax.set_ylabel('Gating')

#plt.show()

plt.savefig("../../figure/tests/FDM_puntual.png")
