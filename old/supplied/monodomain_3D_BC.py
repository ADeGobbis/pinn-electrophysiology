#!/usr/bin/env python3

###############################################################################
# Consider the monodomain equation:
# xi*[Cm*u_t + I_{ion}] = div(Dm*grad(u)),  in R^3
# where I_{ion} is assembled using a LUT from the ten Tusscher-Panfilov ionic model.
#
# The input of this code is a numerical solution of the problem above.
#
# Goal of this code is to identify the value of the conductivities starting
# from a collection of data ((x,y,z,t), u(x,y,z,t)).
###############################################################################

import numpy as np
import tensorflow as tf
import pandas as pd
import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#%%%%%%%%%%%%%%%%%% options
xi_exact                = 1.0
xi_guess                = 1.0        # not used now
cm_exact                = 1.0
cm_guess                = 1.0        # not used now
sigmal_exact            = 0.5714e-4
sigmal_guess            = 0.9865e-4
sigmat_exact            = 0.0635e-4
sigmat_guess            = 0.0811e-4
sigman_exact            = 0.0635e-4
sigman_guess            = 0.0435e-4
num_collocation_points  = 200
num_epochs              = 1000
learning_rate           = 1e-2

filepath = "solution.csv"
df = pd.read_csv(filepath, sep = ",")

#%%%%%%%%%%%%%%%%%% Initialization
#inizialize random generators for reproducibility
np.random.seed(2)
tf.random.set_seed(2)

# numerical solution
x_train = df["x"]
y_train = df["y"]
z_train = df["z"]
t_train = df["t"]
u_train = df["u"]
'''
tags      = df["tags"] # TODO: to be exported.
Nx        = df["N_x"] # TODO: to be exported.
Ny        = df["N_y"] # TODO: to be exported.
Nz        = df["N_z"] # TODO: to be exported.
x_train_bdry = []
y_train_bdry = []
z_train_bdry = []
for index in range(len(tags)):
    if (tags[index] == "endo" or tags[index] == "epi" or tags[index] == "base"):
        x_train_bdry = np.append(x_train_bdry, x_train[index])
        y_train_bdry = np.append(y_train_bdry, y_train[index])
        z_train_bdry = np.append(z_train_bdry, z_train[index])
'''
xyzt_train = tf.stack((x_train, y_train, z_train, t_train), axis=1)
space_dim = xyzt_train.shape[1] - 1

# collocation points (for the sake of simplicity, gaussian noise added to the training points)
# fibers (f0) and crossfibers (n0) are assigned on collocation points with three constant vectors (to be changed)
selected_collocation_points = np.random.choice(len(x_train), size=num_collocation_points, replace=False)
x_grid = x_train[selected_collocation_points] + tf.random.normal(shape=[num_collocation_points]).numpy()
y_grid = y_train[selected_collocation_points] + tf.random.normal(shape=[num_collocation_points]).numpy()
z_grid = z_train[selected_collocation_points] + tf.random.normal(shape=[num_collocation_points]).numpy()
x_bdry_grid = x_train[selected_collocation_points] + tf.random.normal(shape=[num_collocation_points]).numpy() # TODO: should be x_train_bdry
y_bdry_grid = y_train[selected_collocation_points] + tf.random.normal(shape=[num_collocation_points]).numpy() # TODO: should be x_train_bdry
z_bdry_grid = z_train[selected_collocation_points] + tf.random.normal(shape=[num_collocation_points]).numpy() # TODO: should be x_train_bdry
t_grid = t_train[selected_collocation_points] + tf.random.normal(shape=[num_collocation_points]).numpy()

f0 = tf.repeat([[1.0], [0.0], [0.0]], num_collocation_points, axis=1)
n0 = tf.repeat([[0.0], [0.0], [1.0]], num_collocation_points, axis=1)
# TODO: Nx, Ny, Nz and tags must be read from file
Nx = tf.repeat(0.0, num_collocation_points, axis=0)
Ny = tf.repeat(0.0, num_collocation_points, axis=0)
Nz = tf.repeat(0.0, num_collocation_points, axis=0)
tags = tf.repeat("myo", num_collocation_points, axis=0)

# model
model = tf.keras.Sequential([
    tf.keras.layers.Dense(10, input_shape=(4,), activation=tf.nn.tanh),
    tf.keras.layers.Dense(10, activation=tf.nn.tanh),
    tf.keras.layers.Dense(10, activation=tf.nn.tanh),
    tf.keras.layers.Dense(10, activation=tf.nn.tanh),
    tf.keras.layers.Dense(1)
])

sigmal = tf.Variable(sigmal_guess, dtype=tf.float32)
sigmat = tf.Variable(sigmat_guess, dtype=tf.float32)
sigman = tf.Variable(sigman_guess, dtype=tf.float32)

trainable_variables = [model.variables, sigmal, sigmat, sigman]

def buildDmandDmgradu(u_x, u_y, u_z):
    f0tensorf0 = [[f0[0, :] * f0[0, :], f0[0, :] * f0[1, :], f0[0, :] * f0[2, :]], \
                  [f0[1, :] * f0[0, :], f0[1, :] * f0[1, :], f0[1, :] * f0[2, :]], \
                  [f0[2, :] * f0[0, :], f0[2, :] * f0[1, :], f0[2, :] * f0[2, :]]]
    n0tensorn0 = [[n0[0, :] * n0[0, :], n0[0, :] * n0[1, :], n0[0, :] * n0[2, :]], \
                  [n0[1, :] * n0[0, :], n0[1, :] * n0[1, :], n0[1, :] * n0[2, :]], \
                  [n0[2, :] * n0[0, :], n0[2, :] * n0[1, :], n0[2, :] * n0[2, :]]]
    # f0tensorf0/n0tensorn0 shape: (3, 3, num_collocation_points)
    f0tensorf0 = tf.convert_to_tensor(f0tensorf0)
    n0tensorn0 = tf.convert_to_tensor(n0tensorn0)

    # I shape: (3, 3, num_collocation_points)
    I = tf.eye(space_dim)
    I = tf.reshape(I, [space_dim, space_dim, 1])
    I = tf.repeat(I, num_collocation_points, axis=2)

    Dm = sigmat * I + (sigmal - sigmat) * f0tensorf0 + (sigman - sigmat) * n0tensorn0

    Dmgradu = [[Dm[0, 0, :] * u_x + Dm[0, 1, :] * u_y + Dm[0, 2, :] * u_z], \
               [Dm[1, 0, :] * u_x + Dm[1, 1, :] * u_y + Dm[1, 2, :] * u_z], \
               [Dm[2, 0, :] * u_x + Dm[2, 1, :] * u_y + Dm[2, 2, :] * u_z]]

    return Dm, Dmgradu

# loss functions
def PDE(x, y, z, t):
    with tf.GradientTape(persistent=True) as tape:
        tape.watch(x)
        tape.watch(y)
        tape.watch(z)
        tape.watch(t)
        u = model(tf.stack((x,y,z,t), axis=1))
        u_x = tape.gradient(u, x)
        u_y = tape.gradient(u, y)
        u_z = tape.gradient(u, z)

        Dm, Dmgradu = buildDmandDmgradu(u_x, u_y, u_z)

        Dmgradu_x = tape.gradient(Dmgradu, x)
        Dmgradu_y = tape.gradient(Dmgradu, y)
        Dmgradu_z = tape.gradient(Dmgradu, z)

        u_t = tape.gradient(u, t)

        # Fitzhugh-Nagumo cubic ionic current.
        Iion = u * (u - 1.0) * (u - 0.1)

    return xi_exact * (cm_exact * u_t + Iion) - Dmgradu_x - Dmgradu_y - Dmgradu_z

def BCN(x, y, z, t, Nx, Ny, Nz):
    with tf.GradientTape(persistent=True) as tape:
        tape.watch(x)
        tape.watch(y)
        tape.watch(z)
        tape.watch(t)
        u = model(tf.stack((x,y,z,t), axis=1))
        u_x = tape.gradient(u, x)
        u_y = tape.gradient(u, y)
        u_z = tape.gradient(u, z)

        Dm, Dmgradu = buildDmandDmgradu(u_x, u_y, u_z)

        return Dmgradu[0] * Nx + Dmgradu[1] * Ny + Dmgradu[2] * Nz

last_loss_fit = tf.constant([0.0])
def loss_fit():
    u_NN  = model(xyzt_train)
    global last_loss_fit
    last_loss_fit = tf.reduce_mean(tf.square(u_NN - u_train[:,None]))
    return last_loss_fit
last_loss_PDE = tf.constant([0.0])
def loss_PDE():
    global last_loss_PDE
    last_loss_PDE = tf.reduce_mean(tf.square(PDE(tf.constant(x_grid), tf.constant(y_grid), tf.constant(z_grid), tf.constant(t_grid))))
    return last_loss_PDE

last_loss_BCN = tf.constant([0.0])
def loss_BCN():
    global last_loss_BCN
    last_loss_BCN = tf.reduce_mean(tf.square(BCN(tf.constant(x_bdry_grid), tf.constant(y_bdry_grid), tf.constant(z_bdry_grid), tf.constant(t_grid), tf.constant(Nx), tf.constant(Ny), tf.constant(Nz))))
    return last_loss_BCN

#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
opt = tf.keras.optimizers.Adam(learning_rate = learning_rate)

# optimization loop
ts_ini = time.time()
for i in range(num_epochs):
    opt.minimize(lambda: loss_fit() + loss_PDE() + loss_BCN(), trainable_variables)
    print('iter = %d, sigmal = %f, sigmat = %f, sigman = %f, loss_fit = %f, loss_PDE = %f' %
          (i, sigmal.numpy(), sigmat.numpy(), sigman.numpy(), last_loss_fit.numpy(), last_loss_PDE.numpy()))

print('elapsed time: %1.2f s' % (time.time() - ts_ini))
print('estimated sigmal:   %f' % sigmal.numpy())
print('estimated sigmat:   %f' % sigmat.numpy())
print('estimated sigman:   %f' % sigman.numpy())
print('relative error for sigmal: %1.2e' % (abs(sigmal.numpy() - sigmal_exact)/sigmal_exact))
print('relative error for sigmat: %1.2e' % (abs(sigmat.numpy() - sigmat_exact)/sigmat_exact))
print('relative error for sigman: %1.2e' % (abs(sigman.numpy() - sigman_exact)/sigman_exact))

#%%%%%%%%%%%%%%%%%% Post-processing
