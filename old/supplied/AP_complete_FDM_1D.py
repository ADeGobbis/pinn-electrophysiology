#!/usr/bin/env python3

#####################################################################################


import numpy as np
import matplotlib.pyplot as plt

# plate size, mm
w = 100
# intervals in x-, y- directions, mm
dx = 0.5
# diffusivity,
D = 5.0  #mm/ms  12.9 -> AP model
epsilon_0 = 0.018
c_1 = 0.2
c_2 = 0.3
beta = 0.15

Tcool = 0.0
Thot = 1.0
K = 8.0    # 12.9 -> AP model

nx = int(w/dx)

dx2= dx*dx
#dt = dx2 * dy2 / (2 * D * (dx2 + dy2))  to have the stability?
dt = 0.001 # 0.1 ms real_t = 12.9 * t

u0 = Tcool * np.ones((nx+1,2))
u = u0.copy()

# Initial conditions - ring of inner radius r, width dr centred at (cx,cy) (mm)
# Actually a circle centered there not a ring
r, cx = 25, 50
r2 = r**2
for i in range(nx):
    p2 = (i*dx-cx)**2
    if p2 < r2:
        u0[i,0] = Thot

# Advance time step
def do_timestep(u0, u):
    u[1:-1,0] = u0[1:-1,0] + D * dt * (u0[2:,0] - 2.0*u0[1:-1,0] + u0[:-2,0])/dx2 - dt * K * (u0[1:-1,0]*(u0[1:-1,0]-0.1)*(u0[1:-1,0]-1.0)) - u0[1:-1,1]*u0[1:-1,0]

    u[1:-1,1] = u0[1:-1,1] + dt* ((epsilon_0 + (c_1*u0[1:-1,1])/(c_2 + u0[1:-1, 0]))*( - u0[1:-1, 1] - K*u0[1:-1,0]*(u0[1:-1,0] - beta -1)))
    u0 = u.copy()
    return u0, u

# Number of timesteps
nsteps = 2000
# Output 4 figures at these timesteps
mfig = [0, 100, 500, 1000]
fignum = 0
fig = plt.figure()
for m in range(nsteps):
    u0, u = do_timestep(u0, u)
    if m in mfig:
        fignum += 1
        print(m, fignum)
        ax = fig.add_subplot(220 + fignum)
        ax.plot(dx*np.arange(nx+1), u0[:,0])
        ax.set_title('{:.1f} ms'.format(m*dt))
fig.subplots_adjust(right=0.85)
#plt.show()

plt.savefig("../../figure/tests/FDM_complete.png")
