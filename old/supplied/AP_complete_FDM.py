#!/usr/bin/env python3

#####################################################################################


import numpy as np
import matplotlib.pyplot as plt

# plate size, mm
w = h = 100
# intervals in x-, y- directions, mm
dx = dy = 0.5
# diffusivity,
D = 5.0  #mm/ms  12.9 -> AP model
epsilon_0 = 0.018
c_1 = 0.2
c_2 = 0.3
beta = 0.15

Tcool = 0.0
Thot = 1.0
K = 8.0    # 12.9 -> AP model

nx, ny = int(w/dx), int(h/dy)

dx2, dy2 = dx*dx, dy*dy
#dt = dx2 * dy2 / (2 * D * (dx2 + dy2))  to have the stability?
dt = 0.001 # 0.1 ms real_t = 12.9 * t

u0 = Tcool * np.ones((nx, ny,2))
u = u0.copy()

# Initial conditions - ring of inner radius r, width dr centred at (cx,cy) (mm)
# Actually a circle centered there not a ring
r, cx, cy = 25, 50, 50
r2 = r**2
for i in range(nx):
    for j in range(ny):
        p2 = (i*dx-cx)**2 + (j*dy-cy)**2
        if p2 < r2:
            u0[i,j,0] = Thot

def do_timestep(u0, u):
    # Propagate with forward-difference in time, central-difference in space
    u[1:-1, 1:-1,0] = u0[1:-1, 1:-1,0] + D * dt * (
          (u0[2:, 1:-1,0] - 2.0*u0[1:-1, 1:-1,0] + u0[:-2, 1:-1,0])/dx2
          + (u0[1:-1, 2:,0] - 2.0*u0[1:-1, 1:-1,0] + u0[1:-1, :-2,0])/dy2 ) - dt * K * (u0[1:-1, 1:-1,0]*(u0[1:-1, 1:-1,0]-0.1)*(u0[1:-1, 1:-1,0]-1.0)) -  u0[1:-1, 1:-1,1]*u0[1:-1, 1:-1,0]
    u[1:-1, 1:-1,1] = u0[1:-1, 1:-1,1] + dt* ((epsilon_0 + (c_1*u0[1:-1, 1:-1,1])/(c_2 + u0[1:-1, 1:-1,0]))*( - u0[1:-1, 1:-1,1] - K*u0[1:-1, 1:-1,0]*(u0[1:-1, 1:-1,0] - beta -1)))
    u0 = u.copy()
    return u0, u

# Number of timesteps
nsteps = 20000
# Output 4 figures at these timesteps
mfig = [0, 10, 50, 100]
fignum = 0
fig = plt.figure()
for m in range(nsteps):
    u0, u = do_timestep(u0, u)
    if m in mfig:
        fignum += 1
        print(m, fignum)
        ax = fig.add_subplot(220 + fignum)
        im = ax.imshow(u[:,:,0].copy(), cmap=plt.get_cmap('hot'), vmin=Tcool,vmax=Thot)
        ax.set_axis_off()
        ax.set_title('{:.1f} ms'.format(m*dt*12.9))
fig.subplots_adjust(right=0.85)
cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.7])
cbar_ax.set_xlabel('V', labelpad=20)
fig.colorbar(im, cax=cbar_ax)
#plt.show()

plt.savefig("../../figure/tests/FDM_complete.png")
