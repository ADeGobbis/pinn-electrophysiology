#!/usr/bin/env python3
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


w = 1.0
T = 3.0

num_epochs = 15000

num_boundary_samples = 1000
num_collocation_points = 5000
mu_exact = 2.0
mu_guess = 1.0



def boundary(x):
    y = x * (w + 2*T)
    if y < T:
        return 0.0, T - y
    elif y < w + T:
        return y - T, 0.0
    else:
        return w, y - T - w


tf.random.set_seed(100)

unif = tf.random.uniform(shape = [num_boundary_samples], maxval = 1.0)

sol_exact = lambda x, t: np.exp(-mu_exact*(np.pi**2)*t)*np.sin(np.pi*x/w)

b_points = tf.stack(tf.map_fn(fn = lambda x: boundary(x), elems=unif, dtype = (tf.float32, tf.float32)), axis=1)
u_train = tf.constant(sol_exact(b_points[:,0], b_points[:,1]))

#print(u_train)
#print(b_points)


#collocation points
x_grid  = tf.random.uniform(shape=[num_collocation_points], maxval = w).numpy()
t_grid  = tf.random.uniform(shape=[num_collocation_points], maxval = T).numpy()


# model
model = tf.keras.Sequential([
    tf.keras.layers.Dense(5, input_shape=(2,), activation=tf.nn.tanh),
    tf.keras.layers.Dense(5, activation=tf.nn.tanh),
    tf.keras.layers.Dense(5, activation=tf.nn.tanh),
    tf.keras.layers.Dense(1)
])

mu = mu_exact

trainable_variables = [model.variables]

# loss functions
def PDE(x, t):
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        tape.watch(t)
        u = model(tf.stack((x,t),axis=1))
        u_x = tape.gradient(u, x)
        u_t = tape.gradient(u, t)
        u_xx = tape.gradient(u_x, x)
    return u_t - mu*(u_xx) #- tf.reshape(u,(num_collocation_points,))

last_loss_fit = tf.constant([0.0])
def loss_fit():
    global last_loss_fit
    last_loss_fit = tf.reduce_mean(tf.square(model(b_points) - u_train[:,None]))
    return last_loss_fit
last_loss_PDE = tf.constant([0.0])
def loss_PDE():
    global last_loss_PDE
    last_loss_PDE = tf.reduce_mean(tf.square(PDE(tf.constant(x_grid), tf.constant(t_grid))))
    return last_loss_PDE


#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
opt = tf.keras.optimizers.Adam(learning_rate = 10e-4)

# optimization loop
for i in range(num_epochs):
    opt.minimize(lambda: loss_fit() + loss_PDE(), trainable_variables)
    if i % 100 == 0:
        print('iter = %d, mu = %f, loss_fit = %f, loss_PDE = %f' %
              (i, mu, last_loss_fit.numpy(), last_loss_PDE.numpy()))


#%%%%%%%%%%%%%%%%%% Post-processing
t_test = [0.0, 0.01, 0.02, 0.05]
fig = plt.figure()
fignum = 1
xx = np.linspace(0,w, num=1000, dtype=np.float32)
for i in range(len(t_test)):
    test_points = tf.stack((xx, np.full((1000), t_test[i])), axis = 1)
    ax = fig.add_subplot(220 + fignum)
    ax.plot(xx,  model(test_points).numpy(), 'r-')
    ax.plot(xx, sol_exact(test_points[:,0], test_points[:,1]), 'k-')
    ax.set_title('{:.1f} ms'.format(t_test[i]*1000))
    ax.set_ylim([-0.1, 1.5])
    fignum = fignum + 1
plt.tight_layout(h_pad=2)
#plt.show()
plt.savefig("../../figure/forward_1D/parabolic_linear.png")
