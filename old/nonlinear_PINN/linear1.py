#!/usr/bin/env python3

###############################################################################
# Consider the elliptic equation:
# -mu*u_xx - u = 0,  in R
#
# A solution (among the infinitely many) is given by:
# u(x) = sin(x/sqrt(mu))
#
# Goal of this code is to identify the value of mu starting from a collection
# of pairs (x, u(x)).
###############################################################################

import numpy as np
import tensorflow as tf
import time
import matplotlib.pyplot as plt

#%%%%%%%%%%%%%%%%%% options
mu_exact                = 0.2
mu_guess                = 0.
num_training_samples    = 200
num_collocation_points  = 100
num_epochs              = 1000
learning_rate           = 1e-2

#%%%%%%%%%%%%%%%%%% Initialization
#inizialize random generators for reproducibility
np.random.seed(1)
tf.random.set_seed(1)

# exact solution (used to generate the training samples)
u_ex = lambda x: np.sin(x/np.sqrt(mu_exact))
x_train  = tf.constant([0.0, 1.0])
u_train  = u_ex(x_train)

# collocation points
x_grid = np.linspace(np.min(x_train),np.max(x_train), num=num_collocation_points, dtype=np.float32)

# model
model = tf.keras.Sequential([
    tf.keras.layers.Dense(4, input_shape=(1,), activation=tf.nn.tanh),
    tf.keras.layers.Dense(1)
])

#mu = tf.Variable(mu_guess)
mu = tf.constant(mu_exact)

trainable_variables = [model.variables]

# loss functions: fit is on the u, PDE is on the f
def PDE(x_point):
    x = tf.constant(x_point)
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        u = model(tf.reshape(x,(num_collocation_points,1)))
        u_x = tape.gradient(u, x)
        u_xx = tape.gradient(u_x, x)
    return -mu*u_xx-tf.reshape(u,(num_collocation_points,))

last_loss_fit = tf.constant([0.0])
def loss_fit():
    global last_loss_fit
    last_loss_fit = tf.reduce_mean(tf.square(model(x_train[:,None]) - u_train[:,None]))
    return last_loss_fit
last_loss_PDE = tf.constant([0.0])
def loss_PDE():
    global last_loss_PDE
    last_loss_PDE = tf.reduce_mean(tf.square(PDE(x_grid)))
    return last_loss_PDE

#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
opt = tf.keras.optimizers.Adam(learning_rate = learning_rate)

# optimization loop
ts_ini = time.time()
for i in range(num_epochs):
    opt.minimize(lambda: loss_fit() + loss_PDE(), trainable_variables)
    print('iter = %d, mu = %f, loss_fit = %f, loss_PDE = %f' %
          (i, mu.numpy(), last_loss_fit.numpy(), last_loss_PDE.numpy()))

print('elapsed time: %1.2f s' % (time.time() - ts_ini))
print('estimated mu:   %f' % mu.numpy())
print('relative error: %1.2e' % (abs(mu.numpy() - mu_exact)/mu_exact))

#%%%%%%%%%%%%%%%%%% Post-processing
fig, ax = plt.subplots(nrows=1, ncols=1)
xx = np.linspace(0.0, 1.0, num=1000, dtype=np.float32)
ax.plot(xx,  model(xx[:,None]).numpy(), 'r-')
ax.plot(xx, u_ex(xx), 'k-')
ax.scatter(x_train,  u_train)
ax.legend(('learned solution','training points'))
#plt.show()
plt.savefig("../../figure/forward_1D/linear1.png")

#%%%%%%%%%%%%%%%%%% MSE on the test set

MSE = np.mean((np.reshape(u_ex(xx),(1000,1)) -  model(xx[:,None]).numpy())**2)
#print((np.reshape(u_ex(xx),(1000,1)) -  model(xx[:,None]).numpy()).shape)
print("The measured error on a test set is: ",MSE)
