#!/usr/bin/env python3


#Creates the random points in the 2 + 1 dimensions

import sys

import numpy as np
import tensorflow as tf
import time
import matplotlib.pyplot as plt


if len(sys.argv) >= 2:
    rho = float(sys.argv[1])
else:
    rho = 1.0

#Parameters of the model
num_training_samples = 10000
num_testing_samples = 1000
num_epochs              = 5000
learning_rate           = 1e-3
num_collocation_points  = 1000

K_guess = 0.0
D_guess = 10.0


#Parameters of the problem

# plate size, mm
w = h = 100
# intervals in x-, y- directions, mm
dx = dy = 0.5
# diffusivity,
D_true = 12.9*0.25  #mm/ms  12.9 -> AP model

#Time horizon
time_max = 2.0

dt = 0.01

nx, ny, nt = int(w/dx), int(h/dy), int(time_max/dt)

dx2, dy2 = dx*dx, dy*dy

Tcool = 0.0
Thot = 1.0
K_true = 12.9 * 2.0    # 12.9 -> AP model

tf.random.set_seed(240)
np.random.seed(1)

#Initialization for the FDM

u_train = np.zeros((num_training_samples))
u_fdm   = np.full((nx + 1, ny + 1, nt + 1), Tcool)
#print(u_fdm.shape)

#Initial condition
r, cx, cy = 25, 50, 50
r2 = r**2
for i in range(nx + 1):
    for j in range(ny + 1):
        p2 = (i*dx-cx)**2 + (j*dy-cy)**2
        if p2 < r2:
            u_fdm[i,j,0] = Thot

def do_timestep(u0):
    # Propagate with forward-difference in time, central-difference in space
    u = np.full((nx + 1, ny + 1), Tcool)
    u[1:-1, 1:-1] = u0[1:-1, 1:-1] + D_true * dt * (
          (u0[2:, 1:-1] - 2.0*u0[1:-1, 1:-1] + u0[:-2, 1:-1])/dx2
          + (u0[1:-1, 2:] - 2.0*u0[1:-1, 1:-1] + u0[1:-1, :-2])/dy2 ) + dt * K_true * np.multiply(np.multiply(u0[1:-1, 1:-1],(u0[1:-1, 1:-1]-0.1)),(1.0 - u0[1:-1, 1:-1]))

    return u


x_train = tf.random.uniform(shape=[num_training_samples], maxval=w).numpy()
y_train = tf.random.uniform(shape=[num_training_samples], maxval=h).numpy()
t_train = np.sort(tf.random.uniform(shape=[num_training_samples], maxval=time_max).numpy())


def lin_interpolate(f, index, increment, point):
    #Linearly interpolate the 3D function goven the values in a grid around the point
    #print(f.shape)
    coord   = (point - np.multiply(index, increment))/increment
    i_coord = np.ones(3) - coord

    return f[1,1,1]*coord[0]*coord[1]*coord[2] \
        + f[0,1,1]*i_coord[0]*coord[1]*coord[2] \
        + f[1,0,1]*coord[0]*i_coord[1]*coord[2] \
        + f[1,1,0]*coord[0]*coord[1]*i_coord[2] \
        + f[0,0,1]*i_coord[0]*i_coord[1]*coord[2] \
        + f[0,1,0]*i_coord[0]*coord[1]*i_coord[2] \
        + f[1,0,0]*coord[0]*i_coord[1]*i_coord[2] \
        + f[0,0,0]*i_coord[0]*i_coord[1]*i_coord[2]


for k in range(nt):
    u_fdm[:,:,k+1] = do_timestep(u_fdm[:,:,k])
    #Trova i valori di t_train che stanno qui in mezzo e approssima
    for m in range(0, num_training_samples):
        if t_train[m] < (k+1)*dt and t_train[m] >= k*dt:
            i, j = int(x_train[m]/dx), int(y_train[m]/dy)
            u_train[m] = lin_interpolate(u_fdm[i:(i+2), j:(j+2), k:(k+2)], (i,j,k), (dx,dy,dt), np.array([x_train[m], y_train[m], t_train[m]]))

#print(u_train)


#Train on the grid
#xi = np.random.choice(nx + 1, size=num_training_samples)
#yi = np.random.choice(ny + 1, size=num_training_samples)
#ti = np.random.choice(nt + 1, size=num_training_samples)

#u_train = u_fdm[xi,yi,ti]

#xyt_train = tf.stack((xi*dx, yi*dy, ti*dt), axis=1)


##Now we check train the neural network with the data we have

xyt_train = tf.stack((x_train, y_train, t_train), axis=1)
space_dim = xyt_train.shape[1] - 1

# collocation points (for the sake of simplicity, gaussian noise added to the training points) but can be any grid I want
x_grid  = x_train[np.random.choice(len(x_train), size=num_collocation_points, replace=True)] + tf.random.normal(shape=[num_collocation_points]).numpy()
y_grid  = y_train[np.random.choice(len(y_train), size=num_collocation_points, replace=True)] + tf.random.normal(shape=[num_collocation_points]).numpy()
t_grid  = t_train[np.random.choice(len(t_train), size=num_collocation_points, replace=True)] + tf.random.normal(shape=[num_collocation_points]).numpy()

# model
model = tf.keras.Sequential([
    tf.keras.layers.Dense(10, input_shape=(3,), activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(10, activation=tf.nn.tanh),
    tf.keras.layers.Dense(1)
])

#K = tf.Variable(K_guess, dtype=tf.float32)
#D = tf.Variable(D_guess, dtype=tf.float32)

K = K_true
D = D_true

trainable_variables = [model.variables]

# loss functions
def PDE(x, y, t):
    with tf.GradientTape(persistent=True) as tape:
        tape.watch(x)
        tape.watch(y)
        tape.watch(t)
        u = model(tf.stack((x,y,t), axis=1))
        u_x = tape.gradient(u, x)
        u_y = tape.gradient(u, y)

        u_xx = tape.gradient(u_x, x)
        u_yy = tape.gradient(u_y, y)

        u_t = tape.gradient(u, t)

        # Fitzhugh-Nagumo cubic ionic current.
        Iion = tf.reshape(u * (u - 1.0) * (u - 0.1), shape = (num_collocation_points,))
        res = u_t  - D*(u_xx + u_yy) + K*Iion


    return tf.where(tf.reshape((u >= 0.0) & (u <= 1.0), (num_collocation_points,)), res, np.full((num_collocation_points,), 10e4))

last_loss_fit = tf.constant([0.0])
def loss_fit():
    u_NN  = model(xyt_train)
    global last_loss_fit
    last_loss_fit = tf.reduce_mean(tf.square(u_NN - u_train[:,None]))
    return last_loss_fit

last_loss_PDE = tf.constant([0.0])
def loss_PDE():
    global last_loss_PDE
    last_loss_PDE = tf.reduce_mean(tf.square(PDE(tf.constant(x_grid), tf.constant(y_grid), tf.constant(t_grid))))
    return last_loss_PDE

#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
opt = tf.keras.optimizers.Adam(learning_rate = learning_rate)

#print(model.variables)


# optimization loop
ts_ini = time.time()
for i in range(num_epochs):
    opt.minimize(lambda: loss_fit() + rho*loss_PDE(), trainable_variables)
    print('iter = %d, loss_fit = %f, loss_PDE = %f' %
          (i, last_loss_fit.numpy(), last_loss_PDE.numpy()))

print('elapsed time: %1.2f s' % (time.time() - ts_ini))
#print(model.variables)

#Check the error of the model on a random subset of the FDM t_grid

xi = np.random.choice(nx + 1, size=num_testing_samples)
yi = np.random.choice(ny + 1, size=num_testing_samples)
ti = np.random.choice(nt + 1, size=num_testing_samples)

u_test = u_fdm[xi,yi,ti]


#xyt_test = tf.stack((xi*dx, yi*dy, ti*dt), axis=1)
xyt_test = tf.stack((xi*dx, yi*dy, ti*dt), axis=1)

u_NN_test = model(xyt_test).numpy()

u_NN_train = model(xyt_train)

#test_MSE = tf.reduce_mean(tf.square(u_NN_train - u_train[:,None]))
test_MSE = np.mean((u_NN_test - np.reshape(u_test[:,None], (num_testing_samples,1)))**2)
#print((u_NN_test - np.reshape(u_test[:,None], (num_testing_samples,1))))
print("Error on the test set: %f" % test_MSE)

error = np.sqrt(np.array(tf.square(u_NN_test - u_test[:,None])))
#error = np.array(u_NN_test - u_test[:,None])

#Plot the Error
fig = plt.figure()
#plt.title("Penalized loss", loc = 'center', y=1.0)
ax = fig.add_subplot(221, projection='3d')
ax.scatter(xyt_test[:,0], xyt_test[:,1], xyt_test[:,2], c = -error, alpha = 0.2)
#ax.colorbar()
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
ax2 = fig.add_subplot(222)
ax2.scatter(u_test, u_NN_test)
ax2.set_xlabel('real values')
ax2.set_ylabel('learned values')
#plt.savefig("../figure/temp_error.png")
print("Everything's fine")
#fig, ax = plt.subplots()
#im = ax.imshow(u_fdm[:,:,150], cmap=plt.get_cmap('hot'), vmin=Tcool,vmax=Thot)
#ax.set_axis_off()
#plt.savefig("testing_temp.png")

#Plot the heatmap at a certain time testing_temp

k_time = 150

u_exact = u_fdm[:, :, k_time]
u_model = np.zeros((nx + 1, ny + 1))

t = np.full((ny + 1), k_time*dt)

for i in range(nx + 1):
    x = np.full((ny + 1),dx*i)
    y = np.arange(ny + 1)*dy
    u_model[i,:] = np.reshape(model(tf.stack((x,y,t), axis=1)).numpy(), ny + 1)

#Plot the comparizon


#fig = plt.figure()
ax3 = fig.add_subplot(223)
im = ax3.imshow(u_exact.copy(), cmap=plt.get_cmap('hot'), vmin=Tcool - 0.5, vmax=Thot + 0.5)
ax3.set_axis_off()
ax3.set_xlabel('Exact')
ax4 = fig.add_subplot(224)
im = ax4.imshow(u_model.copy(), cmap=plt.get_cmap('hot'), vmin=Tcool - 0.5, vmax=Thot + 0.5)
ax4.set_axis_off()
ax4.set_xlabel('Learned')

fig.subplots_adjust(right=0.85)
cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.35])
cbar_ax.set_xlabel('V', labelpad=20)
fig.colorbar(im, cax=cbar_ax)

plt.suptitle('Penalized loss - rho=%f' % rho)
#plt.show()

plt.savefig("../figure/compare_test_5.png")
