#!/usr/bin/env python3
import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


#Creates the random points in the 2 + 1 dimensions

import sys

import numpy as np
import tensorflow as tf
import time
import matplotlib.pyplot as plt


if len(sys.argv) >= 2:
    rho = float(sys.argv[1])
else:
    rho = 1.0

#Parameters of the model
num_training_samples = 1000
num_testing_samples = 10000
num_epochs              = 2000
learning_rate           = 1e-3
num_collocation_points  = 5000

#Collocation band width
coll_width = 5.0


#Parameters of the problem

# plate size, mm
w = h = 100
# intervals in x-, y- directions, mm
dx = dy = 0.5
# diffusivity,
D_true = 12.9*0.25  #mm/ms  12.9 -> AP model

#Time horizon
time_max = 2.0

dt = 0.01

nx, ny, nt = int(w/dx), int(h/dy), int(time_max/dt)

dx2, dy2 = dx*dx, dy*dy

Tcool = 0.0
Thot = 1.0
K_true = 12.9 * 2.0    # 12.9 -> AP model

#tf.random.set_seed(240)
#np.random.seed(1)

#Initialization for the FDM

u_train = np.zeros((num_training_samples))
u_fdm   = np.full((nx + 1, ny + 1, nt + 1), Tcool)
#print(u_fdm.shape)

#Initial condition
r, cx, cy = 25, 50, 50
r2 = r**2
for i in range(nx + 1):
    for j in range(ny + 1):
        p2 = (i*dx-cx)**2 + (j*dy-cy)**2
        if p2 < r2:
            u_fdm[i,j,0] = Thot

def do_timestep(u0):
    # Propagate with forward-difference in time, central-difference in space
    u = np.full((nx + 1, ny + 1), Tcool)
    u[1:-1, 1:-1] = u0[1:-1, 1:-1] + D_true * dt * (
          (u0[2:, 1:-1] - 2.0*u0[1:-1, 1:-1] + u0[:-2, 1:-1])/dx2
          + (u0[1:-1, 2:] - 2.0*u0[1:-1, 1:-1] + u0[1:-1, :-2])/dy2 ) + dt * K_true * np.multiply(np.multiply(u0[1:-1, 1:-1],(u0[1:-1, 1:-1]-0.1)),(1.0 - u0[1:-1, 1:-1]))

    return u


x_train = tf.random.uniform(shape=[num_training_samples], maxval=w).numpy()
y_train = tf.random.uniform(shape=[num_training_samples], maxval=h).numpy()
t_train = np.sort(tf.random.uniform(shape=[num_training_samples], maxval=time_max).numpy())


def lin_interpolate(f, index, increment, point):
    #Linearly interpolate the 3D function goven the values in a grid around the point
    #print(f.shape)
    coord   = (point - np.multiply(index, increment))/increment
    i_coord = np.ones(3) - coord

    return f[1,1,1]*coord[0]*coord[1]*coord[2] \
        + f[0,1,1]*i_coord[0]*coord[1]*coord[2] \
        + f[1,0,1]*coord[0]*i_coord[1]*coord[2] \
        + f[1,1,0]*coord[0]*coord[1]*i_coord[2] \
        + f[0,0,1]*i_coord[0]*i_coord[1]*coord[2] \
        + f[0,1,0]*i_coord[0]*coord[1]*i_coord[2] \
        + f[1,0,0]*coord[0]*i_coord[1]*i_coord[2] \
        + f[0,0,0]*i_coord[0]*i_coord[1]*i_coord[2]


for k in range(nt):
    u_fdm[:,:,k+1] = do_timestep(u_fdm[:,:,k])
    #Trova i valori di t_train che stanno qui in mezzo e approssima
    for m in range(0, num_training_samples):
        if t_train[m] < (k+1)*dt and t_train[m] >= k*dt:
            i, j = int(x_train[m]/dx), int(y_train[m]/dy)
            u_train[m] = lin_interpolate(u_fdm[i:(i+2), j:(j+2), k:(k+2)], (i,j,k), (dx,dy,dt), np.array([x_train[m], y_train[m], t_train[m]]))

#print(u_train)



# Test points on the grid
#Train on the grid
xi = np.random.choice(nx + 1, size=num_testing_samples)
yi = np.random.choice(ny + 1, size=num_testing_samples)
ti = np.random.choice(nt + 1, size=num_testing_samples)

u_test = u_fdm[xi,yi,ti]

xyt_test = tf.stack((xi*dx, yi*dy, ti*dt), axis=1)


##Now we check train the neural network with the data we have

xyt_train = tf.stack((x_train, y_train, t_train), axis=1)
space_dim = 2

x_grid = tf.random.uniform(shape=[num_collocation_points], maxval=w).numpy()
y_grid = tf.random.uniform(shape=[num_collocation_points], maxval=h).numpy()
t_grid = np.sort(tf.random.uniform(shape=[num_collocation_points], maxval=time_max).numpy())


#print('r_grid: ',r_grid.shape)
#print('x_grid: ',x_grid.shape)
#print('y_grid: ',y_grid.shape)


#plt.plot(x_grid, t_grid, 'ko')
#plt.savefig('test.png')
# model

K = K_true
D = D_true


# loss functions
@tf.function
def PDE(x, y, t, u_model):
    with tf.GradientTape(persistent=True) as tape:
        tape.watch(x)
        tape.watch(y)
        tape.watch(t)
        u = u_model(tf.stack((x,y,t), axis=1))
        u_x = tape.gradient(u, x)
        u_y = tape.gradient(u, y)

        u_xx = tape.gradient(u_x, x)
        u_yy = tape.gradient(u_y, y)

        u_t = tape.gradient(u, t)

        # Fitzhugh-Nagumo cubic ionic current.
        Iion = tf.reshape(u * (u - 1.0) * (u - 0.1), shape = (num_collocation_points,))

    return u_t  - D*(u_xx + u_yy) + K*Iion


model_1 = tf.keras.Sequential([
    tf.keras.layers.Dense(10, input_shape=(3,), activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(10, activation=tf.nn.tanh),
    tf.keras.layers.Dense(1)
])

#K = tf.Variable(K_guess, dtype=tf.float32)
#D = tf.Variable(D_guess, dtype=tf.float32)

last_loss_fit = tf.constant([0.0])
def loss_fit():
    u_NN  = model_1(xyt_train)
    global last_loss_fit
    last_loss_fit = tf.reduce_mean(tf.square(u_NN - u_train[:,None]))
    return last_loss_fit

last_loss_PDE = tf.constant([0.0])
def loss_PDE():
    global last_loss_PDE
    last_loss_PDE = tf.reduce_mean(tf.square(PDE(tf.constant(x_grid, dtype=tf.float32), tf.constant(y_grid, dtype=tf.float32), tf.constant(t_grid, dtype=tf.float32), model_1)))
    return last_loss_PDE

#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
opt = tf.keras.optimizers.Adam(learning_rate = learning_rate)

#print(model.variables)


# optimization loop
ts_ini = time.time()
for i in range(num_epochs):
    opt.minimize(lambda: loss_fit() + rho*loss_PDE(), model_1.variables)
    if i % 1000 == 0:
        print('iter = %d, loss_fit = %f, loss_PDE = %f' %
              (i, last_loss_fit.numpy(), last_loss_PDE.numpy()))

print('elapsed time: %1.2f s' % (time.time() - ts_ini))
#print(model.variables)

#----------------------------------------------------------------------------------------------------

# Sample points again near the activation front
# Since the diffusion is iostropic the activation front is a circle for small t
def radius(a):
    i, = np.where(u_fdm[nx // 2, :, int(a/dt)] >= 0.5)
    return h/2 - i[0]*dy


t_grid = tf.random.uniform(shape=[num_collocation_points], maxval=time_max).numpy()
unif   = tf.random.uniform(shape=[num_collocation_points], minval= -coll_width/2, maxval= coll_width/2).numpy()
theta  = tf.random.uniform(shape=[num_collocation_points], maxval = 2*np.pi).numpy()
r_grid = np.array([radius(x) for x in t_grid])
x_grid = (r_grid + unif)*np.cos(theta) + w/2
y_grid = (r_grid + unif)*np.sin(theta) + h/2


model_2 = tf.keras.Sequential([
    tf.keras.layers.Dense(10, input_shape=(3,), activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(10, activation=tf.nn.tanh),
    tf.keras.layers.Dense(1)
])

#K = tf.Variable(K_guess, dtype=tf.float32)
#D = tf.Variable(D_guess, dtype=tf.float32)

last_loss_fit = tf.constant([0.0])
def loss_fit():
    u_NN  = model_2(xyt_train)
    global last_loss_fit
    last_loss_fit = tf.reduce_mean(tf.square(u_NN - u_train[:,None]))
    return last_loss_fit

last_loss_PDE = tf.constant([0.0])
def loss_PDE():
    global last_loss_PDE
    last_loss_PDE = tf.reduce_mean(tf.square(PDE(tf.constant(x_grid, dtype=tf.float32), tf.constant(y_grid, dtype=tf.float32), tf.constant(t_grid, dtype=tf.float32), model_2)))
    return last_loss_PDE

#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer

# optimization loop
ts_ini = time.time()
for i in range(num_epochs):
    opt.minimize(lambda: loss_fit() + rho*loss_PDE(), model_2.variables)
    if i % 1000 == 0:
        print('iter = %d, loss_fit = %f, loss_PDE = %f' %
              (i, last_loss_fit.numpy(), last_loss_PDE.numpy()))

print('elapsed time: %1.2f s' % (time.time() - ts_ini))
#print(model.variables)

print('\n')

u_NN_test = model_1(xyt_test).numpy()


test_MSE = np.mean((u_NN_test - np.reshape(u_test[:,None], (num_testing_samples,1)))**2)
print("Error on the test set basic sampling: %f" % test_MSE)


# Compute MSE on the test grid
u_NN_test = model_2(xyt_test).numpy()


test_MSE = np.mean((u_NN_test - np.reshape(u_test[:,None], (num_testing_samples,1)))**2)
print("Error on the test set sampling near collocation front: %f" % test_MSE)

# Plot at certain times
k_time = [10, 50, 100, 150]

u_exact = u_fdm[:, :, k_time]
u_1 = np.zeros((nx + 1, ny + 1, len(k_time)))
u_2 = np.zeros((nx + 1, ny + 1, len(k_time)))

for k in range(len(k_time)):
    t = np.full((ny + 1), k_time[k]*dt)

    for i in range(nx + 1):
        x = np.full((ny + 1),dx*i)
        y = np.arange(ny + 1)*dy
        u_1[i,:,k] = np.reshape(model_1(tf.stack((x,y,t), axis=1)).numpy(), ny + 1)
        u_2[i,:,k] = np.reshape(model_2(tf.stack((x,y,t), axis=1)).numpy(), ny + 1)

#Plot the comparison

fig = plt.figure(figsize=(12.0,12.0))
fignum = 1
for k in range(len(k_time)):
    ax = fig.add_subplot(len(k_time), 3, fignum)
    ax.imshow(u_exact[:,:,k].copy(), cmap=plt.get_cmap('hot'), vmin=Tcool - 0.5, vmax=Thot + 0.5)
    ax.set_axis_off()
    if k==0:
        ax.set_title('Exact')
    ax = fig.add_subplot(len(k_time), 3, fignum+1)
    ax.imshow(u_1[:,:,k].copy(), cmap=plt.get_cmap('hot'), vmin=Tcool - 0.5, vmax=Thot + 0.5)
    ax.set_axis_off()
    if k==0:
        ax.set_title('Normal')
    ax = fig.add_subplot(len(k_time), 3, fignum+2)
    im = ax.imshow(u_2[:,:,k].copy(), cmap=plt.get_cmap('hot'), vmin=Tcool - 0.5, vmax=Thot + 0.5)
    ax.set_axis_off()
    if k==0:
        ax.set_title('Special sampling')
    fignum += 3


fig.subplots_adjust(right=0.85)
cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.7])
#cbar_ax.set_xlabel('', labelpad=20)
plt.colorbar(im, cax=cbar_ax)

plt.suptitle('Collection points - rho=%f' % rho)
#plt.show()

plt.savefig("../../figure/old/compare_coll_points_sampling.png")
