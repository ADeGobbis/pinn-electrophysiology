#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

rho_values = [0, 0.001, 0.01, 0.1, 1]
p_values   = np.linspace(0.0, 1.0, num=5)

n = len(rho_values)
m = len(p_values)
perc_values = [str(int(100*xi))+'%' for xi in p_values]


df = pd.read_csv('results.csv', sep=',')

MSE = df['MSE']
loss_fit = df['Loss fit']
loss_PDE = df['Loss PDE']
MSE_mat = np.zeros((n,m))
fit_mat = np.zeros((n,m))
PDE_mat = np.zeros((n,m))

for i in range(n):
    for j in range(m):
        MSE_mat[i,j] = MSE[i*m + j]
        fit_mat[i,j] = loss_fit[i*m + j]
        PDE_mat[i,j] = loss_PDE[i*m + j]

# initialize lists to iterate
mat_list = [(MSE_mat, 'MSE'), (fit_mat,'loss_fit'), (PDE_mat, 'loss_PDE')]


# Now plot the heatmaps

for (mat, s) in mat_list:
    print(s + ' map:')
    print(mat)

    fig, ax = plt.subplots()
    #plt.set_cmap('hot_r')
    im = ax.imshow(mat)


    ax.set_xticks(np.arange(len(perc_values)))
    ax.set_yticks(np.arange(len(rho_values)))
    ax.set_yticklabels(rho_values)
    ax.set_xticklabels(perc_values)

    ax.set_xlabel('Fraction of training points')
    ax.set_ylabel('Weight PDE loss')

    ax.set_title('The ' + s + ' heatmap')

    plt.savefig('../figure/'+ s + '_map.png')
