#!/usr/bin/env python3

import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import numpy as np
import tensorflow as tf
import time
import matplotlib.pyplot as plt
from params_2d import *


if len(sys.argv) >= 2:
    rho = float(sys.argv[1])
else:
    rho = 1/K_exact**2


#%%%%%%%%%%%%%%%%%%%

# Samples

# Training points
x_train = tf.random.uniform(shape=[num_training_samples], minval=w[0], maxval=w[1]).numpy()
y_train = tf.random.uniform(shape=[num_training_samples], minval=h[0], maxval=h[1]).numpy()
t_train = tf.random.uniform(shape=[num_training_samples], minval=T[0], maxval=T[1]).numpy()

xyt_train = tf.stack((x_train, y_train, t_train), axis=1)
space_dim = xyt_train.shape[1] - 1

u_train = np.zeros((num_training_samples))

# Collocation points
x_grid = tf.random.uniform(shape=[num_collocation_points], minval=w[0], maxval=w[1]).numpy()
y_grid = tf.random.uniform(shape=[num_collocation_points], minval=h[0], maxval=h[1]).numpy()
t_grid = tf.random.uniform(shape=[num_collocation_points], minval=T[0], maxval=T[1]).numpy()


#%%%%%%%%%%%%%%%%%%%

# Initialization for the FDM

u_fdm   = np.full((nx + 1, ny + 1, nt + 1), Tcool)

#Initial condition
for i in range(nx + 1):
    for j in range(ny + 1):
        p2 = (i*dx-cx)**2 + (j*dy-cy)**2
        if p2 < r2:
            u_fdm[i,j,0] = Thot

def do_timestep(u0):
    # Propagate with forward-difference in time, central-difference in space
    u = np.full((nx + 1, ny + 1), Tcool)
    u[1:-1, 1:-1] = u0[1:-1, 1:-1] + D_exact * dt * (
          (u0[2:, 1:-1] - 2.0*u0[1:-1, 1:-1] + u0[:-2, 1:-1])/dx2
          + (u0[1:-1, 2:] - 2.0*u0[1:-1, 1:-1] + u0[1:-1, :-2])/dy2 ) + dt * K_exact * np.multiply(np.multiply(u0[1:-1, 1:-1],(u0[1:-1, 1:-1]-0.1)),(1.0 - u0[1:-1, 1:-1]))

    return u


def lin_interpolate(f, index, increment, point):
    #Linearly interpolate the 3D function goven the values in a grid around the point
    #print(f.shape)
    coord   = (point - np.multiply(index, increment))/increment
    i_coord = np.ones(3) - coord

    return f[1,1,1]*coord[0]*coord[1]*coord[2] \
        + f[0,1,1]*i_coord[0]*coord[1]*coord[2] \
        + f[1,0,1]*coord[0]*i_coord[1]*coord[2] \
        + f[1,1,0]*coord[0]*coord[1]*i_coord[2] \
        + f[0,0,1]*i_coord[0]*i_coord[1]*coord[2] \
        + f[0,1,0]*i_coord[0]*coord[1]*i_coord[2] \
        + f[1,0,0]*coord[0]*i_coord[1]*i_coord[2] \
        + f[0,0,0]*i_coord[0]*i_coord[1]*i_coord[2]


for k in range(nt):
    u_fdm[:,:,k+1] = do_timestep(u_fdm[:,:,k])


for m in range(num_training_samples):
    i, j, k = int(x_train[m]/dx), int(y_train[m]/dy), int(t_train[m]/dt)
    u_train[m] = lin_interpolate(u_fdm[i:(i+2), j:(j+2), k:(k+2)], (i,j,k), (dx,dy,dt), np.array([x_train[m], y_train[m], t_train[m]]))


# Model
model = tf.keras.Sequential([
    tf.keras.layers.Dense(10, input_shape=(3,), activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(10, activation=tf.nn.tanh),
    tf.keras.layers.Dense(1)
])

if train_params:
    D = tf.Variable(D_guess)
    K = tf.Variable(K_guess)
    trainable_variables = [model.variables, D, K]
else:
    D = D_exact
    K = K_exact
    trainable_variables = [model.variables]

# loss functions
@tf.function
def PDE(x,y,t):
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        tape.watch(y)
        tape.watch(t)
        with tf.GradientTape(persistent = True) as tape2:
            tape2.watch(x)
            tape2.watch(y)
            tape2.watch(t)
            u = model(tf.stack((x,y,t),axis=1))
        u_x = tape2.gradient(u, x)
        u_y = tape2.gradient(u, y)
        u_t = tape2.gradient(u, t)
    u_xx = tape.gradient(u_x, x)
    u_yy = tape.gradient(u_y, y)

    Iion = tf.reshape(u * (u - 1.0) * (u - alpha), shape = (num_collocation_points,))

    return u_t - D*(u_xx + u_yy) + K*Iion

last_loss_fit = tf.constant([0.0])
def loss_fit():
    u_NN  = model(xyt_train)
    global last_loss_fit
    last_loss_fit = tf.reduce_mean(tf.square(u_NN - u_train[:,None]))
    return last_loss_fit

last_loss_PDE = tf.constant([0.0])
def loss_PDE():
    global last_loss_PDE
    last_loss_PDE = tf.reduce_mean(tf.square(PDE(tf.constant(x_grid), tf.constant(y_grid), tf.constant(t_grid))))
    return last_loss_PDE

#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
opt = tf.keras.optimizers.Adam(learning_rate = learning_rate)

#print(model.variables)


# optimization loop
ts_ini = time.time()
for i in range(num_epochs):
    opt.minimize(lambda: loss_fit() + rho*loss_PDE(), trainable_variables)
    if i % 100 == 0:
        print('iter = %d, loss_fit = %f, loss_PDE = %f' %
            (i, last_loss_fit.numpy(), last_loss_PDE.numpy()))

print('elapsed time: %1.2f s' % (time.time() - ts_ini))

if train_params:
    print('Learned D: %f' % D.numpy())
    print('Relative error:  %f' % (abs(D.numpy() - D_exact)/D_exact))


#%%%%%%%%%%%%%%%%%% Post-processing

#Plot FDM solution vs learned solution


u_exact = u_fdm[:, :, k_time]
u_model = np.zeros((nx + 1, ny + 1, len(k_time)))


for k in range(len(k_time)):
    t = np.full((ny + 1), k_time[k]*dt)
    for i in range(nx + 1):
        x = np.full((ny + 1),dx*i)
        y = np.arange(ny + 1)*dy
        u_model[i,:, k] = np.reshape(model(tf.stack((x,y,t), axis=1)).numpy(), ny + 1)


fig = plt.figure()
fignum = 1
plt.title('Weight={:.4f}'.format((rho)))
plt.axis('off')
for k in range(len(k_time)):
    ax = fig.add_subplot(420 + fignum)
    im = ax.imshow(u_exact[:,:,k].copy(), cmap=plt.get_cmap('hot'), vmin=Tcool - 0.5,vmax=Thot + 0.5)
    ax.set_axis_off()
    fignum += 1
    ax = fig.add_subplot(420 + fignum)
    im = ax.imshow(u_model[:,:,k].copy(), cmap=plt.get_cmap('hot'), vmin=Tcool - 0.5,vmax=Thot + 0.5)
    ax.set_axis_off()
    fignum += 1

fig.subplots_adjust(right=0.85)
cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.7])
cbar_ax.set_xlabel('V', labelpad=20)
fig.colorbar(im, cax=cbar_ax)
#plt.show()

plt.savefig("../../figure/old/normal_2D.png")

# Measure of the MSE

print('Computing MSE on %d points' % num_testing_samples)

xi = np.random.choice(nx + 1, size=num_testing_samples)
yi = np.random.choice(ny + 1, size=num_testing_samples)
ti = np.random.choice(nt + 1, size=num_testing_samples)

u_test = u_fdm[xi,yi,ti]
xyt_test = tf.stack((xi*dx, yi*dy, ti*dt), axis=1)

u_NN_test = model(xyt_test).numpy()

MSE = tf.sqrt(tf.reduce_mean(tf.square(u_test[:,None] - u_NN_test)))
print("MSE error on the test set: %f" % MSE.numpy())
