#!/usr/bin/env python3

import numpy as np
import tensorflow as tf


# Physical parameters

T = [0.0, 1.0]
w = [0.0, 100.0]
h = [0.0, 100.0]
w2 = (w[1] - w[0])**2
h2 = (h[1] - h[0])**2

D_exact = 12.9*2.0
K_exact = 12.9*8.0

alpha = 0.1

# Model parameters
D_guess = 5.0
K_guess = 100.0

num_training_samples = 200
num_testing_samples  = 10000
num_collocation_points = 10000


num_epochs = 7000
learning_rate = 10e-3

train_params = True

# Discretization parameters

dx = 0.5
dy = 0.5
dt = 0.001

dx2, dy2 = dx*dx, dy*dy
nx, ny, nt = int((w[1]-w[0])/dx), int((h[1]-h[0])/dy), int((T[1]-T[0])/dt)


# Solution parameters

Thot = 1.0
Tcool = 0.0

r, cx, cy = 25, 50, 50
r2 = r**2

# Plot parameters

t_time = [0.0, 0.1, 0.5, 0.75]
k_time = [int((T[1]-T[0])/dt*x) for x in t_time]
