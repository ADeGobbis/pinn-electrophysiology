import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import nisaba as ns
import numpy as np

from params import *


# Reads (or creates) the samples points in tensor form

# Collocation points
x_list = []
for i in range(space_dim):
    x_grid = scale(tf.random.uniform(shape=[num_collocation_points], minval = w[i][0], maxval = w[i][1], dtype=dtype), w[i])
    x_list.append(x_grid)
t_grid = tf.random.uniform(shape=[num_collocation_points], minval = T[0], maxval = T[1], dtype=dtype)

# Initial points
from training_1d import u_init, xt_init


# Testing samples

from training_1d import u_train, xt_train


# Get it from another module as a tensor

# Boundary points

# Activation points

from activation_1d import xt_down, xt_up

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# Model

model = tf.keras.Sequential([
 tf.keras.layers.Dense(10, input_shape=(space_dim + 1,), activation=tf.nn.tanh, dtype=dtype),
 tf.keras.layers.Dense(20, activation=tf.nn.tanh, dtype=dtype),
 tf.keras.layers.Dense(20, activation=tf.nn.tanh, dtype=dtype),
 tf.keras.layers.Dense(10, activation=tf.nn.tanh, dtype=dtype),
 tf.keras.layers.Dense(1, dtype=dtype)
])


trainable_variables = model.variables


from Aliev_Panfilov import *

if train_params:
    trainable_variables += [sigma]

#print(trainable_variables)

PDE = handler()

loss_unused = [ns.LossMeanSquares('fit', lambda: model(xt_train) - u_train[:,None])]

losses = [ns.LossMeanSquares('PDE', lambda: PDE(x_list, t_grid, model), weight=rho), \
          ns.LossMeanSquares('init', lambda: model(xt_init) - u_init[:,None]), \
          ns.LossMeanSquares('diff_down', lambda: tf.math.maximum(0.0, model(xt_down) - eps)), \
          ns.LossMeanSquares('diff_up', lambda: tf.math.minimum(0.0, model(xt_up) - 1.0 + eps))]


#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
pb = ns.OptimizationProblem(trainable_variables, losses, frequency_print=100)

#ns.minimize(pb, 'scipy', 'L-BFGS-B', num_epochs=num_epochs)
ns.minimize(pb, learner_backend, learner, num_epochs=num_epochs)

if train_params:
    print('Learned D: %f' % sigma.numpy())
    print('Relative error:  %f' % ((sigma.numpy() - sigma_exact[0])/sigma_exact[0]))
    #np.save('D_normal.npy', D_values)

#%%%%%%%%%%%%%%%%%% Post-processing

# Plot the real solution vs learned solution
import matplotlib.pyplot as plt
from training_1d import tf_sol
t_test = [0.0, 0.5, 1.0, 1.5]
fig = plt.figure()
fignum = 1
xx = np.linspace(w[0][0], w[0][1], num=5000, dtype=np.float32)
for i in range(len(t_test)):
     test_points = tf.stack((xx, np.full((5000), t_test[i])), axis = 1)
     test_points_mod = tf.stack((scale(xx, w[0]), np.full((5000), scale(t_test[i], T))), axis = 1)
     ax = fig.add_subplot(220 + fignum)
     ax.plot(xx,  model(test_points_mod).numpy(), 'r-')
     ax.plot(xx, tf_sol(test_points[:,0], test_points[:,1]), 'k-')
     ax.set_title('{:.1f} ms'.format(t_test[i]*1000))
     ax.set_ylim([-0.1, 1.1])
     fignum = fignum + 1
#plt.show()
plt.savefig("plot-1D.png")


# Measure the MSE

print('Computing MSE on %d points' % num_test_points)
from training_1d import u_test, xt_test
u_NN = model(xt_test)

MSE = tf.sqrt(tf.reduce_mean(tf.square(u_test[:,None] - u_NN)))

print('MSE error on test set: ', MSE.numpy())
