This directory contains a first version of the project, not class based.

`Aliev_Panfilov.py` and `main_no_lapl.py` are an incomplete implementation from before `nisaba.experimental.physics` was available, so they do not vectorial gradient or divergence.

`main.py` and `main_scaled.py` employ laplacian function from Nisaba.

The rest of the files are modules that later became the `generate_data` functors.
