import tensorflow as tf
from params import num_activation_points, T, w, K_exact, sigma_exact, alpha, scale

delta = 0.1


c = tf.sqrt(2*sigma_exact[0]) * tf.sqrt(K_exact) * (1 - 2*alpha) / 2

unif = tf.random.uniform(shape=[num_activation_points], minval = T[0], maxval = T[1]).numpy()
x_act  = scale(c*unif, w[0])
t_act  = scale(unif, T)

delta = 2*delta/(T[1]-T[0])
t_act_up = t_act + delta
t_act_down = t_act - delta

xt_up = tf.stack((x_act, t_act_up), axis=1)
xt_down = tf.stack((x_act, t_act_down), axis=1)
