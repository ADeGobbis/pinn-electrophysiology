import tensorflow as tf
import nisaba as ns
from params import T, w2, alpha, sigma_guess, space_dim, K_exact

sigma = tf.Variable(sigma_guess[:space_dim], dtype='float64')
K = K_exact

# The hadler for the space dimension
def handler():
    if space_dim == 1:
        return _1d
    elif space_dim == 2:
        return _2d
    else:
        return _3d

@tf.function
def _1d(li, t, u_model):
    x = li[0]
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        tape.watch(t)
        u = u_model(tf.stack((x,t),axis=1))

        u_t = tape.gradient(u, t)
        u_x = tape.gradient(u, x)
        u_xx = tape.gradient(u_x, x)

        Iion = tf.squeeze(u * (u - 1.0) * (u - alpha))

    return u_t/(T[1] - T[0]) - 2*sigma[0]*u_xx/w2[0][0] + K*Iion/2
