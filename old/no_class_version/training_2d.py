import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import numpy as np
import tensorflow as tf
from params import w, T, space_dim, num_training_samples, D_exact, K_exact, center, radius, dtype

w = np.array(w)

assert space_dim == 2 , 'The training module is for 2D problems only'
assert w.shape[1] > 1 , 'The limit matrix is too small'

# intervals in x-, y- directions, mm
dx = dy = 0.5
dt = 0.001 # 0.1 ms real_t = 12.9 * t
nx, ny, nt = int((w[0,1] - w[0,0])/dx), int((w[1,1] - w[1,0])/dy), int((T[1]-T[0])/dt)

dx2, dy2 = dx*dx, dy*dy
D = D_exact
K = K_exact

u = np.zeros((nx + 1, ny + 1, nt + 1))
# Initial conditions - ring of inner radius r, width dr centred at (cx,cy) (mm)
# Actually a circle centered there not a ring
r2 = radius**2
for i in range(nx + 1):
    for j in range(ny + 1):
        p2 = (i*dx-center[0])**2 + (j*dy-center[1])**2
        if p2 < r2:
            u[i,j,0] = 1.0

def do_timestep(u0):
    # Propagate with forward-difference in time, central-difference in space
    u = u0[1:-1, 1:-1] + D * dt * (
          (u0[2:, 1:-1] - 2.0*u0[1:-1, 1:-1] + u0[:-2, 1:-1])/dx2
          + (u0[1:-1, 2:] - 2.0*u0[1:-1, 1:-1] + u0[1:-1, :-2])/dy2 ) + dt * K * np.multiply( np.multiply(u0[1:-1, 1:-1],(u0[1:-1, 1:-1]-0.1)),(1.0 - u0[1:-1, 1:-1]) )
    return u

for k in range(nt):
    u[1:-1, 1:-1, k+1] = do_timestep(u[:,:,k])

u_train = np.zeros((num_training_samples))

x_train = tf.random.uniform(shape=[num_training_samples, space_dim], minval = w[:,0], maxval = w[:,1], dtype=dtype)
t_train = tf.random.uniform(shape=[num_training_samples, 1], minval = T[0], maxval = T[1], dtype=dtype)

def lin_interpolate(f, index, increment, point):
    #Linearly interpolate the 3D function goven the values in a grid around the point
    #print(f.shape)
    coord   = (point - np.multiply(index, increment))/increment
    i_coord = np.ones(3) - coord

    return f[1,1,1]*coord[0]*coord[1]*coord[2] \
        + f[0,1,1]*i_coord[0]*coord[1]*coord[2] \
        + f[1,0,1]*coord[0]*i_coord[1]*coord[2] \
        + f[1,1,0]*coord[0]*coord[1]*i_coord[2] \
        + f[0,0,1]*i_coord[0]*i_coord[1]*coord[2] \
        + f[0,1,0]*i_coord[0]*coord[1]*i_coord[2] \
        + f[1,0,0]*coord[0]*i_coord[1]*i_coord[2] \
        + f[0,0,0]*i_coord[0]*i_coord[1]*i_coord[2]


for m in range(0, num_training_samples):
    i, j, k = int(x_train[m,0]/dx), int(x_train[m,1]/dy), int(t_train[m]/dt)
    u_train[m] = lin_interpolate(u[i:(i+2), j:(j+2), k:(k+2)], (i,j,k), (dx,dy,dt), np.array([x_train[m,0], x_train[m,1], t_train[m]]))

u_train = tf.constant(u_train)

# Plot test

# Output 4 figures at these timesteps
""" import matplotlib.pyplot as plt

mfig = [0, 500, 750, 1000]
fignum = 0
fig = plt.figure()
for m in mfig:
    fignum += 1
    print(m, fignum)
    ax = fig.add_subplot(220 + fignum)
    im = ax.imshow(u[:,:,m].copy(), cmap=plt.get_cmap('hot'), vmin=0.0,vmax=1.0)
    ax.set_axis_off()
    ax.set_title('{:.1f} ms'.format(m*dt*12.9))
fig.subplots_adjust(right=0.85)
cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.7])
cbar_ax.set_xlabel('V', labelpad=20)
fig.colorbar(im, cax=cbar_ax)
#plt.show()

plt.savefig('test_sol.png') """
