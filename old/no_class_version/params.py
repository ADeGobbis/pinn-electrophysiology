import tensorflow as tf

# The limiting values of the imputs, this way we can scale it for the NN

w = [[0, 100.0], [0, 100]]
w2 = (w[0][0] - w[0][1])**2
T = [0, 1.0]

space_dim = 2

# Problem parameters (this part will be implied in the class later, in a way to be compatible with the pyfex binding)
# Maybe we can read these off a .prm  file to keep it the same with lifeX

alpha = 0.1
K_exact = 12.9*8.0

# The intial datum is a function that acts on a tensor of dimension space_dim
center, radius = tf.constant([50.0, 50.0], dtype='float64'), 20.0

u_0 = lambda x: tf.cast(tf.reduce_sum(tf.square(x - center), axis=1) < radius**2, dtype = dtype)

# sigma = [sigmal, sigmat, sigman]

sigma = [2.0e-4, 0.5e-4, 0.25e-4]
sigma_guess = [1.0e-4, 1.0e-4, 1.0e-4]

D_exact = 12.9*1.0
D_guess = 6.0

# Input parameters

num_training_samples = 5000
num_collocation_points = 5000
num_initial_points = 1000
num_boundary_points = 1000
num_activation_points = 3000
num_test_points = 10000

train_params = True

# Model parameters

num_epochs = 100

learning_rate = 1e-3
#learner_cat = 'scipy'
#learner = 'BFGS'
learner_cat = 'keras'
learner = tf.keras.optimizers.Adam(learning_rate=learning_rate)

dtype = 'float64'

rho = 0.01
