import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import nisaba as ns
import nisaba.experimental as nse
import utils as u

from params import *


# Reads (or creates) the samples points in tensor form
min_w = [w[d][0] for d in range(space_dim)]
max_w = [w[d][1] for d in range(space_dim)]
# Collocation points
x_grid = tf.random.uniform(shape=[num_collocation_points, space_dim], minval = min_w, maxval = max_w, dtype=dtype)
t_grid = tf.random.uniform(shape=[num_collocation_points, 1], minval = T[0], maxval = T[1], dtype=dtype)

# Initial points
x_init = tf.random.uniform(shape=[num_initial_points, space_dim], minval = min_w, maxval = max_w, dtype=dtype)
t_init = tf.fill([num_initial_points, 1], tf.cast(T[0], dtype=dtype))

u_init = u_0(x_init)
xt_init = tf.concat((x_init, t_init), axis=1)
#print(u.numpy())

# Training samples
print('Importing training data')
from training_2d import u_train, x_train, t_train

xt_train = tf.concat((x_train, t_train), axis=1)
#print(u_train)

# Testing points
print('Importing test data')
from testing_2d import u_test, x_test, t_test

xt_test = tf.concat((x_test, t_test), axis=1)


# Get it from another module as a tensor

# Boundary points

# Activation points

print(40*'=')
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# Model

model = tf.keras.Sequential([
 tf.keras.layers.Dense(10, input_shape=(space_dim + 1,), activation=tf.nn.tanh),
 tf.keras.layers.Dense(20, activation=tf.nn.tanh),
 tf.keras.layers.Dense(20, activation=tf.nn.tanh),
 tf.keras.layers.Dense(10, activation=tf.nn.tanh),
 tf.keras.layers.Dense(1, dtype=dtype)
])

K = K_exact
trainable_variables = model.variables
if train_params:
    D = ns.Variable(D_guess)
    trainable_variables.append(D)
else:
    D = D_exact

# loss functions
#@tf.function
def PDE(x, t, u_model):
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        tape.watch(t)
        u = u_model(tf.concat((x,t),axis=1))
        lapl = nse.physics.tens_style.laplacian_scalar(tape, u, x, space_dim)
        u_t = tf.squeeze(tape.gradient(u, t))

        #print('u_t: ', u_t.shape)
        #print('Lapl: ', lapl.shape)

        Iion = tf.squeeze(u * (u - 1.0) * (u - alpha))

    return u_t - D*lapl + K*Iion


losses = [ns.LossMeanSquares('fit', lambda: model(xt_train) - u_train[:,None]), \
          ns.LossMeanSquares('PDE', lambda: PDE(x_grid, t_grid, model), weight=rho), \
          ns.LossMeanSquares('init', lambda: model(xt_init) - u_init[:,None])]

losses_fit = [ns.LossMeanSquares('MSE', lambda: model(xt_test) - u_test[:,None])]

if train_params:
    losses_fit.append(ns.LossMeanSquares('D', lambda: (D - D_exact)/D_exact))

#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
pb = ns.OptimizationProblem(trainable_variables, losses, losses_fit, frequency_print=100)
pb.compile()

#ns.minimize(pb, 'scipy', 'L-BFGS-B', num_epochs=num_epochs)
ns.minimize(pb, learner_cat, learner, num_epochs=num_epochs)
#ns.minimize(pb, u.opt.alias['Adam'][0], u.opt.alias['Adam'][1](learning_rate=learning_rate), num_epochs=num_epochs)
pb.save_history('main.json')

if train_params:
    print('Learned D: %f' % D.numpy())
    print('Relative error:  %f' % ((D.numpy() - D_exact)/D_exact))
    #np.save('D_normal.npy', D_values)
