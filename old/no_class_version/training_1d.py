import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import numpy as np
import tensorflow as tf
from params import w, T, space_dim, num_training_samples, sigma_exact, K_exact, alpha, dtype, num_initial_points, scale, num_test_points

w = np.array(w)
D_exact = sigma_exact[0]

assert space_dim == 1 , 'The training module is for 1D problems only'

c = np.sqrt(2*D_exact) * np.sqrt(K_exact) * (1 - 2*alpha) / 2
beta = np.sqrt(K_exact)/(2 * np.sqrt(2*D_exact))

tf_sol    = lambda x, t: 1/2 + tf.tanh(-beta*(x - c*t))/2

x_train = tf.random.uniform(shape=[num_training_samples], minval=w[0][0], maxval=w[0][1], dtype=dtype)
t_train = tf.random.uniform(shape=[num_training_samples], minval=T[0], maxval=T[1], dtype=dtype)
xt_train = tf.stack((scale(x_train, w[0]), scale(t_train, T)), axis=1)

u_train = tf_sol(x_train, t_train)

x_init = tf.random.uniform(shape=[num_initial_points], minval = w[0][0], maxval = w[0][1], dtype=dtype)
t_init = tf.fill([num_initial_points], tf.cast(T[0], dtype=dtype))
xt_init = tf.stack((scale(x_init,w[0]), scale(t_init,T)), axis=1)

u_init = tf_sol(x_init, t_init)

x_test = tf.random.uniform(shape=[num_test_points], minval = w[0][0], maxval = w[0][1], dtype=dtype)
t_test = tf.random.uniform(shape=[num_test_points], minval=T[0], maxval=T[1], dtype=dtype)
xt_test = tf.stack((scale(x_test,w[0]), scale(t_test,T)), axis=1)

u_test = tf_sol(x_test, t_test)
