import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import sys,os
# !!! Changed the directory
sys.path.append("{0}{1}".format( os.path.dirname(os.path.realpath(__file__)) , "/.."))


import tensorflow as tf
import nisaba as ns
import nisaba.experimental as nse
import utils as u

from params import *


# Reads (or creates) the samples points in tensor form

# Collocation points
x_grid = u.tens_style.scale_space(tf.random.uniform(shape=[num_collocation_points, space_dim], minval = [w[0][0], w[1][0]], maxval = [w[0][1], w[1][1]], dtype=dtype), w, space_dim)
t_grid = u.tens_style.scale(tf.random.uniform(shape=[num_collocation_points, 1], minval = T[0], maxval = T[1], dtype=dtype), T)

# Initial points
x_init = tf.random.uniform(shape=[num_initial_points, space_dim], minval = w[0], maxval = w[1], dtype=dtype)
t_init = tf.fill([num_initial_points, 1], tf.cast(T[0], dtype=dtype))

u_init = u_0(x_init)
xt_init = tf.concat((u.tens_style.scale_space(x_init, w, space_dim), u.tens_style.scale(t_init, T)), axis=1)
#print(u.numpy())

# Training samples
print('Importing training data')
from training_2d import u_train, x_train, t_train

xt_train = tf.concat((u.tens_style.scale_space(x_train,w,space_dim), u.tens_style.scale(t_train,T)), axis=1)
#print(u_train)

# Testing points
print('Importing test data')
from testing_2d import u_test, x_test, t_test

xt_test = tf.concat((u.tens_style.scale_space(x_test,w,space_dim), u.tens_style.scale(t_test,T)), axis=1)


# Get it from another module as a tensor

# Boundary points

# Activation points

# Samples

print(40*'=')
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# Model

model = tf.keras.Sequential([
 tf.keras.layers.Dense(10, input_shape=(space_dim + 1,), activation=tf.nn.tanh, dtype=dtype),
 tf.keras.layers.Dense(20, activation=tf.nn.tanh, dtype=dtype),
 #tf.keras.layers.Dense(20, activation=tf.nn.tanh, dtype=dtype),
 tf.keras.layers.Dense(10, activation=tf.nn.tanh, dtype=dtype),
 tf.keras.layers.Dense(1, dtype=dtype)
])

K = K_exact
trainable_variables = model.variables
if train_params:
    D = ns.Variable(D_guess)
    trainable_variables.append(D)
else:
    D = D_exact

# loss functions
#@tf.function
def PDE(x, t, u_model):
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        tape.watch(t)
        u = u_model(tf.concat((x,t),axis=1))
        lapl = nse.physics.tens_style.laplacian_scalar(tape, u, x, space_dim)
        u_t = tf.squeeze(tape.gradient(u, t))

        #print('u_t: ', u_t.shape)
        #print('Lapl: ', lapl.shape)

        Iion = tf.squeeze(u * (u - 1.0) * (u - alpha))

    return u_t/(T[1]-T[0]) - 2*D*lapl/w2 + K*Iion/2

def PDE2(x, y, t, u_model):
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        tape.watch(y)
        tape.watch(t)
        u = u_model(tf.stack((x,y,t),axis=1))
        u_t = tf.squeeze(tape.gradient(u, t))
        u_x = tf.squeeze(tape.gradient(u, x))
        u_y = tf.squeeze(tape.gradient(u, y))

        u_xx = tf.squeeze(tape.gradient(u_x, x))
        u_yy = tf.squeeze(tape.gradient(u_y, y))
        Iion = tf.squeeze(u * (u - 1.0) * (u - alpha))

        return u_t/(T[1]-T[0]) - 2*D*(u_xx + u_yy)/w2 + K*Iion/2


losses = [ns.LossMeanSquares('fit', lambda: model(xt_train) - u_train[:,None]),  \
          ns.LossMeanSquares('PDE', lambda: PDE(x_grid, t_grid, model), weight=4*rho), \
          #ns.LossMeanSquares('PDE_no_lapl', lambda: PDE2(x_grid[:,0], x_grid[:,1], t_grid[:,0], model), weight=4*rho), \
          ns.LossMeanSquares('init', lambda: model(xt_init) - u_init[:,None])]

losses_fit = [ns.LossMeanSquares('MSE', lambda: model(xt_test) - u_test[:,None])]

if train_params:
    losses_fit.append(ns.LossMeanSquares('D', lambda: (D - D_exact)/D_exact))

print('K: ', K_exact)
print('w2: ', w2)
print('alpha: ', alpha)
print('rho: ', rho)

#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
pb = ns.OptimizationProblem(trainable_variables, losses, losses_fit, frequency_print=100)
pb.compile()

#ns.minimize(pb, 'scipy', 'L-BFGS-B', num_epochs=num_epochs)
ns.minimize(pb, learner_cat, learner, num_epochs=num_epochs)
pb.save_history('main.json')

if train_params:
    print('Learned D: %f' % D.numpy())
    print('Relative error:  %f' % ((D.numpy() - D_exact)/D_exact))
    #np.save('D_normal.npy', D_values)
