#############################################################################
#
# Interpolates solution with PINN and compare to pure fitting (works with MPI)
#
#############################################################################



#import sys,os
#sys.path.append("{0}{1}".format( os.path.dirname(os.path.realpath(__file__)) , "/../.."))

# Number of dimensions
space_dim = 3


assert space_dim in [1,2], "Number of dimensions unacceptable"

from pinn_electrophysiology.builders import *

# Import the correct data generation method
if space_dim == 2:
    from generate_data._2d_fdm import create_data
else:
    from generate_data._1d_fdm import create_data


import pinn_electrophysiology.utils as u
import matplotlib.pyplot as plt
import tensorflow as tf
import nisaba as ns
import time
import numpy as np
import json
from mpi4py import MPI

# Training epochs
n_epochs = 100
n_samples = 3

# Flag for sampling training points
uniform_training = True


num_training_points = 1000
num_initial_points = 1000
num_collocation_points = 10000
num_test_points = 10000
num_boundary_points = 1000

file_path = '../pinn_electrophysiology/params.json'
input_dir = '../data/input/'

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()


file = json.load(open(file_path))
w = file[str(space_dim)+'D']['w']
T = file[str(space_dim)+'D']['T']

if rank == 0:
    # Generate data
    data = create_data()
    data(file_path, input_dir, num_initial_points, num_collocation_points, num_training_points, num_test_points, num_boundary_points, 0, uniform_training)

comm.Barrier()

# Initialized as numpy arrays
MSE_1, MSE_2 = np.zeros(1), np.zeros(1)
t_fit, t_class = np.zeros(1), np.zeros(1)
MSE_avg = np.empty(1)
t_avg = np.zeros(1)


for i in range(rank, n_samples, size):
    # With only fitting

    mod_fit = tf.keras.Sequential([
        tf.keras.layers.Dense(10, input_shape=(space_dim + 1,), activation=tf.nn.tanh),
        tf.keras.layers.Dense(20, activation=tf.nn.tanh),
        tf.keras.layers.Dense(20, activation=tf.nn.tanh),
        tf.keras.layers.Dense(10, activation=tf.nn.tanh),
        tf.keras.layers.Dense(1)
    ])

    array = np.load(input_dir+'train.npy')

    xt_train_fit = u.array_style.scale_space(array[:,:space_dim+1], w + [T], space_dim+1)
    u_train_fit = array[:,-1]

    array = np.load(input_dir+'MSE.npy')

    xt_test_fit = u.array_style.scale_space(array[:,:space_dim+1], w + [T], space_dim+1)
    u_test_fit = array[:,-1]

    del array

    # Create Nisaba problem

    losses = [ns.LossMeanSquares('fit', lambda: mod_fit(xt_train_fit) - u_train_fit[:,None])]
    pb = ns.OptimizationProblem(mod_fit.variables, losses, frequency_print=1000, verbosity=0)
    pb.compile()

    t = time.time()
    ns.minimize(pb, 'scipy', 'BFGS', num_epochs = n_epochs)
    t_fit += time.time() - t

    MSE_1 += tf.reduce_mean(tf.square(mod_fit(xt_test_fit) - u_test_fit[:,None])).numpy()

    # Use class

    obj = generate_AP(space_dim, file_path, 'Interpolation', boundary = 'Neumann')
    obj.init_pb(1000, compile=True, verbosity=0)

    t = time.time()
    obj.minimize('BFGS', n_epochs)

    t_class += time.time() - t

    MSE_2 += obj.get_loss_test('MSE_fit')

# Plot one solution
if rank == 0:
    obj.test_plot([0.0, 0.1, 0.2, 0.5], '../figure/interpolation/inter_'+str(space_dim)+'D.png', plane_axis = 1, plane = 0.0005)

# Post processing --------------------------------------------------------------------

comm.Barrier()
# Reduce average MSE and time per epochs and prints them to output in rank 0


comm.Reduce([MSE_1, MPI.FLOAT], MSE_avg, op=MPI.SUM, root=0)
comm.Reduce([t_fit, MPI.FLOAT], t_avg, op=MPI.SUM, root=0)

#print(t_avg)

if rank == 0:
    print(20*'=')
    print('\nOnly fitting:')
    print('MSE: ', MSE_avg[0] / n_samples)
    print('Time per epoch: ', t_avg[0] / (n_epochs*n_samples))

comm.Reduce([MSE_2, MPI.FLOAT], MSE_avg, op=MPI.SUM, root=0)
comm.Reduce([t_class, MPI.FLOAT], t_avg, op=MPI.SUM, root=0)

if rank == 0:
    print('\nWith residual:')
    print('MSE: ', MSE_2[0] / n_samples)
    print('Time per epoch: ', t_avg[0] / (n_epochs*n_samples))
