This directory contains examples of the old implementation and the scripts used for the preliminary results in Section 4 of the report.

*supplied*: initial scripts made available to me.

*nonlinear_PINN*: forward problem in one spatial dimension (Section 4.1)

*alternate_loss_pde*: sampling near activation front, penalized loss and residual loss components (Section 4.2)

*weight_PDE*: compares different weights for the loss_PDE generating the boxplots seen in the report (Section 4.3)

*scaled*: different transformations on the inputs, the residual can be weighted (Section 4.4)

*no_class_version*: a first versione of the project in without classes

*MPI_interpolate.py*: simple example of parallelization with `mpi4py` applied to `example/interpolate.py`. Can be run with the following command where NUM_PROCS is the number of desired processes

```bash
mpiexec -n NUM-PROCS python MPI_interpolate.py
```
> **Warning**: if not run with NUM_PROCS distinc cores (for example with --use-hwthread-cpus) the resulting time per epoch might not reflect the real value.


All plots are saved in `figure/old/` directory.
