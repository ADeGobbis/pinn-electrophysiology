All plots and other figures are saved here as specified in the scripts.

*old*: plots from scripts in `old/`

*interpolation*: plots from `example/interpolate.py`

*inverse*: plots from `example/inverse_problems.py`
