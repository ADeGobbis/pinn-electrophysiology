import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import json

d = 2

num_train = [5000, 10000, 15000]

file_path = '../../pinn_electrophysiology/params.json'

sigma_exact = json.load(open(file_path))[str(d)+'D']['sigma_exact']
sigma_learned = np.load('sigma_'+str(d)+'.npy')



fig = plt.figure(figsize=(8.0, d*4.8))

for i in range(d):
    ax = fig.add_subplot(d, 1, i+1)
    ax.boxplot(sigma_learned[:,:,i].T)
    ax.set_xticklabels(num_train)
    ax.axhline(sigma_exact[i], color='k', linestyle='--')
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.set_xlabel('Number of training points')
    ax.set_ylabel('sigma_'+str(i+1))

plt.suptitle('Sigma learned in '+str(d)+'D')
plt.savefig('sigma_boxplots_'+str(d)+'D.png')
