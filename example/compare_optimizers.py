#######################################################################
#
# Compares optimizers, in 1D
#
#######################################################################

import sys,os
sys.path.append("{0}{1}".format( os.path.dirname(os.path.realpath(__file__)) , "/.."))

from  pinn_electrophysiology.builders import *
import matplotlib.pyplot as plt
from pinn_electrophysiology.utils import opt
from generate_data._1d_analytical import create_data
from operator import add
import numpy as np
import time


# Name of the learners to be tested
learners = ['Nelder-Mead','Adam', 'NAdam', 'BFGS', 'Newton-CG', 'L-BFGS-B']

# Number of times the MSE is computed
n_samples = 5

# Learning epochs
n_epochs = 100
input_dir = '../data/input/'
pb_type = 'Interpolation'
bdry = None

prm_file = '../pinn_electrophysiology/params.json'

MSE = np.zeros(len(learners))
times = np.zeros(len(learners))
MSE_logs = []


# Checks if the optimizers are implemented
for key in learners:
    if key not in opt.alias.keys():
        raise Exception('Learner tag not recognized')

data = create_data()
data(prm_file, input_dir, 500, 4000, 500, 5000, 0, 0)
#TODO: get the time by epoch
for k in range(len(learners)):
    for j in range(n_samples):
        obj = generate_AP(1, prm_file, pb_type, bdry)
        obj.init_pb(compile=True)
        t = time.time()
        obj.minimize(learners[k], n_epochs)
        times[k] += time.time() - t
        MSE[k] += obj.get_loss_test('MSE_fit')
        if j == 0:
            MSE_logs.append(obj.get_log_loss_test('MSE_fit'))
        else:
            MSE_logs[k] = list(map(add, obj.get_log_loss_test('MSE_fit'), MSE_logs[k]))

        del obj

    MSE_logs[k] = [x/n_samples for x in MSE_logs[k]]

MSE = MSE/n_samples
times = times/(n_samples*n_epochs)

#print(len(MSE_logs))
#print(type(MSE_logs[0]))
#print(len(MSE_logs[0]))


# Post processing ------------------------------------------------------------------------

print(40*'=')
print('\nMSE:')

for k in range(len(learners)):
    print(learners[k] + ':' + str(MSE[k]))

 # Plot MSE hostory and time per epoch barplot
fig = plt.figure(figsize=(15, 9.5))
ax = fig.add_subplot(121)
for k in range(len(MSE_logs)):
    ax.set_yscale('log')
    ax.plot(10*np.arange(len(MSE_logs[k]))*times[k], MSE_logs[k])
ax.set_ylabel('MSE on test set')
ax.set_xlabel('time [s]')
ax.set_xlim([-0.1, n_epochs*np.amax(times) + 1.0])
ax = fig.add_subplot(122)
pos = 0
for t in times:
    ax.bar(pos, t)
    pos += 1
ax.legend(learners, loc = 'lower left')
ax.set_xticks([])
ax.set_ylabel('time per epoch [s]')

plt.tight_layout(w_pad=1.6)
plt.savefig('../figure/compare_optimizers.png')
