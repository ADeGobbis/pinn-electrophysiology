#######################################################################
#
# Estimates conductivities
#
#######################################################################


#import sys,os
#sys.path.append("{0}{1}".format( os.path.dirname(os.path.realpath(__file__)) , "/../.."))

# Number of dimensions
space_dim = 1
#param_file = '../../lifex_electrophysiology_slab.prm'


assert space_dim in [1,2,3], "Number of dimensions unacceptable"

from pinn_electrophysiology.builders import *

# Import the correct data generation method
if space_dim == 3:
    from generate_data._3d_pyfex import create_data, TIMESOLVER, NONLINEARSOLVER
elif space_dim == 2:
    from generate_data._2d_fdm import create_data
else:
    from generate_data._1d_analytical import create_data


import matplotlib.pyplot as plt
import numpy as np
import json

n_epochs = 100
n_samples = 2

# Number of trainig points
num_train = [5000, 10000, 15000, 17500]

# Other problem parameters
num_initial_points = 1000
num_collocation_points = 500
num_test_points = 5000
num_boundary_points = 1000

# Params file and input/output directories
file_path = '../pinn_electrophysiology/params.json'
input_dir = '../data/input/'
output_dir = '../data/output/inverse/'

sigma_learned = np.zeros((len(num_train), n_samples, space_dim))

# Initialize data generation functor
data = create_data()

for j in range(len(num_train)):
    # Generate dataset
    data(file_path, input_dir, num_initial_points, num_collocation_points, num_train[j], num_test_points, num_boundary_points, 1)
    for i in range(n_samples):

        obj = generate_AP(space_dim, file_path, 'Inverse standard', boundary = 'Neumann')
        obj.init_pb(200, compile=True)
        obj.minimize('BFGS', n_epochs)
        #obj.save_history('inverse_'+str(d)+'D')
        if i == 0:
            obj.save_history(output_dir+'inverse_history_'+str(space_dim)+'D_'+str(num_train[j]))

        sigma_learned[j,i,:] = obj.get_sigma()


# Get real value of sigma from file
sigma_exact = json.load(open(file_path))[str(space_dim)+'D']['sigma_exact']
np.save(output_dir+'sigma_'+str(space_dim), sigma_learned)

# Takes mean values of conductivities learned
sigma_mean = np.mean(sigma_learned, axis=1)


# Post processing -------------------------------------------------------------------------

print('\nSigma exact:')
print(sigma_exact)
print('\nSigma learned:')

for i in range(len(num_train)):
    print('Training points {} => '.format((num_train[i])), sigma_mean[i,:])


# Plot the boxplot of estimated parameters

fig = plt.figure(figsize=(6.4, space_dim*4.8))
for i in range(space_dim):
    ax = fig.add_subplot(space_dim, 1, i+1)
    ax.boxplot(sigma_learned[:,:,i].T)
    ax.set_xticklabels(num_train)
    ax.axhline(sigma_exact[i], color='k', linestyle='--')
    ax.set_xlabel('Number of training points')
    ax.set_ylabel('sigma_'+str(i+1))

plt.savefig('../figure/inverse/sigma_boxplots_'+str(space_dim)+'D.png')
