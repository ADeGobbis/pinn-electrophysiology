This directory contains all scripts to run the tests from Section 6 of the report.

*compare_optimizers.py*: compare performaneces of different learners. Prints the average MSE by optimizer and generates a plot in `figure/compare_optimizers.png` (Section 6.1)

*interpolate.py*: compares the performances of the PINN in interpolating compared to a neural network with a simple fitting loss. Prints MSE and tipe per epoch, and saves a plot of the PINN solution in `figure/interpolation/`. Spatial dimension can be modified, the flag `uniform_training` is used to pick the type of sampling for values of the real solution. (Section 6.2)

*inverse_problems.py*: estimates conductivities (sigma) for different numbers of training points. Prints the learned parameters and saves boxplots to `figure/inverse/`. Spatial dimension and problem parameters can be modified. (Section 6.3)

*activation_losses_1D.py*: compares performances of activation times losses defined in Section 2.2 applied to the inverse problem. Prints estimated conductivity and saves a plot of parameters history and boxplots in `figure/compare_activation_losses_*`. This script is only compatible with spatial dimension 1, but the other parameters can be modified. (Section 6.4)

The two `plot_aid` scripts are used in case the final plots of all other scripts need to be changed quickly and use the arrays contained in `data/output/`

All examples must be executed from this directory with the command:
```bash
python 'script_name'
```
simulation can take a long time depending on the example so it is advised to run them with a low number of learning epochs and/or samples.
