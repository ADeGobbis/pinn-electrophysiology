import numpy as np
import json


dims = [1, 2, 3]

file_num = json.load(open('num_points.json'))
num_training_points = file_num['num_training_points']
num_init_points = file_num['num_init_points']
num_collocation_points = file_num['num_collocation_points']

file = json.load(open('1D/time_prm.json'))



for i in dims:

    w = np.array(file[str(i)+'D']['w'])
    T = file[str(i)+'D']['T']


    # Training

    x = np.random.uniform(size=(num_training_points,i), low=w[:,0], high=w[:,1])
    t = np.random.uniform(size=(num_training_points,1), low=T[0], high=T[1])
    u = np.ones((num_training_points,1))

    np.save(str(i)+'D/fit', np.concatenate((x,t,u), axis=1))

    x = np.random.uniform(size=(num_init_points,i), low=w[:,0], high=w[:,1])
    t = np.full((num_init_points,1), T[0])
    u = np.zeros((num_init_points,1))

    np.save(str(i)+'D/init', np.concatenate((x,t,u), axis=1))

    x = np.random.uniform(size=(num_collocation_points,i), low=w[:,0], high=w[:,1])
    t = np.random.uniform(size=(num_collocation_points,1), low=T[0], high=T[1])

    np.save(str(i)+'D/PDE', np.concatenate((x,t,u), axis=1))
