import sys,os
#sys.path.append("{0}{1}".format( os.path.dirname(os.path.realpath(__file__)) , "/../../.."))

from pinn_electrophysiology.aliev_panfilov import AP
import json
import time
import numpy as np

file_num = json.load(open('../num_points.json'))
num_training_points = file_num['num_training_points']
num_init_points = file_num['num_init_points']
num_collocation_points = file_num['num_collocation_points']
num_epochs = file_num['num_epochs']

print('Class based 3D\n')
a_1 = AP(3, 'time_prm.json', True, True, False, False)
a_1.init_pb(num_epochs, compile=False)

ts_ini = time.time()
a_1.minimize('Adam', num_epochs, learning_rate=1e-3)
t_not_compiled = time.time()-ts_ini
print('Eager')
print('elapsed time: %1.4f s' % (t_not_compiled))
print('time by epoch: %1.4f s' % (t_not_compiled/num_epochs))


a_2 = AP(3, 'time_prm.json', True, True, False, False)
a_2.init_pb(num_epochs, compile=True)
ts_ini = time.time()
a_2.minimize('Adam', num_epochs, learning_rate=1e-3)
t_compiled = time.time()-ts_ini
print('Autograph')
print('elapsed time: %1.4f s' % (t_compiled))
print('time by epoch: %1.4f s' % (t_compiled/num_epochs))

# Save results in the numpy file

if os.path.isfile('times.npy'):
    t_array = np.load('times.npy')
else:
    t_array = np.zeros((2, 3))

t_array[0,2] = t_not_compiled/num_epochs
t_array[1,2] = t_compiled/num_epochs

np.save('times', t_array)
