This directory contains the scripts to generate the plot in Section 4.5. Since it is a mix of old and new I included `bash` scripts that call all necessary Python programs. The syntax to get the barplots from this directory is:

```bash
./run_all.sh 1 2 3
```
with the arguments referring to the space dimensions, by omitting any one of them the relative section is not run.

Since only training time is of interest input data is generated at random by `create_dummy_data.py`.
General parameters can be edited in the `num_points.json`.
