#!/usr/bin/env python3

import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import numpy as np
import tensorflow as tf
import time
#import matplotlib.pyplot as plt
import json

file_num = json.load(open('../num_points.json'))
num_training_points = file_num['num_training_points']
num_init_points = file_num['num_init_points']
num_collocation_points = file_num['num_collocation_points']
num_epochs = file_num['num_epochs']

file = json.load(open('time_prm.json'))

K_exact = file['Physics']['Aliev-Panfilov']['K_exact']
alpha = file['Physics']['Aliev-Panfilov']['alpha']
D_exact = file['2D']['sigma_exact']
w = file['2D']['w']
T = file['2D']['T']

w2 = (w[0][1]-w[0][0])**2
h2 = (w[1][1]-w[1][0])**2

rho = file['Optimization']['weight_PDE']

h = w[1]
w = w[0]


print('Standard 2D\n')

#tf.random.set_seed(240)
#np.random.seed(1)

# Scaling function (the input is between -1 and 1)

def scale(x, limits):
    return (2*x - limits[1] - limits[0])/(limits[1] - limits[0])

#%%%%%%%%%%%%%%%%%%%

# Samples

# Training points
x_train = tf.random.uniform(shape=[num_training_points], minval=w[0], maxval=w[1]).numpy()
y_train = tf.random.uniform(shape=[num_training_points], minval=h[0], maxval=h[1]).numpy()
t_train = tf.random.uniform(shape=[num_training_points], minval=T[0], maxval=T[1]).numpy()

xyt_train = tf.stack((scale(x_train,w), scale(y_train,h), scale(t_train,T)), axis=1)
space_dim = xyt_train.shape[1] - 1

u_train = np.ones((num_training_points))

# Collocation points
x_grid = scale(tf.random.uniform(shape=[num_collocation_points], minval=w[0], maxval=w[1]).numpy(), w)
y_grid = scale(tf.random.uniform(shape=[num_collocation_points], minval=h[0], maxval=h[1]).numpy(), h)
t_grid = scale(tf.random.uniform(shape=[num_collocation_points], minval=T[0], maxval=T[1]).numpy(), T)

# Initial points
x_init = tf.random.uniform(shape=[num_init_points], minval=w[0], maxval=w[1]).numpy()
y_init = tf.random.uniform(shape=[num_init_points], minval=h[0], maxval=h[1]).numpy()
t_init = np.zeros((num_init_points))

xyt_init = tf.stack((scale(x_init,w), scale(y_init,h), scale(t_init,T)), axis=1)

u_init = np.full((num_init_points), 0.5)

"""
#initialize
for m in range(num_initial_samples):
    p2 = (x_init[m]-cx)**2 + (y_init[m]-cy)**2
    if p2 < r2:
        u_init[m] = Thot


# Tensors for diffusivity
f0tensorf0 = [[f0[0, :] * f0[0, :], f0[0, :] * f0[1, :]], \
              [f0[1, :] * f0[0, :], f0[1, :] * f0[1, :]]]
f0tensorf0 = tf.convert_to_tensor(f0tensorf0, dtype=tf.float32)

I = tf.eye(space_dim)
I = tf.reshape(I, [space_dim, space_dim, 1])
I = tf.repeat(I, num_collocation_points, axis=2)

Dm_exact = l_2_exact * I + (l_1_exact - l_2_exact)*f0tensorf0
Dm_exact = Dm_exact[:,:,0]


#%%%%%%%%%%%%%%%%%%%

# Initialization for the FDM

u_fdm   = np.full((nx + 1, ny + 1, nt + 1), Tcool)

#Initial condition
for i in range(nx + 1):
    for j in range(ny + 1):
        p2 = (i*dx-cx)**2 + (j*dy-cy)**2
        if p2 < r2:
            u_fdm[i,j,0] = Thot

def do_timestep(u0):
    # Propagate with forward-difference in time, central-difference in space
    u = np.full((nx + 1, ny + 1), Tcool)
    u[1:-1, 1:-1] = u0[1:-1, 1:-1] + dt * ( \
          Dm_exact[0,0]*(u0[2:, 1:-1] - 2.0*u0[1:-1, 1:-1] + u0[:-2, 1:-1])/dx2   \
          + Dm_exact[1,1]*(u0[1:-1, 2:] - 2.0*u0[1:-1, 1:-1] + u0[1:-1, :-2])/dy2  \
          + (Dm_exact[1,0] + Dm_exact[0,1])*(u0[2:,2:] + u0[:-2,:-2] - u0[2:,:-2] - u0[:-2,2:])/(4*dxdy)) \
          + dt * K_exact * np.multiply(np.multiply(u0[1:-1, 1:-1],(u0[1:-1, 1:-1]-0.1)),(1.0 - u0[1:-1, 1:-1]))

    return u


def lin_interpolate(f, index, increment, point):
    #Linearly interpolate the 3D function goven the values in a grid around the point
    #print(f.shape)
    coord   = (point - np.multiply(index, increment))/increment
    i_coord = np.ones(3) - coord

    return f[1,1,1]*coord[0]*coord[1]*coord[2] \
        + f[0,1,1]*i_coord[0]*coord[1]*coord[2] \
        + f[1,0,1]*coord[0]*i_coord[1]*coord[2] \
        + f[1,1,0]*coord[0]*coord[1]*i_coord[2] \
        + f[0,0,1]*i_coord[0]*i_coord[1]*coord[2] \
        + f[0,1,0]*i_coord[0]*coord[1]*i_coord[2] \
        + f[1,0,0]*coord[0]*i_coord[1]*i_coord[2] \
        + f[0,0,0]*i_coord[0]*i_coord[1]*i_coord[2]


for k in range(nt):
    u_fdm[:,:,k+1] = do_timestep(u_fdm[:,:,k])


for m in range(num_training_samples):
    i, j, k = int(x_train[m]/dx), int(y_train[m]/dy), int(t_train[m]/dt)
    u_train[m] = lin_interpolate(u_fdm[i:(i+2), j:(j+2), k:(k+2)], (i,j,k), (dx,dy,dt), np.array([x_train[m], y_train[m], t_train[m]]))

"""
# Model
model = tf.keras.Sequential([
    tf.keras.layers.Dense(10, input_shape=(3,), activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(20, activation=tf.nn.tanh),
    tf.keras.layers.Dense(10, activation=tf.nn.tanh),
    tf.keras.layers.Dense(1)
])

K = K_exact
#if train_params:
#    l_1 = tf.Variable(l_1_guess)
#    l_2 = tf.Variable(l_2_guess)
#    trainable_variables = [model.variables, l_1, l_2]
#else:
l_1 = D_exact[0]
l_2 = D_exact[1]
trainable_variables = [model.variables]

# loss functions
"""
@tf.function
def PDE(x,y,t):
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        tape.watch(y)
        tape.watch(t)
        u = model(tf.stack((x,y,t),axis=1))
        u_x = tape.gradient(u, x)
        u_y = tape.gradient(u, y)
        u_t = tape.gradient(u, t)
        Dm = l_2 * I + (l_1 - l_2) * f0tensorf0
        Dmgradu = [[Dm[0, 0, :] * u_x/w2 + Dm[0, 1, :] * u_y/wh ], \
                   [Dm[1, 0, :] * u_x/wh + Dm[1, 1, :] * u_y/h2 ]]
    #print(Dmgradu.shape)
        Dmgradu_x = tape.gradient(Dmgradu, x)
        Dmgradu_y = tape.gradient(Dmgradu, y)
    #print(Dmgradu_x.shape)
    #print(Dmgradu_y.shape)

        Iion = tf.squeeze(u * (u - 1.0) * (u - alpha))

    return u_t/(T[1] - T[0]) - 2*Dmgradu_x - 2*Dmgradu_y + K*Iion/2
"""

#@tf.function
def PDE(x,y,t):
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        tape.watch(y)
        tape.watch(t)

        u = model(tf.stack((x,y,t),axis=1))
        u_x = tape.gradient(u, x)
        u_y = tape.gradient(u, y)
        u_t = tape.gradient(u, t)
        u_xx = tape.gradient(u_x, x)
        u_yy = tape.gradient(u_y, y)
        #print(Dmgradu_x.shape)
        #print(Dmgradu_y.shape)

        Iion = tf.squeeze(u * (u - 1.0) * (u - alpha))

    return u_t/(T[1] - T[0]) - 2*l_1*u_xx/w2 - 2*l_2*u_yy/h2 + K*Iion/2



last_loss_fit = tf.constant([0.0])
def loss_fit():
    u_NN  = model(xyt_train)
    global last_loss_fit
    last_loss_fit = tf.reduce_mean(tf.square(u_NN - u_train[:,None]))
    return last_loss_fit

last_loss_PDE = tf.constant([0.0])
def loss_PDE():
    global last_loss_PDE
    last_loss_PDE = tf.reduce_mean(tf.square(PDE(tf.constant(x_grid), tf.constant(y_grid), tf.constant(t_grid))))
    return last_loss_PDE

last_loss_init = tf.constant([0.0])
def loss_init():
    u_NN  = model(xyt_init)
    global last_loss_init
    last_loss_init = tf.reduce_mean(tf.square(u_NN - u_init[:,None]))
    return last_loss_init


#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
opt = tf.keras.optimizers.Adam(learning_rate = 1e-3)

#print('Learning rate = %f' % (learning_rate))
ts_ini = time.time()
for i in range(num_epochs):
    opt.minimize(lambda: loss_fit() + rho*loss_PDE() + loss_init(), trainable_variables)
    #if i % 100 == 0:
    #    print('iter = %d, loss_fit = %f, loss_PDE = %f, loss_init = %f' %
    #        (i, last_loss_fit.numpy(), last_loss_PDE.numpy(), last_loss_init.numpy()))


t_not_compiled = time.time()-ts_ini
print('Eager')
print('elapsed time: %1.4f s' % (t_not_compiled))
print('time by epoch: %1.4f s' % (t_not_compiled/num_epochs))


PDE = tf.function(PDE)
ts_ini = time.time()
# optimization loop
for i in range(num_epochs):
    opt.minimize(lambda: loss_fit() + rho*loss_PDE() + loss_init(), trainable_variables)

t_compiled = time.time()-ts_ini
print('Autograph')
print('elapsed time: %1.4f s' % (t_compiled))
print('time by epoch: %1.4f s' % (t_compiled/num_epochs))


# Save results in the numpy file

if os.path.isfile('times.npy'):
    t_array = np.load('times.npy')
else:
    t_array = np.zeros((2, 3))

t_array[0,0] = t_not_compiled/num_epochs
t_array[1,0] = t_compiled/num_epochs

np.save('times', t_array)

"""
#%%%%%%%%%%%%%%%%%% Post-processing

#Plot FDM solution vs learned solution


u_exact = u_fdm[:, :, k_time]
u_model = np.zeros((nx + 1, ny + 1, len(k_time)))


for k in range(len(k_time)):
    t = np.full((ny + 1), scale(k_time[k]*dt, T))
    for i in range(nx + 1):
        x = np.full((ny + 1),scale(dx*i,w))
        y = scale(np.arange(ny + 1)*dy, h)
        u_model[i,:, k] = np.reshape(model(tf.stack((x,y,t), axis=1)).numpy(), ny + 1)


fig = plt.figure()
fignum = 1
plt.title('Scaled inputs')
plt.axis('off')
for k in range(len(k_time)):
    ax = fig.add_subplot(420 + fignum)
    im = ax.imshow(u_exact[:,:,k].copy(), cmap=plt.get_cmap('hot'), vmin=Tcool - 0.5,vmax=Thot + 0.5)
    ax.set_axis_off()
    fignum += 1
    ax = fig.add_subplot(420 + fignum)
    im = ax.imshow(u_model[:,:,k].copy(), cmap=plt.get_cmap('hot'), vmin=Tcool - 0.5,vmax=Thot + 0.5)
    ax.set_axis_off()
    fignum += 1

fig.subplots_adjust(right=0.85)
cbar_ax = fig.add_axes([0.9, 0.15, 0.03, 0.7])
cbar_ax.set_xlabel('V', labelpad=20)
fig.colorbar(im, cax=cbar_ax)
#plt.show()

plt.savefig("test-18-2D.png")

# Measure of the MSE

print('Computing MSE on %d points' % num_testing_samples)

xi = np.random.choice(nx + 1, size=num_testing_samples)
yi = np.random.choice(ny + 1, size=num_testing_samples)
ti = np.random.choice(nt + 1, size=num_testing_samples)

u_test = u_fdm[xi,yi,ti]
xyt_test = tf.stack((scale(xi*dx, w), scale(yi*dy,h), scale(ti*dt,T)), axis=1)

u_NN_test = model(xyt_test).numpy()

MSE = tf.sqrt(tf.reduce_mean(tf.square(u_test[:,None] - u_NN_test)))
print("MSE error on the test set: %f" % MSE.numpy())
"""
