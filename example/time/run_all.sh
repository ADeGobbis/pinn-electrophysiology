#! /bin/bash

for dim in "$@"
do
  cd "$dim"D
  ./get_all.sh
  cd ..
done

python barplot.py
