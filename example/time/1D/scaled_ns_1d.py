#!/usr/bin/env python3

import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import numpy as np
import nisaba as ns
#import matplotlib.pyplot as plt
import json
import time

file_num = json.load(open('../num_points.json'))
num_training_points = file_num['num_training_points']
num_init_points = file_num['num_init_points']
num_collocation_points = file_num['num_collocation_points']
num_epochs = file_num['num_epochs']

file = json.load(open('time_prm.json'))

K_exact = file['Physics']['Aliev-Panfilov']['K_exact']
alpha = file['Physics']['Aliev-Panfilov']['alpha']
D_exact = file['1D']['sigma_exact'][0]
w = file['1D']['w'][0]
T = file['1D']['T']

w2 = (w[1]-w[0])**2

rho = file['Optimization']['weight_PDE']

print('Nisaba 1D\n')





# Scaling function (scales the input between 0 and 1)

def scale(x, limits):
    return (2*x - limits[1] - limits[0])/(limits[1] - limits[0])

dtype = 'float64'
#%%%%%%%%%%%%%%

# Samples

# Training points
x_train  = tf.random.uniform(shape=[num_training_points], minval = w[0], maxval = w[1], dtype=dtype).numpy()
t_train  = tf.random.uniform(shape=[num_training_points], minval = T[0], maxval = T[1], dtype=dtype).numpy()

xt_train = tf.stack((scale(x_train, w), scale(t_train, T)), axis=1)
#u_train = tf.constant(tf_sol(x_train, t_train))
u_train = np.ones((num_training_points))




# Collocation points
x_grid  = scale(tf.random.uniform(shape=[num_collocation_points], minval = w[0], maxval = w[1], dtype=dtype).numpy(), w)
t_grid  = scale(tf.random.uniform(shape=[num_collocation_points], minval = T[0], maxval = T[1], dtype=dtype).numpy(), T)

# Initial points
x_init = tf.random.uniform(shape=[num_init_points], minval = w[0], maxval = w[1]).numpy()
t_init = np.full([num_init_points], T[0])

xt_init = tf.stack((scale(x_init, w), scale(t_init, T)), axis=1)
#u_init = tf_sol(x_init, t_init)
u_init = np.zeros((num_init_points))
"""
# Testing points
x_test  = tf.random.uniform(shape=[num_testing_samples], minval = w[0], maxval = w[1]).numpy()
t_test  = tf.random.uniform(shape=[num_testing_samples], minval = T[0], maxval = T[1]).numpy()

xt_test = tf.stack((scale(x_test, w), scale(t_test, T)), axis=1)
u_test = tf_sol(x_test, t_test).numpy()
"""

# Model
model = tf.keras.Sequential([
 tf.keras.layers.Dense(10, input_shape=(2,), activation=tf.nn.tanh),
 tf.keras.layers.Dense(20, activation=tf.nn.tanh),
 tf.keras.layers.Dense(20, activation=tf.nn.tanh),
 tf.keras.layers.Dense(10, activation=tf.nn.tanh),
 tf.keras.layers.Dense(1, dtype=dtype)
])

K = K_exact
trainable_variables = model.variables
#if train_params:
#    D = ns.Variable(D_guess)
#    trainable_variables.append(D)
#else:
D = D_exact

# loss functions
#@tf.function
def PDE(x, t, u_model):
    with tf.GradientTape(persistent = True) as tape:
        tape.watch(x)
        tape.watch(t)
        u = u_model(tf.stack((x,t),axis=1))
        #print('u:', u.shape)

        u_x = tape.gradient(u, x)
        u_t = tape.gradient(u, t)
        u_xx = tape.gradient(u_x, x)

        Iion = tf.squeeze(u * (u - 1.0) * (u - alpha))

    return u_t/(T[1] - T[0]) - 2*D*u_xx/w2 + K*Iion/2

losses = [ns.LossMeanSquares('fit', lambda: model(xt_train) - u_train[:,None]), \
          ns.LossMeanSquares('PDE', lambda: PDE(tf.constant(x_grid), tf.constant(t_grid), model), weight=rho), \
          ns.LossMeanSquares('init', lambda: model(xt_init) - u_init[:,None])]
#%%%%%%%%%%%%%%%%%% Training
# initialize optimizer
pb = ns.OptimizationProblem(trainable_variables, losses, frequency_print=1000)

ts_ini = time.time()

#ns.minimize(pb, 'scipy', 'BFGS', num_epochs=num_epochs)
ns.minimize(pb, 'keras', tf.keras.optimizers.Adam(learning_rate=1e-3), num_epochs=num_epochs)


t_not_compiled = time.time()-ts_ini
print('Eager')
print('elapsed time: %1.4f s' % (t_not_compiled))
print('time by epoch: %1.4f s' % (t_not_compiled/num_epochs))

pb.compile()

ts_ini = time.time()

#ns.minimize(pb, 'scipy', 'BFGS', num_epochs=num_epochs)
ns.minimize(pb, 'keras', tf.keras.optimizers.Adam(learning_rate=1e-3), num_epochs=num_epochs)
t_compiled = time.time()-ts_ini
print('Autograph')
print('elapsed time: %1.4f s' % (t_compiled))
print('time by epoch: %1.4f s' % (t_compiled/num_epochs))


# Save results in the numpy file

if os.path.isfile('times.npy'):
    t_array = np.load('times.npy')
else:
    t_array = np.zeros((2, 3))

t_array[0,1] = t_not_compiled/num_epochs
t_array[1,1] = t_compiled/num_epochs

np.save('times', t_array)


#if train_params:
#    print('Learned D: %f' % D.numpy())
#    print('Relative error:  %f' % ((D.numpy() - D_exact)/D_exact))
    #np.save('D_normal.npy', D_values)
"""
#%%%%%%%%%%%%%%%%%% Post-processing

# Plot the real solution vs learned solution
t_test = [0.0, 0.5, 1.0, 1.5]
fig = plt.figure()
fignum = 1
xx = np.linspace(w[0], w[1], num=5000, dtype=np.float32)
for i in range(len(t_test)):
     test_points = tf.stack((xx, np.full((5000), t_test[i])), axis = 1)
     test_points_mod = tf.stack((scale(xx, w), np.full((5000), scale(t_test[i], T))), axis = 1)
     ax = fig.add_subplot(220 + fignum)
     ax.plot(xx,  model(test_points_mod).numpy(), 'r-')
     ax.plot(xx, tf_sol(test_points[:,0], test_points[:,1]), 'k-')
     ax.set_title('{:.1f} ms'.format(t_test[i]*1000))
     ax.set_ylim([-0.1, 1.1])
     fignum = fignum + 1
#plt.show()
plt.savefig("test-11-1D.png")


# Measure the MSE

print('Computing MSE on %d points' % num_testing_samples)
u_NN = model(xt_test)

MSE = tf.sqrt(tf.reduce_mean(tf.square(u_test[:,None] - u_NN)))

print('MSE error on test set: ', MSE.numpy())
"""
