import numpy as np
import matplotlib.pyplot as plt

dims = [1,2,3]
fig = plt.figure(figsize=(10, 15))

for i in dims:

    times = np.load('{}D/times.npy'.format(i))
    #print(times.shape)

    X = np.arange(3)
    ax = fig.add_subplot(len(dims),1, i)
    ax.bar(X + 0.0, times[0,:], color='r', width=0.2)
    ax.bar(X + 0.2, times[1,:], color='c', width=0.2)
    ax.legend(['Eager', 'AutoGraph'])
    ax.set_ylabel('time per epoch [s]')
    ax.set_ylim([0.0, 0.075])
    ax.set_xticks([0.1, 1.1, 2.1])
    ax.set_xticklabels(['Base', 'Nisaba', 'Class'])
    ax.set_title('{} spatial dimensions'.format(i))

plt.savefig('time_barplot.png')
