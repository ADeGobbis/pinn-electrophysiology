import numpy as np
import matplotlib.pyplot as plt
import json

space_dim = 1

prm_file = '../../pinn_electrophysiology/params.json'
act_type = ['Standard', 'Differences', 'Fitting', 'Derivatives']
sigma_exact = np.array(json.load(open(prm_file))['1D']['sigma_exact'])

sigma_logs = np.load('sigma_logs_1d.npy')[:,:,:,:]
sigma = np.load('sigma_1d.npy')[:,:,:]
sigma_mean = np.mean(sigma, axis=1)
sigma_logs = np.mean(sigma_logs, axis=1)
print(sigma_logs.shape)

sigma_rel = np.zeros(sigma_logs.shape)


for d in range(space_dim):
#    sigma_rel[:,:,d] = np.array(map(lambda x: get_rel(x,sigma_exact[d]), sigma_logs[:,:,d]))
    sigma_rel[:,:,d] = ((sigma_logs[:,:,d] - sigma_exact[d])/sigma_exact[d])**2

print(sigma_rel.shape)

# Plot the results
fig = plt.figure(figsize=(13.0,4.8))
fignum = 1
for d in range(space_dim):
    ax = fig.add_subplot(space_dim,2,fignum)
    ax.plot(10*np.arange(sigma_logs.shape[1]),np.squeeze(sigma_logs[:,:,d]).T)
    ax.axhline(sigma_exact, color='k', linestyle='--')
    ax.set_xlabel('Training epochs')
    if d == 0:
        ax.set_title('Sigmas')
    ax = fig.add_subplot(space_dim,2,fignum+1)
    ax.set_yscale('log')
    ax.plot(10*np.arange(sigma_logs.shape[1]),np.squeeze(sigma_rel[:,:,d]).T)
    ax.axhline(color='k', linestyle='--')
    ax.set_xlabel('Training epochs')
    if d == 0:
        ax.set_title('Squared relative error')

    fignum += 1

plt.legend(act_type)

#plt.show()
plt.savefig('../../figure/compare_activation_losses.png')


# Boxplots for the sigma
fig = plt.figure()
fignum = 1
for d in range(space_dim):
    ax = fig.add_subplot(space_dim,1,fignum)
    ax.boxplot(sigma[:,:,d].T)
    ax.axhline(sigma_exact, color='k', linestyle='--')
    ax.set_xticklabels(act_type)
    fignum += 1

plt.savefig('../../figure/compare_activation_losses_boxplot.png')


print(40*'=')
print('\nSigma:')
print('Sigma exact:' + str(sigma_exact))

for k in range(len(act_type)):
    print(act_type[k] + ':' + str(sigma_mean[k,:]))

print('\nRelative error:')

for k in range(len(act_type)):
    print(act_type[k] + ':' + str(np.sqrt(sigma_rel[k,-1,:])))
