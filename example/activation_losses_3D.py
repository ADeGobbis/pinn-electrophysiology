#######################################################################
#
# Compares activation times losses in 1D
#
#######################################################################


#import sys,os
#sys.path.append("{0}{1}".format( os.path.dirname(os.path.realpath(__file__)) , "/../.."))

from pinn_electrophysiology.builders import *
from generate_data._3d_pyfex import create_data
import json
import matplotlib.pyplot as plt
import numpy as np


# Number of times the MSE is computed
n_samples = 1

# Number of data points and trainig epochs
n_epochs = 3000
num_activation_points = 5000

# Boundary conditions
bdry = None

prm_file = '../data/act3D_prm.json'
input_dir = '../data/input/'
output_dir = '../data/output/'

pb_type = 'Inverse activation'

act_type = ['Standard', 'Differences', 'Fitting', 'Derivatives']

# Reads sigma exact from file
space_dim = 3
sigma_exact = np.array(json.load(open(prm_file))[str(space_dim)+'D']['sigma_exact'])
sigma = np.zeros((len(act_type), n_samples, space_dim))
sigma_logs = np.zeros((len(act_type), n_samples, n_epochs // 10 + 1, space_dim))

# Create dataset
data = create_data()
data(prm_file, input_dir, 2000, 25000, 5000, 5000, 0, num_activation_points)

# Loops over all losses n_samples times
for k in range(len(act_type)):
    if act_type[k] == 'Standard':
        name = 'Inverse standard'
    else:
        name = pb_type + '-' + act_type[k]

    for j in range(n_samples):
        obj = generate_AP(space_dim, prm_file, name, bdry)
        obj.init_pb(compile=True)
        obj.minimize('BFGS', n_epochs)
        sigma[k,j,:] += obj.get_sigma()
        for d in range(space_dim):
            a = obj.get_log_loss_test('sigma_'+str(d+1))

            # Pads vector in case of early stopping
            if len(a) == n_epochs // 10 +1:
                sigma_logs[k,j,:,d] = a
            else:
                sigma_logs[k,j,:,d] = np.concatenate((a, np.full((n_epochs // 10 + 1 - len(a)), a[-1])))

        del obj


# Saves results
np.save(output_dir+'sigma_logs_act', sigma_logs)
np.save(output_dir+'sigma_act', sigma)

# Take the means
sigma_mean = np.mean(sigma, axis=1)
sigma_logs = np.mean(sigma_logs, axis=1)

# Relative error
sigma_rel = np.zeros(sigma_logs.shape)

for d in range(space_dim):
#    sigma_rel[:,:,d] = np.array(map(lambda x: get_rel(x,sigma_exact[d]), sigma_logs[:,:,d]))
    sigma_rel[:,:,d] = ((sigma_logs[:,:,d] - sigma_exact[d])/sigma_exact[d])**2


# Post processing ------------------------------------------------------------------------------------

print(40*'=')
print('\nSigma:')
print('Sigma exact:' + str(sigma_exact))

for k in range(len(act_type)):
    print(act_type[k] + ':' + str(sigma_mean[k,:]))


# Plot loss history
fig = plt.figure(figsize=(13.0,4.8*space_dim))
fignum = 1
for d in range(space_dim):
    ax = fig.add_subplot(space_dim,2,fignum)
    ax.plot(10*np.arange(sigma_logs.shape[1]),sigma_logs[:,:,d].T)
    ax.axhline(sigma_exact[d], color='k', linestyle='--')
    ax.set_xlabel('Training epochs')
    if d == 0:
        ax.set_title('Sigmas')
    ax = fig.add_subplot(space_dim,2,fignum+1)
    ax.plot(10*np.arange(sigma_logs.shape[1]),np.square(sigma_rel[:,:,d].T))
    ax.axhline(color='k', linestyle='--')
    ax.set_xlabel('Training epochs')
    ax.set_ylabel(['l','t','n'][d])
    if d == 0:
        ax.set_title('Squared relative error')

    fignum += 2

plt.legend(act_type)

#plt.show()
plt.savefig('../figure/activation/compare_activation_losses_3d.png')


# Boxplots for the sigma
fig = plt.figure()
fignum = 1
for d in range(space_dim):
    ax = fig.add_subplot(space_dim,1,fignum)
    ax.boxplot(sigma[:,:,d].T)
    ax.axhline(sigma_exact[d], color='k', linestyle='--')
    ax.set_xticklabels(act_type)
    fignum += 1

plt.savefig('../figure/activation/compare_activation_losses_boxplot_3d.png')
