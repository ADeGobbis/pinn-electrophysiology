Here are saved inputs and numerical outputs of all other scripts (usually as .npy flies)

*input*: the default directory where scripts read the input datasets.

*output*: contains `.json` files with Optimization Problem's loss histories and `.npy` files with lerned parameters and MSE values.
