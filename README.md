Step 0 - Download *mk-modules* for lifeX
-------------------------------------------------------
> **Note**: in case you have a previous version of *mk-modules* installed, delete it with `sudo rm -rf /u/`.

1. Download the *mk-modules* archive from [this link](https://github.com/elauksap/mk/releases/download/v2020.1/mk-2020.1-lifex.tar.gz).
2. Open a terminal in the folder containing the archive mk-2020.1-lifex.tar.gz just downloaded and type the following command:
```bash
sudo tar xvzf mk-2020.1-lifex.tar.gz -C /
```
3. Add the following lines to your `~/.bashrc` file:
```bash
# mk-modules.
export mkPrefix=/u/sw
source ${mkPrefix}/etc/profile
module load gcc-glibc/9 dealii vtk
export PYTHONPATH=

# workaround for libutils.so
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/user/.local/lib/python3.8/site-packages
```
4. Restart the shell.


Step 1 - Download repository with submodules
----------------------------------------------------
Download respository recursively with
```bash
git clone --recursive git@gitlab.com:ADeGobbis/pinn-electrophysiology.git
```


Step 2 - Python requirements
----------------------------------------------------

Make sure that your Python version  `>=3.8` with the command:

`python --version`

if not update to Python 3.8 by:

```bash
sudo apt update -y
sudo apt install python3.8
```

Step 3 - Install *Nisaba*
----------------------------------------------------

1. Install the Python libraries required with `pip install -r requirements.txt`

2. Add the following line to your `~/.bashrc` file and restart the shell:

```bash
export PYTHONPATH= ${PYTHONPATH}:/path/to/project:/path/to/project/nisaba
```


Step 4 - Compile *pyfeX* using *mk-modules*
---------------------------------------------------

Run the following commands from the project directory.

1. `cd pyfex && mkdir build && cd build && cmake ../lifex/.`
2. `make -j<N>` where N is the desired number of processes, this step could take a few minutes
3. `cd ..`
4. `LIFEX_DIR=$(pwd)/build pip3 install --user --verbose .`
5. `cd ..`


> **Note**: Windows is not directly compatible with `lifex` and needs a VM to run the project
