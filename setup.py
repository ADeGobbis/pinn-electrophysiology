from setuptools import find_packages, setup

setup(
    name='pinn_electrophysiology',
    version='0.1',
    author='Andrea De Gobbis',
    author_email='andrea.degobbis@polimi.it',
    description='A PINN project with application to cardiac electrophysiology',
    long_description=''
)
