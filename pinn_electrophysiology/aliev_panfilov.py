#############################################################################
#
# Derived class for monodomain equation with Aliev - Panfilov ionic component
#
#############################################################################


#import sys
#import os, logging
#logging.disable(logging.WARNING)
#os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


from pinn_electrophysiology.core import Core
import pinn_electrophysiology.utils as u
import nisaba as ns
import nisaba.experimental as nse
import numpy as np
import tensorflow as tf


"""
class AP_iso(Core):

    def __init__(self, path_to_prm_file, test, scaled = False, inverse = False):
        super().__init__(path_to_prm_file, test, scaled)

        if self.is_scaled:
            self.w2 = (self.params['w'][0][1] - self.params['w'][0][0])**2

        self.load_PDE(inverse)



    # The arguments must be tensor
    def residual(self, x, t, D):
        with tf.GradientTape(persistent = True) as tape:
            tape.watch(x)
            tape.watch(t)
            u = self.NN(tf.concat((x,t),axis=1))
            lapl = nse.physics.tens_style.laplacian_scalar(tape, u, x, self.params['space_dim'])
            u_t = tf.squeeze(tape.gradient(u, t))

            #print('u_t: ', u_t.shape)
            #print('Lapl: ', lapl.shape)

            Iion = tf.squeeze(u * (u - 1.0) * (u - self.params['alpha']))

        return u_t - D*lapl + self.params['K_exact']*Iion


    def residual_scaled(self, x, t, D):
        with tf.GradientTape(persistent = True) as tape:
            tape.watch(x)
            tape.watch(t)
            u = self.NN(tf.concat((x,t),axis=1))
            lapl = nse.physics.tens_style.laplacian_scalar(tape, u, x, self.params['space_dim'])
            u_t = tf.squeeze(tape.gradient(u, t))

            #print('u_t: ', u_t.shape)
            #print('Lapl: ', lapl.shape)

            Iion = tf.squeeze(u * (u - 1.0) * (u - self.params['alpha']))

        return u_t/(self.params['T'][1]-self.params['T'][0]) - 2*D*lapl/self.w2 + self.params['K_exact']*Iion/2
"""
# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


# This is the class version with anisotropic diffusion

class AP(Core):


    def __init__(self, space_dim, path_to_prm_file,  scaled = False, train = True, test = True, inverse = False, boundary = None, activation = None):
        super().__init__(space_dim, path_to_prm_file, scaled = scaled, train = train, test = test, inverse = inverse, boundary = boundary)

        # Private member is re-initialized
        self.__is_scaled = scaled

        if activation != None:
            self.load_data_activation(type_act = activation)




    # Override method that creates all the parameters specific to the derived class
    def read_phys(self, dict):

        self.K = dict['Aliev-Panfilov']['K_exact']
        self.alpha = dict['Aliev-Panfilov']['alpha']
        self.act_prm = dict['Aliev-Panfilov']['activation']

# PDE residuals are overridden ------------------------------------------------------------------------------------------

    # The arguments must be tensor
    def residual(self, x, t):
        space_dim = self.space_dim
        with tf.GradientTape(persistent = True) as tape:
            tape.watch(x)
            tape.watch(t)
            u = self.NN(tf.concat((x,t),axis=1))
            u_t = tf.squeeze(tape.gradient(u, t))
            mat = tf.reduce_sum(tf.math.exp(self.sigma)*self.tens, axis=0)

            d = mat @ tf.reshape(nse.physics.tens_style.gradient_scalar(tape, u, x), [self.num_coll_points, space_dim, 1])
            diff = nse.physics.tens_style.divergence_vector(tape, d, x, space_dim)

            Iion = tf.squeeze(u * (u - 1.0) * (u - self.alpha))

        return u_t - diff + self.K*Iion


    def residual_scaled(self, x, t):
        space_dim = self.space_dim
        with tf.GradientTape(persistent = True) as tape:
            tape.watch(x)
            tape.watch(t)
            u = self.NN(tf.concat((x,t),axis=1))
            u_t = tf.squeeze(tape.gradient(u, t))
            mat = tf.reduce_sum(tf.math.exp(self.sigma)*self.tens, axis=0)

            d = mat @ tf.reshape(nse.physics.tens_style.gradient_scalar(tape, u, x), [self.num_coll_points, space_dim, 1])
            diff = nse.physics.tens_style.divergence_vector(tape, d, x, space_dim)

            Iion = tf.squeeze(u * (u - 1.0) * (u - self.alpha))

        return u_t/(self.params['T'][1]-self.params['T'][0]) - 2*diff + self.K*Iion/2


# Activation times losses -------------------------------------------------------------------------------------

    def load_data_activation(self, name = 'activation', type_act = 'Differences'):
        # Load the initial data from file
        array = self.load_array(name)
        assert array.shape[1] == self.params['space_dim'] + 1, 'The data array dimensions are ' + array.shape[1] + ' when ' + str(self.params['space_dim'] + 1) + ' was expected.'

        x_act = array[:,:self.params['space_dim']]
        t_act = array[:, -1][:,None]

        # Scale if necessary
        if self.__is_scaled:
            x_act = u.array_style.scale_space(x_act, self.params['w'], self.params['space_dim'])
            t_act = u.array_style.scale(t_act, self.params['T'])

        u_init = tf.constant(array[:,-1])
        del array

    # Select correct type
        if type_act == 'Differences':
            self.add_diff_act(x_act, t_act)
        elif type_act == 'Fitting':
            self.add_fit_act(x_act, t_act)
        elif type_act == 'Derivatives':
            self.add_der_act(x_act, t_act)
        else:
            raise Exception('Error: activation name not recognized!')



# Activation losses loaders-----------------------------------------------------------------

    # Differences
    def add_diff_act(self, x, t):
        delta = self.act_prm['delta_diff']
        eps = self.act_prm['epsilon']

        if self.__is_scaled:
            delta = 2*delta/(self.params['T'][1]-self.params['T'][0])

        t_up = t + delta
        t_down = t - delta

        xt_up = tf.concat((x, t_up), axis=1)
        xt_down = tf.concat((x, t_down), axis=1)
        _0 = tf.cast(0.0, dtype='float64')
        # Add the loss function

        self.losses.append(ns.LossMeanSquares('diff_down', lambda: tf.math.maximum(_0, self.NN(xt_down) - eps)))
        self.losses.append(ns.LossMeanSquares('diff_up', lambda: tf.math.minimum(_0, self.NN(xt_up) - 1.0 + eps)))


    # Fitting
    def add_fit_act(self, x, t):
        # Fitting loss
        delta = self.act_prm['delta_fit']
        num_extra = self.act_prm['num extra']
        T = self.params['T']
        space_dim = self.params['space_dim']

        if self.__is_scaled:
            delta = 2*delta/(self.params['T'][1]-self.params['T'][0])


        u_act = np.concatenate((np.zeros(num_extra), np.ones(num_extra)))
        u_act = np.tile(u_act, x.shape[0])
        for m in range(x.shape[0]):

            # Check if t is scaled, and samples points accordingly
            # The min/max are used to make sure the linspace doesn't invert the two extrema
            if self.__is_scaled:
                a = np.linspace(np.minimum(-1.0, t[m] - delta), t[m] - delta, num_extra)
                b = np.linspace(t[m] + delta, np.maximum(1.0, t[m] + delta), num_extra)
            else:
                a = np.linspace(np.minimum(T[0], t[m] - delta), t[m] - delta, num_extra)
                b = np.linspace(t[m] + delta, np.maximum(T[1], t[m] + delta), num_extra)

            # The space values are constant
            c = np.full((2*num_extra, space_dim), x[m])

            # Concatenate to the data array
            if m == 0:
                x_act = c
                t_act = np.concatenate((a,b), axis=0)
            else:
                x_act = np.concatenate((x_act, c), axis=0)
                t_act = np.concatenate((t_act, a,b), axis=0)


        xt = tf.concat((x_act, t_act) , axis=1)
        self.losses.append(ns.LossMeanSquares('fit_activation', lambda: self.NN(xt) - u_act[:,None]))

    # Derivatives
    def add_der_act(self, x, t):
        barrier = self.act_prm['Barrier']
        weights = self.act_prm['Weight der']

        # If the problem is scaled the time derivatives will be too, so we just rescale the respective weights
        if self.__is_scaled:
            weights[0] *= 16/(self.params['T'][1] - self.params['T'][0])**4
            weights[1] *= 4/(self.params['T'][1] - self.params['T'][0])**2

        self.losses.append(ns.LossMeanSquares('der_loss', lambda: self.der_helper(tf.convert_to_tensor(x), tf.convert_to_tensor(t), barrier, weights[0], weights[1])))


    # Helper to separate initialization and proper loss
    def der_helper(self, x, t, barrier, w1, w2):
        with tf.GradientTape(persistent = True) as tape:
            tape.watch(t)
            tape.watch(x)
            u = self.NN(tf.concat((x,t),axis=1))
            u_t = tape.gradient(u, t)
            u_tt = tape.gradient(u_t, t)
        return tf.sqrt(w1*tf.square(u_tt) + w2*tf.square(tf.math.minimum(u_t - barrier, 0)))


# Utilities functions -------------------------------------------------------------------------------

    def __str__(self):
        if self.__is_scaled:
            s0 = 'Scaled'
        else:
            s0 = 'Not scaled'
        s1 = 20*'='

        string = """\nAliev-Panfilov PINN
{}
{}
K: {:.2f}     alpha: {:.2f}
Diffusion matrix eigenvalues: """.format(s0, s1, self.K, self.alpha)+ str(self.get_sigma())

        return string
