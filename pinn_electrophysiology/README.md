This directory contains all the main functionalities of the project. In depth descriptions are available in the report in Section 5.

All modules can be included in scripts with

```python
import pinn_electrophysiolgy._module_name
```

*core*: base class for the PINN.

*aliev_panfilov*, *heat*: derived class for the monodomain equation and heat equation respectively.

*builders*: factory with simplified arguments.

*utils*: scaling functions and optimizer dictionary.
