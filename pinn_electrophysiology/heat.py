###################################
#
# Derived class for heat equation
#
###################################


import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


from pinn_electrophysiology.core import Core
import pinn_electrophysiology.utils as u
import nisaba as ns
import nisaba.experimental as nse
import numpy as np
import tensorflow as tf

class Heat(Core):

    def __init__(self, space_dim, path_to_prm_file,  scaled = False, train = True, test = True, inverse = False, boundary = None):
        super().__init__(space_dim, path_to_prm_file, scaled = scaled, train = train, test = test, inverse = inverse, boundary = boundary)

        # Private member is re-initialized
        self.__is_scaled = scaled



    # Override method that creates all the parameters specific to the derived class
    def read_phys(self, dict):

        self.a = dict['Heat']['a']

# PDE residuals are overridden ------------------------------------------------------------------------------------------


    # The arguments must be tensor
    def residual(self, x, t):
        space_dim = self.space_dim
        with tf.GradientTape(persistent = True) as tape:
            tape.watch(x)
            tape.watch(t)
            u = self.NN(tf.concat((x,t),axis=1))
            u_t = tf.squeeze(tape.gradient(u, t))
            mat = tf.reduce_sum(self.sigma*self.tens, axis=0)

            d = mat @ tf.reshape(nse.physics.tens_style.gradient_scalar(tape, u, x), [self.num_coll_points, space_dim, 1])
            diff = nse.physics.tens_style.divergence_vector(tape, d, x, space_dim)

            reac = tf.squeeze(u)

        return u_t - diff + self.a*reac


    def residual_scaled(self, x, t):
        space_dim = self.space_dim
        with tf.GradientTape(persistent = True) as tape:
            tape.watch(x)
            tape.watch(t)
            u = self.NN(tf.concat((x,t),axis=1))
            u_t = tf.squeeze(tape.gradient(u, t))
            mat = tf.reduce_sum(self.sigma*self.tens, axis=0)

            d = mat @ tf.reshape(nse.physics.tens_style.gradient_scalar(tape, u, x), [self.num_coll_points, space_dim, 1])
            diff = nse.physics.tens_style.divergence_vector(tape, d, x, space_dim)


            #print('u_t: ', u_t.shape)
            #print('Lapl: ', lapl.shape)
            reac = tf.squeeze(u)

        return u_t/(self.params['T'][1]-self.params['T'][0]) - 2*diff + self.a*reac/2


# Utilities functions -------------------------------------------------------------------------------

    def __str__(self):
            if self.__is_scaled:
                s0 = 'Scaled'
            else:
                s0 = 'Not scaled'
            s1 = 20*'='

            string = """\nHeat equation PINN
{}
{}
a: {:.2f}
Diffusion matrix eigenvalues: """.format(s0, s1, self.a)+ str(self.get_sigma())

            return string
