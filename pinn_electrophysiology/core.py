######################################################
#
# Base class for the PINN
#
######################################################


import sys
import os, logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


import json
import abc
import numpy as np
import tensorflow as tf
import nisaba as ns
import nisaba.experimental as nse
import pinn_electrophysiology.utils as u
import matplotlib.pyplot as plt


"""
class Core_Options:
    scaled = True
    inverse = False
    test = True
"""


class Core(abc.ABC):


    # Checks if argumenst are of the right type
    def check_types(self, dimension, path_to_prm_file, scaled, train, test, inverse, boundary):
        assert isinstance(dimension, int), 'Space dimension must be an int'
        assert isinstance(path_to_prm_file, str), 'Path to file must be a string'
        assert isinstance(scaled, (int, bool)), 'scaled argument must be a bool'
        assert isinstance(train, (int, bool)), 'train argument must be a bool'
        assert isinstance(test, (int, bool)), 'test argument must be a bool'
        assert isinstance(inverse, (int, bool)), 'inverse argument must be a bool'
        assert isinstance(boundary, str) or boundary is None, 'boundary argument msut be str or None'

    def read_params(self, dimension, path_to_file):
        # params is a dictionary containing all the model parameters
        # Rakes parameters common to all parabolic PDEs

        file = json.load(open(path_to_file))
        # Select the parameters to read depending on the space dimension
        self.params = file[str(dimension)+'D']

        # Optimization parameters
        self.opt_prm = file['Optimization']

        # Information on the input files
        self.data_prm = file['Data']
        self.dir = self.data_prm['Directory']

        # Virtual method, informations specific to the PDE
        self.read_phys(file['Physics'])

        # Initialize diffusion tensor

        vec = self.params['diff versors']
        self.tens = tf.linalg.matmul(vec, vec, transpose_b=True)
        del vec



    # This method is overridden by the derived class, in this way all parameters are specific of a type of PDE
    def read_phys(self, file):
        pass


    # Initializer -------------------------------------------------------------------------------------------------------------------------

    def __init__(self, dimension, path_to_prm_file, scaled = False, train = True, test = True, inverse = False,  boundary = None):

        self.check_types(dimension, path_to_prm_file, scaled, train, test, inverse, boundary)

        self.space_dim = int(dimension)
        print('Loading the parameters from file')
        self.read_params(dimension, path_to_prm_file)

        self.__is_scaled = scaled

        # Initialize the model
        self.NN = tf.keras.Sequential([
            tf.keras.layers.Dense(10, input_shape=(self.params['space_dim'] + 1,), activation=tf.nn.tanh),
            tf.keras.layers.Dense(20, activation=tf.nn.tanh),
            tf.keras.layers.Dense(20, activation=tf.nn.tanh),
            tf.keras.layers.Dense(10, activation=tf.nn.tanh),
            tf.keras.layers.Dense(1, dtype = 'float64')
        ])
        self.trainable_variables = self.NN.variables

        self.losses = []
        self.losses_test = []

        # Loads the correct loss functions
        if train:
            print('Loading Train loss')
            self.__load_data_fit('train', True)

        print('Loading Initial loss')
        self.__load_data_fit('init', True)

        print('Loading PDE loss')
        self.__load_PDE(inverse)
        if test:
            print('Loading Test loss')
            self.__load_data_fit('MSE', False)

        # Boundary conditions
        if boundary != None:
            self.__load_boundary_conditions(cond_type = boundary)
            print('Loaded Boundary loss : ' + str(boundary))
        else:
            print('Warining: No boundary conditions specified the method might fail in case of dishomogeneous conditions.')


        """
        print('Tensor shape: ', self.tens.shape)
        print('Sigma shape: ', self.sigma.shape)
        print('Diffusivity shape: ', tf.reduce_sum(self.sigma*self.tens, axis=0).shape)
        """

# Loss functions methods  --------------------------------------------------------------------------------

    # Load a .npy or .csv file as a numpy array

    def load_array(self, name):
        filename = self.data_prm['Files'][name]

        file_type = filename.split('.')[-1]

        if file_type == 'npy':
            array =  np.load(self.dir + filename)
        elif file_type == 'csv':
            array = np.genfromtxt(name, delimiter=',')
        else:
            raise Exception('Error: File format not recognized!')

        # If the file has 0 rows it raises a warning
        if array.shape[0] == 0:
            print('Warning: File has 0 rows')

        return array


    # Fitting loss: used for training points, MSE and initial condition

    def __load_data_fit(self, name, train):

        # Load the initial data from file
        array = self.load_array(name)
        assert array.shape[1] == self.space_dim + 2, 'The data array dimensions are ' + str(array.shape[1]) + ' when ' + str(self.space_dim + 2) + ' was expected.'

        #xt_train = tf.convert_to_tensor(array[:,:self.params['space_dim'] + 1])
        xt = tf.convert_to_tensor(array[:,:self.space_dim + 1])
        if self.__is_scaled:
            xt = u.tens_style.scale_space(xt, self.params['w'] + [self.params['T']], self.space_dim + 1)

        u_train = tf.convert_to_tensor(array[:,-1])
        if train:

            self.losses.append(ns.LossMeanSquares(name + '_fit', lambda: self.NN(xt) - u_train[:,None]))
        else:
            self.losses_test.append(ns.LossMeanSquares(name + '_fit', lambda: self.NN(xt) - u_train[:,None]))
        del array


    # Load the PDE residual loss -----------------------------------------------------------

    def __load_PDE(self, inverse_pb, name='PDE'):

        # Load the initial data from file
        array = self.load_array(name)
        assert array.shape[1] == self.space_dim + 1 or array.shape[1] == self.space_dim + 2, 'The dataset dimensions are incompatible with the problem.'

        # Load as float64 for Nisaba compatibility
        x_grid = tf.constant(array[:,:self.space_dim], dtype = 'float64')
        t_grid = tf.constant(array[:,self.space_dim], dtype = 'float64', shape = (array.shape[0], 1))

        self.num_coll_points = x_grid.shape[0]

        # Check applied current, if not present initialize as 0
        if array.shape[1] == self.space_dim + 1:
            print('Warning: No applied current specified in the file, using zero value')
            f = tf.zeros(self.num_coll_points, dtype='float64')
        else:
            f = tf.constant(array[:,-1], dtype='float64')

        del array

        # Scale diffusion tensor if needed
        if self.__is_scaled:
            # Scale inputs
            x_grid = u.tens_style.scale_space(x_grid, self.params['w'], self.space_dim)
            t_grid = u.tens_style.scale(t_grid, self.params['T'])

            # Initialize jacobian matrix
            w_tens = tf.constant(self.params['w'], dtype='float32')
            J = tf.linalg.diag(1/(w_tens[:,1] - w_tens[:,0]))
            J = tf.reshape(J, (1, self.space_dim, self.space_dim))
            J = tf.repeat(J, self.space_dim, axis=0)
            self.tens = J @ self.tens @ J


        # Repeat the tensor for the batch size
        self.tens = tf.reshape(self.tens, (self.params['space_dim'], 1, self.space_dim, self.space_dim))
        self.tens = tf.cast(tf.repeat(self.tens, self.num_coll_points, axis=1), 'float64')


        # Add conductivities to the trainable variables if it's an inverse problem
        # Saves log value since they are positive
        if inverse_pb:
            # This members contains the logarithms of conductivities
            self.sigma = ns.Variable([tf.math.log(x) for x in self.params['sigma_guess']])
            assert self.sigma.shape == self.space_dim , 'Wrong dimension for the diffusion coefficients'
            self.trainable_variables.append(self.sigma)

            # Add diagnostic losses for the sigmas depending on the dimension
            # No loop to avoid issues with TensorFlow autograph
            if self.space_dim == 1:
                self.losses_test.append(ns.Loss('sigma_1_rel', lambda: (tf.math.exp(self.sigma)-self.params['sigma_exact'])/self.params['sigma_exact']))
                self.losses_test.append(ns.Loss('sigma_1', lambda: tf.math.exp(self.sigma)))

            elif self.space_dim == 2:
                self.losses_test.append(ns.Loss('sigma_1_rel', lambda: (tf.math.exp(self.sigma[0])-self.params['sigma_exact'][0])/self.params['sigma_exact'][0]))
                self.losses_test.append(ns.Loss('sigma_2_rel', lambda: (tf.math.exp(self.sigma[1])-self.params['sigma_exact'][1])/self.params['sigma_exact'][1]))

            elif self.space_dim == 3:
                #self.losses_test.append(ns.Loss('sigma_1_rel', lambda: (tf.math.exp(self.sigma[0])-self.params['sigma_exact'][0])/self.params['sigma_exact'][0]))
                #self.losses_test.append(ns.Loss('sigma_2_rel', lambda: (tf.math.exp(self.sigma[1])-self.params['sigma_exact'][1])/self.params['sigma_exact'][1]))
                #self.losses_test.append(ns.Loss('sigma_3_rel', lambda: (tf.math.exp(self.sigma[2])-self.params['sigma_exact'][2])/self.params['sigma_exact'][2]))
                self.losses_test.append(ns.Loss('sigma_1', lambda: tf.math.exp(self.sigma[0])))
                self.losses_test.append(ns.Loss('sigma_2', lambda: tf.math.exp(self.sigma[1])))
                self.losses_test.append(ns.Loss('sigma_3', lambda: tf.math.exp(self.sigma[2])))
            else:
                pass

        # In case parameters are known
        else:
            self.sigma = tf.math.log(tf.constant(self.params['sigma_exact'], dtype = 'float64'))

        # Select the right residual
        if self.__is_scaled:
            PDE = self.residual_scaled
            weight_PDE = 4*self.opt_prm['weight_PDE']
        else:
            PDE = self.residual
            weight_PDE = self.opt_prm['weight_PDE']

        # Append loss
        self.losses.append(ns.LossMeanSquares(name, lambda: PDE(x_grid, t_grid) - f, weight = weight_PDE))



    # Boundary conditions functions-------------------------------------------------------------------------------

    def __load_boundary_conditions(self, cond_type, name = 'boundary'):

        # Loads data
        array = self.load_array(name)
        assert array.shape[1] == self.space_dim + 2, 'The boundary dataset dimensions are ' + str(array.shape[1]) + ' when ' + str(self.space_dim + 2) + ' was expected.'

        xt_bdry = array[:,:self.space_dim + 1]
        if self.__is_scaled:
            xt_bdry = u.array_style.scale_space(xt_bdry, self.params['w'] + [self.params['T']], self.space_dim + 1)

        g = tf.convert_to_tensor(array[:,-1])
        del array

        self.num_bdry_points = xt_bdry.shape[0]

        # Load the right type of boundary condition

        # Dirichlet
        if cond_type == 'Dirichlet':
            self.weight_BD = self.opt_prm['weight_BD']
            self.losses.append(ns.LossMeanSquares('BD_Dirichlet', lambda: self.Dirichlet(xt_bdry, g), weight = self.weight_BD))


        # Reuses code to to load divergence versors for both Neumann and Robin
        elif cond_type == 'Neumann' or cond_type == 'Robin':
            x_bdry = tf.constant(xt_bdry[:,:self.space_dim], dtype = 'float64')
            t_bdry = tf.constant(xt_bdry[:,self.space_dim], dtype = 'float64', shape = (self.num_bdry_points, 1))

            # Load the normal versors
            array = self.load_array('BD_versors')
            n = tf.convert_to_tensor(array, dtype = 'float64')
            del array


            assert self.num_bdry_points == n.shape[0], 'The batch dimension for the normal versors is different from the number of boundary points'
            assert self.space_dim == n.shape[1], 'The dimension of the normal versor is ' + str(n.shape[1]) + ' when ' + str(self.space_dim) + ' was expected.'

            n = tf.reshape(n, (self.num_bdry_points, 1, self.space_dim))


            # Get another diffusivity tensor
            vec = self.params['diff versors']
            self.tens_bdr = tf.linalg.matmul(vec, vec, transpose_b=True)


            # Scale diffusivity tensor appropriately
            if self.__is_scaled:
                # Initialize jacobian matrix
                w_tens = tf.constant(self.params['w'], dtype='float32')
                J = tf.linalg.diag(1/(w_tens[:,1] - w_tens[:,0]))
                J = tf.reshape(J, (1, self.space_dim, self.space_dim))
                J = tf.repeat(J, self.space_dim, axis=0)
                self.tens_bdr = self.tens_bdr @ J

            # Repeat the tensor for the batch size
            self.tens_bdr = tf.reshape(self.tens_bdr, (self.space_dim, 1, self.space_dim, self.space_dim))
            self.tens_bdr = tf.cast(tf.repeat(self.tens_bdr, self.num_bdry_points, axis=1), 'float64')

            weight_BD = self.opt_prm['weight_BD']

            # Neumann condition
            if cond_type == 'Neumann':

                # Add loss Neumann
                self.losses.append(ns.LossMeanSquares('BD_Neumann', lambda: self.Neumann(x_bdry,t_bdry, n, g), weight = weight_BD))

            # Robin condition
            else:

                #Load h term
                array = self.load_array('h_Robin')
                assert self.num_bdry_points == n.shape[0], 'The batch dimension for the h term is different from the number of boundary points'

                h = tf.convert_to_tensor(array[:,0], dtype = 'float64')

                # Add loss
                self.losses.append(ns.LossMeanSquares('BD_Robin', lambda: self.Neumann(x_bdry,t_bdry, n, g, h), weight = weight_BD))



        else:

            # If not recognized raise exception
            raise Exception('Error: Boundary conditions type not recognized no loss loaded!')


# Helper functions for boundary conditions

    def Dirichlet(self, xt, g):
        return self.NN(xt) - g


    def Neumann(self, x, t, n, g):
        with tf.GradientTape(persistent = True) as tape:
            tape.watch(x)
            u = self.NN(tf.concat((x,t),axis=1))
            mat = tf.reduce_sum(tf.math.exp(self.sigma)*self.tens_bdr, axis=0)

            d = n @ mat @ tf.reshape(nse.physics.tens_style.gradient_scalar(tape, u, x), [self.num_bdry_points, self.space_dim, 1])

        return d - g


    def Robin(self, x, t, n, g, h):
        with tf.GradientTape(persistent = True) as tape:
            tape.watch(x)
            u = self.NN(tf.concat((x,t),axis=1))
            mat = tf.reduce_sum(tf.math.exp(self.sigma)*self.tens_bdr, axis=0)

            d = n @ mat @ tf.reshape(nse.physics.tens_style.gradient_scalar(tape, u, x), [self.num_bdry_points, self.space_dim, 1])

        return d +h*u - g

# Finalizer ---------------------------------------------------------------------------------------------------

    def __del__(self):
        del self.NN
        #tf.keras.backend.clear_session()
        del self.trainable_variables
        for loss in self.losses:
            del loss
        for loss in self.losses_test:
            del loss


    # Residual functions (purely virtual) -------------------------------------------
    @abc.abstractmethod
    def residual(self, x, t):
        raise NotImplementedError()

    @abc.abstractmethod
    def residual_scaled(self, x, t):
        raise NotImplementedError()



    # Optimization methods------------------------------------------------------------

    # Initialize problem before minimization
    def init_pb(self, frequency_print = 100, compile = True, verbosity=1):

        print('Initializing the optimization problem')
        self.pb = ns.OptimizationProblem(self.trainable_variables, self.losses, self.losses_test, frequency_print = frequency_print, verbosity=verbosity)
        if compile:
            self.pb.compile()

    # Wrapper for the minimization
    def minimize(self, learner, num_epochs=None, **kwargs):
        if num_epochs == None:
            num_epochs = self.opt_prm['num_epochs']

        ns.minimize(self.pb, u.opt.alias[learner][0], u.opt.alias[learner][1](**kwargs), num_epochs = num_epochs)

    # Return the prediction of the model----------------------------------------------------------------

    def predict(self, x):

        x = tf.convert_to_tensor(x)

        # Scale inputs if necessary
        if self.__is_scaled:
            x = u.tens_style.scale_space(x, self.params['w']+[self.params['T']], self.params['space_dim']+1)

        return self.NN(x)

    # Diagnostics and utilities--------------------------------------------------------------------------

    # Save history through Nisaba
    def save_history(self, plotname):
        self.pb.save_history(plotname + '.json')

    # Learned diffusivity
    def get_sigma(self):
         return tf.math.exp(self.sigma).numpy()


    # Plots the learned solution at the passed times
    # The three dimensions use different types of plots
    def test_plot(self, times, path, **kwargs):

        fig = plt.figure(figsize=(7.0, 6.2))
        n_pt = 100
        w = self.params['w']
        T = self.params['T']
        nr = (len(times) + 1) // 2
        nc = 2

        # 1D: plot
        if self.params['space_dim'] == 1:
            if self.__is_scaled:
                x = np.linspace(-1, 1, n_pt)
            else:
                x = np.linspace(w[0][0], w[0][1], n_pt)
            for i in range(len(times)):
                t = np.full((n_pt), times[i])
                if self.__is_scaled:
                    t = u.array_style.scale(t, T)
                ax = fig.add_subplot(nr, nc, i+1)
                ax.plot(x, self.NN(tf.stack((x,t), axis=1)), 'r-')
                ax.set_title('t = '+ str(times[i]*1000)+' ms')
            plt.suptitle('Action potential')

        # 2D: imshow
        elif self.params['space_dim'] == 2:

            # Create the input matrix for imshow
            if self.__is_scaled:
                x_ = [np.linspace(-1, 1, n_pt), np.linspace(-1, 1, n_pt)]
            else:
                x_ = [np.linspace(w[0][0], w[0][1], n_pt), np.linspace(w[1][0], w[1][1], n_pt)]
            x = np.zeros((n_pt, n_pt, 2))
            for k in range(n_pt):
                x[k,:,0] = x_[0]
                x[:,k,1] = x_[1]

            for i in range(len(times)):
                t = np.full((n_pt,n_pt,1), times[i])
                if self.__is_scaled:
                    t = u.array_style.scale(t, T)
                ax = fig.add_subplot(nr, nc, i+1)
                #print(self.NN(tf.concat((x,t), axis=2)).shape)
                im = ax.imshow(tf.squeeze(self.NN(tf.concat((x,t), axis=2))), vmin=0.0, vmax=1.0)
                ax.set_title('at t ='+ str(times[i]*1000)+' ms')

            plt.tight_layout(h_pad = 1.6)
            cbar_ax = fig.add_axes([0.93, 0.15, 0.01, 0.7])
            #cbar_ax.set_xlabel('', labelpad=20)
            plt.colorbar(im, cax=cbar_ax)
            plt.suptitle('Action potential')




        # 3D: imshow along a plane (this can be generalized to d>3 dimensions)
        elif self.params['space_dim'] == 3:

            # Check if the selected plane is acceptable
            if 'plane_axis' not in kwargs.keys():
                raise Exception('Specify an axis for the 3D plot.')

            plane_axis = kwargs['plane_axis']

            if plane_axis < 0 or plane_axis > 2:
                raise Exception('Plotting ax outside the 0-2 range.')

            if 'plane' not in kwargs.keys():
                raise Exception('Specify a plane for the 3D plot.')

            plane = kwargs['plane']

            if plane < w[plane_axis][0] or plane > w[plane_axis][1]:
                raise RuntimeWarning('The selected plotting plane is outside the domain.')

            # Delete axis from ranges
            w_ = list(w)
            w_.pop(plane_axis)

            # Create the input matrix
            if self.__is_scaled:
                x_ = [np.linspace(-1, 1, n_pt), \
                    np.linspace(-1, 1, n_pt)]
                plane = u.array_style.scale(plane, w[plane_axis])
            else:
                x_ = [np.linspace(w_[0][0], w_[0][1], n_pt), \
                np.linspace(w_[1][0], w_[1][1], n_pt)]

            x = np.zeros((n_pt, n_pt, 3))
            x[:,:, plane_axis] = np.full((n_pt, n_pt), plane)
            i_ax = list(range(3))
            i_ax.pop(plane_axis)

            for k in range(n_pt):
                x[k,:,i_ax[0]] = x_[0]
                x[:,k,i_ax[1]] = x_[1]

            # Plotting phase
            for i in range(len(times)):
                t = np.full((n_pt,n_pt,1), times[i])
                if self.__is_scaled:
                    t = u.array_style.scale(t, T)
                ax = fig.add_subplot(nr, nc, i+1)
                im = ax.imshow(tf.squeeze(self.NN(tf.concat((x,t), axis=2))), vmin=0.0, vmax=1.0)
                ax.set_xlabel(['x','y','z'][i_ax[0]])
                ax.set_ylabel(['x','y','z'][i_ax[1]])
                ax.set_title('at t = '+ str(times[i]*1000)+' ms')

            plt.tight_layout(h_pad = 1.6)
            cbar_ax = fig.add_axes([0.93, 0.15, 0.01, 0.7])
            #cbar_ax.set_xlabel('', labelpad=20)
            plt.colorbar(im, cax=cbar_ax)
            plt.suptitle('Action potential')



        else:
            print('Plotting not supposted for space dimension > 3')
            return

        #plt.show()
        plt.savefig(path)



    # Gets current value of a test loss (if defined)-----------------------------------------
    def get_loss_test(self, name):
        loss_names = [l.name for l in self.losses_test]
        if name not in loss_names:
            raise Exception(name+' was not computed in this object!')
        else:
            i = loss_names.index(name)
            return self.losses_test[i].call().numpy()

    # Gets history of a test loss (if defined)------------------------------------------------
    def get_log_loss_test(self, name):
        loss_names = [l.name for l in self.losses_test]
        if name not in loss_names:
            raise Exception(name+' was not computed in this object!')
        else:
            return self.pb.history['losses_test'][name]['log']


    # Gets current value of a loss (if defined)-----------------------------------------
    def get_loss(self, name):
        loss_names = [l.name for l in self.losses]
        if name not in loss_names:
            raise Exception(name+' was not computed in this object!')
        else:
            i = loss_names.index(name)
            return self.losses[i].call().numpy()

    # Gets history of a loss (if defined)------------------------------------------------
    def get_log_loss(self, name):
        loss_names = [l.name for l in self.losses]
        if name not in loss_names:
            raise Exception(name+' was not computed in this object!')
        else:
            return self.pb.history['losses'][name]['log']
