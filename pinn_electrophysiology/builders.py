from pinn_electrophysiology.aliev_panfilov import AP
from pinn_electrophysiology.heat import Heat

# Factories for derived classes with a simplified initializer

def generate_AP(dim, path, problem_type, boundary, scaled = True):
    if problem_type == 'Forward':
        return AP(dim, path, scaled, test = True, train = False, inverse = False, boundary = boundary, activation = None)

    if problem_type == 'Inverse standard':
        return AP(dim, path, scaled, test = True, train = True, inverse = True, boundary = boundary, activation = None)

    if problem_type == 'Interpolation':
        return AP(dim, path, scaled, test = True, train = True, inverse = False, boundary = boundary, activation = None)

    if problem_type.split('-')[0] == 'Inverse activation':
        return AP(dim, path, scaled, test = True, train = False, inverse = True, boundary = boundary, activation = problem_type.split('-')[1])

    raise Exception('Error: PINN type not recognized!')


def generate_heat(dim, path, problem_type, boundary, scaled = True):
    if problem_type == 'Forward':
        return Heat(dim, path, scaled, test = True, train = False, inverse = False, boundary = boundary)

    if problem_type == 'Inverse standard':
        return Heat(dim, path, scaled, test = True, train = True, inverse = True, boundary = boundary)

    if problem_type == 'Interpolation':
        return Heat(dim, path, scaled, test = True, train = True, inverse = False, boundary = boundary)

    raise Exception('Error: PINN type not recognized!')


def generate_PINN(name, dim, path, problem_type, boundary, scaled = True):
    if name.upper() == 'HEAT':
        return generate_heat(dim, path, problem_type, boundary, scaled)

    if name.upper() == 'ALIEV-PANFILOV':
        return generate_AP(dim, path, problem_type, boundary, scaled)

    raise Exception('Error: PDE type not suppoerted.')
