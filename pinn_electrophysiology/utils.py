# Define the linear scaling functions

import tensorflow as tf
import numpy as np

# Input is a list
class list_style:

    @staticmethod
    def scale(x, lim):
        return (2*x - lim[1] - lim[0])/(lim[1] - lim[0])

    @staticmethod
    def scale_space(x, lim, d):
        _x = []
        for i in range(d):
            _x.append(list_style.scale(x[i], lim[i]))
        return _x

# Input is an array
class array_style:

    @staticmethod
    def scale(x, lim):
        return (2*x - lim[1] - lim[0])/(lim[1] - lim[0])

    @staticmethod
    def scale_space(x, lim, d):
        _x = np.zeros(x.shape)
        for i in range(d):
            _x[:,i] = array_style.scale(x[:,i], lim[i])
        return _x



# Input is a tensor
class tens_style:

    @staticmethod
    def scale(x, lim):
        return (2*x - lim[1] - lim[0])/(lim[1] - lim[0])

    @staticmethod
    def scale_space(x, lim, d):
        return tf.stack([tens_style.scale(elem, lim[i]) for elem,i in zip(tf.unstack(x , axis=1), range(d))], axis=1)


# Dictionary to acess Nisaba supported optimizers quickly, without the backend
class opt:

    alias = {
            # keras
            'Adadelta'      : ('keras', tf.keras.optimizers.Adadelta),
            'Adagrad'       : ('keras', tf.keras.optimizers.Adagrad),
            'Adam'          : ('keras', tf.keras.optimizers.Adam),
            'Adamax'        : ('keras', tf.keras.optimizers.Adamax),
            'FTRL'          : ('keras', tf.keras.optimizers.Ftrl),
            'NAdam'         : ('keras', tf.keras.optimizers.Nadam),
            'RMSprop'       : ('keras', tf.keras.optimizers.RMSprop),
            'SGD'           : ('keras', tf.keras.optimizers.SGD),

            # scipy
            'Nelder-Mead'   : ('scipy', lambda **kwargs: 'Nelder-Mead'),
            'Powell'        : ('scipy', lambda **kwargs: 'Powell'),
            'CG'            : ('scipy', lambda **kwargs: 'CG'),
            'BFGS'          : ('scipy', lambda **kwargs: 'BFGS'),
            'Newton-CG'     : ('scipy', lambda **kwargs: 'Newton-CG'),
            'L-BFGS-B'      : ('scipy', lambda **kwargs: 'L-BFGS-B'),
            'TNC'           : ('scipy', lambda **kwargs: 'TNC'),
            'COBYLA'        : ('scipy', lambda **kwargs: 'COBYLA'),
            'SLSQP'         : ('scipy', lambda **kwargs: 'SLSQP'),
            'trust-constr'  : ('scipy', lambda **kwargs: 'trust-constr'),
            'dogleg'        : ('scipy', lambda **kwargs: 'dogleg'),
            'trust-ncg'     : ('scipy', lambda **kwargs: 'trust-ncg'),
            'trust-krylov'  : ('scipy', lambda **kwargs: 'trust-krylov'),
            'trust-exact'   : ('scipy', lambda **kwargs: 'trust-exact')

            #scipy_ls
            #'trf'           : ('scipy_ls', lambda **kwargs: 'trf'),
            #'bogbox'        : ('scipy_ls', lambda **kwargs: 'dogbox'),
            #'lm'            : ('scipy_ls', lambda **kwargs: 'lm'),
            #nisaba
            #'nisaba_lm'     : ('nisaba', lambda **kwargs: 'lm')


            }
